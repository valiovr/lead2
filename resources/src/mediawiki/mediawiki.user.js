/**
 * @class mw.user
 * @singleton
 */
( function ( mw, $ ) {
	var user,
		deferreds = {},
		// Extend the skeleton mw.user from mediawiki.js
		// This is kind of ugly but we're stuck with this for b/c reasons
		options = mw.user.options || new mw.Map(),
		tokens = mw.user.tokens || new mw.Map();

	/**
	 * Get the current user's groups or rights
	 *
	 * @private
	 * @param {string} info One of 'groups' or 'rights'
	 * @return {jQuery.Promise}
	 */
	function getUserInfo( info ) {
		var api;
		if ( !deferreds[info] ) {

			deferreds.rights = $.Deferred();
			deferreds.groups = $.Deferred();

			api = new mw.Api();
			api.get( {
				action: 'query',
				meta: 'userinfo',
				uiprop: 'rights|groups'
			} ).always( function ( data ) {
				var rights, groups;
				if ( data.query && data.query.userinfo ) {
					rights = data.query.userinfo.rights;
					groups = data.query.userinfo.groups;
				}
				deferreds.rights.resolve( rights || [] );
				deferreds.groups.resolve( groups || [] );
			} );

		}

		return deferreds[info].promise();
	}

	mw.user = user = {
		options: options,
		tokens: tokens,

		/**
		 * Generate a random user session ID (32 alpha-numeric characters)
		 *
		 * This information would potentially be stored in a cookie to identify a user during a
		 * session or series of sessions. Its uniqueness should not be depended on.
		 *
		 * @return {string} Random set of 32 alpha-numeric characters
		 */
		generateRandomSessionId: function () {
			var i, r,
				id = '',
				seed = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			for ( i = 0; i < 32; i++ ) {
				r = Math.floor( Math.random() * seed.length );
				id += seed.charAt( r );
			}
			return id;
		},

		/**
		 * Get the current user's database id
		 *
		 * Not to be confused with #id.
		 *
		 * @return {number} Current user's id, or 0 if user is anonymous
		 */
		getId: function () {
			return mw.config.get( 'wgUserId', 0 );
		},

		/**
		 * Get the current user's name
		 *
		 * @return {string|null} User name string or null if user is anonymous
		 */
		getName: function () {
			return mw.config.get( 'wgUserName' );
		},

		/**
		 * Get date user registered, if available
		 *
		 * @return {Date|boolean|null} Date user registered, or false for anonymous users, or
		 *  null when data is not available
		 */
		getRegistration: function () {
			var registration = mw.config.get( 'wgUserRegistration' );
			if ( user.isAnon() ) {
				return false;
			} else if ( registration === null ) {
				// Information may not be available if they signed up before
				// MW began storing this.
				return null;
			} else {
				return new Date( registration );
			}
		},

		/**
		 * Whether the current user is anonymous
		 *
		 * @return {boolean}
		 */
		isAnon: function () {
			return user.getName() === null;
		},

		/**
		 * Get an automatically generated random ID (stored in a session cookie)
		 *
		 * This ID is ephemeral for everyone, staying in their browser only until they close
		 * their browser.
		 *
		 * @return {string} Random session ID
		 */
		sessionId: function () {
			var sessionId = $.cookie( 'mediaWiki.user.sessionId' );
			if ( sessionId === undefined || sessionId === null ) {
				sessionId = user.generateRandomSessionId();
				$.cookie( 'mediaWiki.user.sessionId', sessionId, { expires: null, path: '/' } );
			}
			return sessionId;
		},

		/**
		 * Get the current user's name or the session ID
		 *
		 * Not to be confused with #getId.
		 *
		 * @return {string} User name or random session ID
		 */
		id: function () {
			return user.getName() || user.sessionId();
		},

		/**
		 * Get the user's bucket (place them in one if not done already)
		 *
		 *     mw.user.bucket( 'test', {
		 *         buckets: { ignored: 50, control: 25, test: 25 },
		 *         version: 1,
		 *         expires: 7
		 *     } );
		 *
		 * @deprecated since 1.23
		 * @param {string} key Name of bucket
		 * @param {Object} options Bucket configuration options
		 * @param {Object} options.buckets List of bucket-name/relative-probability pairs (required,
		 *  must have at least one pair)
		 * @param {number} [options.version=0] Version of bucket test, changing this forces
		 *  rebucketing
		 * @param {number} [options.expires=30] Length of time (in days) until the user gets
		 *  rebucketed
		 * @return {string} Bucket name - the randomly chosen key of the `options.buckets` object
		 */
		bucket: function ( key, options ) {
			var cookie, parts, version, bucket,
				range, k, rand, total;

			options = $.extend( {
				buckets: {},
				version: 0,
				expires: 30
			}, options || {} );

			cookie = $.cookie( 'mediaWiki.user.bucket:' + key );

			// Bucket information is stored as 2 integers, together as version:bucket like: "1:2"
			if ( typeof cookie === 'string' && cookie.length > 2 && cookie.indexOf( ':' ) !== -1 ) {
				parts = cookie.split( ':' );
				if ( parts.length > 1 && Number( parts[0] ) === options.version ) {
					version = Number( parts[0] );
					bucket = String( parts[1] );
				}
			}

			if ( bucket === undefined ) {
				if ( !$.isPlainObject( options.buckets ) ) {
					throw new Error( 'Invalid bucket. Object expected for options.buckets.' );
				}

				version = Number( options.version );

				// Find range
				range = 0;
				for ( k in options.buckets ) {
					range += options.buckets[k];
				}

				// Select random value within range
				rand = Math.random() * range;

				// Determine which bucket the value landed in
				total = 0;
				for ( k in options.buckets ) {
					bucket = k;
					total += options.buckets[k];
					if ( total >= rand ) {
						break;
					}
				}

				$.cookie(
					'mediaWiki.user.bucket:' + key,
					version + ':' + bucket,
					{ path: '/', expires: Number( options.expires ) }
				);
			}

			return bucket;
		},

		/**
		 * Get the current user's groups
		 *
		 * @param {Function} [callback]
		 * @return {jQuery.Promise}
		 */
		getGroups: function ( callback ) {
			return getUserInfo( 'groups' ).done( callback );
		},

		/**
		 * Get the current user's rights
		 *
		 * @param {Function} [callback]
		 * @return {jQuery.Promise}
		 */
		getRights: function ( callback ) {
			return getUserInfo( 'rights' ).done( callback );
		}
	};

	/**
	 * @method name
	 * @inheritdoc #getName
	 * @deprecated since 1.20 Use #getName instead
	 */
	mw.log.deprecate( user, 'name', user.getName, 'Use mw.user.getName instead.' );

	/**
	 * @method anonymous
	 * @inheritdoc #isAnon
	 * @deprecated since 1.20 Use #isAnon instead
	 */
	mw.log.deprecate( user, 'anonymous', user.isAnon, 'Use mw.user.isAnon instead.' );
    mm_contentHeight();
    mm_displayMetaObjects();

    $( '#bodyContent' ).on('change', function(){
        setTimeout(function(){
            mm_contentHeight();
        },500)

    });

	//fix new template
    //$('#accordion').on('show hide', function() {
    //    $(this).css('height', 'auto');
    //});
    //$('#accordion').collapse({ parent: true, toggle: false });
$('div.accordionTitle').on('click',function(){
    var colapse_elem = $('.accordionContent', $(this).parent());
    $(colapse_elem).slideToggle(700,function(){
        var has_class = $(this).hasClass('open');
        if(has_class){
            $(this).removeClass('open');
        }else{
            $(this).addClass('open');
        }
      //  console.log(has_class);
    });
   // console.log(colapse_elem);
});
}( mediaWiki, jQuery ) );

/*******************************************************
 ******************* Container Height *******************
 ********************************************************/


function mm_contentHeight(){

	var windowWidth = $(window).width();
    var mm_containerheight = $('div.container-fluid').height();
    var mm_navbar_height = $('div.container-fluid div.row nav.lead-navbar').height();
    var mm_toolbar_height = $('div.container-fluid div.toolbar').height();
    var mm_footer_height = $('div.footer-navbar').height();

    var header_height = mm_navbar_height + mm_toolbar_height;

    var sidebar_height = (mm_containerheight - (header_height + mm_footer_height))-15;
    var lead_content_height = $('div.lead-content').height();
    if(lead_content_height>sidebar_height){
        sidebar_height = lead_content_height-15;
    }


    if(parseInt(windowWidth) > 1152) {
        $('div#leftCol').css({'min-height':sidebar_height+'px','height':'auto !important'}); //, 'height':sidebar_height+'px'
    }

}

/*******************************************************
 ******************* Scroll to top *******************
 ********************************************************/

$(window).scroll(function(){

    if ($(this).scrollTop() > 100) {
        $('#top-link-block').removeClass('hidden');
    } else {
        $('#top-link-block').addClass('hidden');
    }
});

/*******************************************************
 ******************* Meta objects Definitions *******************
 ********************************************************/

function mm_displayMetaObjects(){
        $('select#layers-list').on('change',function(){
            $('div.sublayer-box').removeAttr('style');
            var selected_layer = $(this).val();
            if(selected_layer !=''){
                var sublayer_list = $('ul#layer-'+selected_layer+' li');
                $('select#sublayers-list option').each(function( index ) {
                    if(index>0){
                        $(this).remove();
                    }
                });
                sublayer_list.each(function( index ) {
                    var sublayer_name = $( this ).text()
                    var sublayer_id = $( this).attr('class').replace('sublayer-', '');

                    $('select#sublayers-list').append('<option value="sublayer-content-'+sublayer_id+'">'+sublayer_name+'</option>');
                });
                $('.sublayer-box').css('display','none');
                $('.layer-box-'+selected_layer).show();

            }else{
                $('select#sublayers-list option').each(function( index ) {
                    if(index>0){
                        $(this).remove();
                    }
                });
                $('div.sublayer-box').removeAttr('style');
            }
            mm_contentHeight();
        });

    $('select#sublayers-list').on('change',function(){
        $('div.sublayer-box').removeAttr('style');
        var selected_sublayer = $(this).val();
        if(selected_sublayer !=''){
            $('.sublayer-box').css('display','none');
            $('div#'+selected_sublayer).show();
        }else{

            var main_layer = $('select#layers-list').val();
            if(main_layer !=''){
                $('.sublayer-box').css('display','none');
                $('.layer-box-'+main_layer).show();
            }else{
                $('div.sublayer-box').removeAttr('style');
            }

        }
        mm_contentHeight();
    });
}