/*!
 * JavaScript for Special:Preferences
 */
jQuery( function ( $ ) {
	var $preftoc, $preferences, $fieldsets, $legends,
		hash, labelFunc,
		$tzSelect, $tzTextbox, $localtimeHolder, servertime,
		$checkBoxes, savedWindowOnBeforeUnload;

	labelFunc = function () {
		return this.id.replace( /^mw-prefsection/g, 'preftab' );
	};

	$( '#prefsubmit' ).attr( 'id', 'prefcontrol' );
	$preftoc = $( '<ul id="preftoc"></ul>' )
		.attr( 'role', 'tablist' );
	$preferences = $( '#preferences' )
		.addClass( 'jsprefs' )
		.before( $preftoc );
	$fieldsets = $preferences.children( 'fieldset' )
		.hide()
		.attr( {
			role: 'tabpanel',
			'aria-hidden': 'true',
			'aria-labelledby': labelFunc
		} )
		.addClass( 'prefsection' );
	$legends = $fieldsets
		.children( 'legend' )
		.addClass( 'mainLegend' );

	// Make sure the accessibility tip is selectable so that screen reader users take notice,
	// but hide it per default to reduce interface clutter. Also make sure it becomes visible
	// when selected. Similar to jquery.mw-jump
	$( '<div>' ).addClass( 'mw-navigation-hint' )
		.text( mediaWiki.msg( 'prefs-tabs-navigation-hint' ) )
		.attr( 'tabIndex', 0 )
		.on( 'focus blur', function ( e ) {
			if ( e.type === 'blur' || e.type === 'focusout' ) {
				$( this ).css( 'height', '0' );
			} else {
				$( this ).css( 'height', 'auto' );
			}
	} ).insertBefore( $preftoc );

	/**
	 * It uses document.getElementById for security reasons (HTML injections in $()).
	 *
	 * @ignore
	 * @param String name: the name of a tab without the prefix ("mw-prefsection-")
	 * @param String mode: [optional] A hash will be set according to the current
	 *  open section. Set mode 'noHash' to surpress this.
	 */
	function switchPrefTab( name, mode ) {
		var $tab, scrollTop;
		// Handle hash manually to prevent jumping,
		// therefore save and restore scrollTop to prevent jumping.
		scrollTop = $( window ).scrollTop();
		if ( mode !== 'noHash' ) {
			window.location.hash = '#mw-prefsection-' + name;
		}
		$( window ).scrollTop( scrollTop );

		$preftoc.find( 'li' ).removeClass( 'selected' )
			.find( 'a' ).attr( {
				tabIndex: -1,
				'aria-selected': 'false'
			} );

		$tab = $( document.getElementById( 'preftab-' + name ) );
		if ( $tab.length ) {
			$tab.attr( {
				tabIndex: 0,
				'aria-selected': 'true'
			} )
			.focus()
				.parent().addClass( 'selected' );

			$preferences.children( 'fieldset' ).hide().attr( 'aria-hidden', 'true' );
			$( document.getElementById( 'mw-prefsection-' + name ) ).show().attr( 'aria-hidden', 'false' );
		}
	}

	// Populate the prefToc
	$legends.each( function ( i, legend ) {
		var $legend = $( legend ),
			ident, $li, $a;
		if ( i === 0 ) {
			$legend.parent().show();
		}
		ident = $legend.parent().attr( 'id' );

		$li = $( '<li>' )
			.attr( 'role', 'presentation' )
			.addClass( i === 0 ? 'selected' : '' );
		$a = $( '<a>' )
			.attr( {
				id: ident.replace( 'mw-prefsection', 'preftab' ),
				href: '#' + ident,
				role: 'tab',
				tabIndex: i === 0 ? 0 : -1,
				'aria-selected': i === 0 ? 'true' : 'false',
				'aria-controls': ident
			} )
			.text( $legend.text() );
		$li.append( $a );
		$preftoc.append( $li );
	} );

	// Enable keyboard users to use left and right keys to switch tabs
	$preftoc.on( 'keydown', function ( event ) {
		var keyLeft = 37,
			keyRight = 39,
			$el;

		if ( event.keyCode === keyLeft ) {
			$el = $( '#preftoc li.selected' ).prev().find( 'a' );
		} else if ( event.keyCode === keyRight ) {
			$el = $( '#preftoc li.selected' ).next().find( 'a' );
		} else {
			return;
		}
		if ( $el.length > 0 ) {
			switchPrefTab( $el.attr( 'href' ).replace( '#mw-prefsection-', '' ) );
		}
	} );

	// If we've reloaded the page or followed an open-in-new-window,
	// make the selected tab visible.
	hash = window.location.hash;
	if ( hash.match( /^#mw-prefsection-[\w\-]+/ ) ) {
		switchPrefTab( hash.replace( '#mw-prefsection-', '' ) );
	}

	// In browsers that support the onhashchange event we will not bind click
	// handlers and instead let the browser do the default behavior (clicking the
	// <a href="#.."> will naturally set the hash, handled by onhashchange.
	// But other things that change the hash will also be catched (e.g. using
	// the Back and Forward browser navigation).
	// Note the special check for IE "compatibility" mode.
	if ( 'onhashchange' in window &&
		( document.documentMode === undefined || document.documentMode >= 8 )
	) {
		$( window ).on( 'hashchange', function () {
			var hash = window.location.hash;
			if ( hash.match( /^#mw-prefsection-[\w\-]+/ ) ) {
				switchPrefTab( hash.replace( '#mw-prefsection-', '' ) );
			} else if ( hash === '' ) {
				switchPrefTab( 'personal', 'noHash' );
			}
		} );
	// In older browsers we'll bind a click handler as fallback.
	// We must not have onhashchange *and* the click handlers, other wise
	// the click handler calls switchPrefTab() which sets the hash value,
	// which triggers onhashcange and calls switchPrefTab() again.
	} else {
		$preftoc.on( 'click', 'li a', function ( e ) {
			switchPrefTab( $( this ).attr( 'href' ).replace( '#mw-prefsection-', '' ) );
			e.preventDefault();
		} );
	}

	// Timezone functions.
	// Guesses Timezone from browser and updates fields onchange.

	$tzSelect = $( '#mw-input-wptimecorrection' );
	$tzTextbox = $( '#mw-input-wptimecorrection-other' );
	$localtimeHolder = $( '#wpLocalTime' );
	servertime = parseInt( $( 'input[name="wpServerTime"]' ).val(), 10 );

	function minutesToHours( min ) {
		var tzHour = Math.floor( Math.abs( min ) / 60 ),
			tzMin = Math.abs( min ) % 60,
			tzString = ( ( min >= 0 ) ? '' : '-' ) + ( ( tzHour < 10 ) ? '0' : '' ) + tzHour +
				':' + ( ( tzMin < 10 ) ? '0' : '' ) + tzMin;
		return tzString;
	}

	function hoursToMinutes( hour ) {
		var minutes,
			arr = hour.split( ':' );

		arr[0] = parseInt( arr[0], 10 );

		if ( arr.length === 1 ) {
			// Specification is of the form [-]XX
			minutes = arr[0] * 60;
		} else {
			// Specification is of the form [-]XX:XX
			minutes = Math.abs( arr[0] ) * 60 + parseInt( arr[1], 10 );
			if ( arr[0] < 0 ) {
				minutes *= -1;
			}
		}
		// Gracefully handle non-numbers.
		if ( isNaN( minutes ) ) {
			return 0;
		} else {
			return minutes;
		}
	}

	function updateTimezoneSelection() {
		var minuteDiff, localTime,
			type = $tzSelect.val();

		if ( type === 'guess' ) {
			// Get browser timezone & fill it in
			minuteDiff = -( new Date().getTimezoneOffset() );
			$tzTextbox.val( minutesToHours( minuteDiff ) );
			$tzSelect.val( 'other' );
			$tzTextbox.prop( 'disabled', false );
		} else if ( type === 'other' ) {
			// Grab data from the textbox, parse it.
			minuteDiff = hoursToMinutes( $tzTextbox.val() );
		} else {
			// Grab data from the $tzSelect value
			minuteDiff = parseInt( type.split( '|' )[1], 10 ) || 0;
			$tzTextbox.val( minutesToHours( minuteDiff ) );
		}

		// Determine local time from server time and minutes difference, for display.
		localTime = servertime + minuteDiff;

		// Bring time within the [0,1440) range.
		while ( localTime < 0 ) {
			localTime += 1440;
		}
		while ( localTime >= 1440 ) {
			localTime -= 1440;
		}
		$localtimeHolder.text( mediaWiki.language.convertNumber( minutesToHours( localTime ) ) );
	}

	if ( $tzSelect.length && $tzTextbox.length ) {
		$tzSelect.change( updateTimezoneSelection );
		$tzTextbox.blur( updateTimezoneSelection );
		updateTimezoneSelection();
	}

	// Preserve the tab after saving the preferences
	// Not using cookies, because their deletion results are inconsistent.
	// Not using jStorage due to its enormous size (for this feature)
	if ( window.sessionStorage ) {
		if ( sessionStorage.getItem( 'mediawikiPreferencesTab' ) !== null ) {
			switchPrefTab( sessionStorage.getItem( 'mediawikiPreferencesTab' ), 'noHash' );
		}
		// Deleting the key, the tab states should be reset until we press Save
		sessionStorage.removeItem( 'mediawikiPreferencesTab' );

		$( '#mw-prefs-form' ).submit( function () {
			var storageData = $( $preftoc ).find( 'li.selected a' ).attr( 'id' ).replace( 'preftab-', '' );
			sessionStorage.setItem( 'mediawikiPreferencesTab', storageData );
		} );
	}

	// To disable all 'namespace' checkboxes in Search preferences
	// when 'Search in all namespaces' checkbox is ticked.
	$checkBoxes = $( '#mw-htmlform-advancedsearchoptions input[id^=mw-input-wpsearchnamespaces]' );
	if ( $( '#mw-input-wpsearcheverything' ).prop( 'checked' ) ) {
		$checkBoxes.prop( 'disabled', true );
	}
	$( '#mw-input-wpsearcheverything' ).change( function () {
		$checkBoxes.prop( 'disabled', $( this ).prop( 'checked' ) );
	} );

	// Set up a message to notify users if they try to leave the page without
	// saving.
	$( '#mw-prefs-form' ).data( 'origdata', $( '#mw-prefs-form' ).serialize() );
	$( window )
		.on( 'beforeunload.prefswarning', function () {
			var retval;

			// Check if anything changed
			if ( $( '#mw-prefs-form' ).serialize() !== $( '#mw-prefs-form' ).data( 'origdata' ) ) {
				// Return our message
				retval = mediaWiki.msg( 'prefswarning-warning', mediaWiki.msg( 'saveprefs' ) );
			}

			// Unset the onbeforeunload handler so we don't break page caching in Firefox
			savedWindowOnBeforeUnload = window.onbeforeunload;
			window.onbeforeunload = null;
			if ( retval !== undefined ) {
				// ...but if the user chooses not to leave the page, we need to rebind it
				setTimeout( function () {
					window.onbeforeunload = savedWindowOnBeforeUnload;
				}, 1 );
				return retval;
			}
		} )
		.on( 'pageshow.prefswarning', function () {
			// Re-add onbeforeunload handler
			if ( !window.onbeforeunload ) {
				window.onbeforeunload = savedWindowOnBeforeUnload;
			}
		} );
	$( '#mw-prefs-form' ).submit( function () {
		// Unbind our beforeunload handler
		$( window ).off( '.prefswarning' );
	} );
	$( '#mw-prefs-restoreprefs' ).click( function () {
		// Unbind our beforeunload handler
		$( window ).off( '.prefswarning' );
	} );
} );
///** Custom JavaScript for Page Preferences START**/
//$('head').append("<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>");
//WebFontConfig = {
//    google: {
//        families: ['Open+Sans::latin']
//    }
//};
(function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
    //  $('.lead-content').css('min-height', '100vmax');
})();
$(window).load(function() {
    var adjusted = 0;
    var clicked = 0;
    // adjustContentHeight();
    // setTimeout(adjustContentHeight, 500);
    var dropdownHeader = $('.lead-dropdown');
    //var defaultSideBarHeight = $('.lead-sidebar .panel-group')[0].scrollHeight;
    $(dropdownHeader).on('click', function() {
        $('.panel-collapse', $(this).parent()).collapse('toggle');
        $(this).find('.panel-title').find('a').trigger('classChanged');
        setTimeout(function() {
            // var contentHeight = $('.lead-content')[0].scrollHeight;
            // var newSidebarHeight = $('.lead-sidebar .panel-group')[0].scrollHeight;
            // var heightAddition = newSidebarHeight - defaultSideBarHeight;
            // if($('.lead-content')[0].scrollHeight < newSidebarHeight) {
            //    // $('.lead-content').css('height', contentHeight + heightAddition);

            // }
            // $('.lead-content').css('min-height', '100vmax');
        }, 500);
    });
    $('.lead-sidebar').find('.panel-title').find('a').on('classChanged', function() {
        var menuIsExpanded = $(this).attr('aria-expanded');
        if ($(this).parents('.lead-dropdown').find('span.glyphicon').hasClass('glyphicon-circle-arrow-down')) {
            $(this).parents('.lead-dropdown').find('span.glyphicon').addClass('glyphicon-circle-arrow-up').removeClass('glyphicon-circle-arrow-down');
            $(this).parents('.lead-dropdown').find('span.glyphicon').trigger('hover');
        } else {
            $(this).parents('.lead-dropdown').find('span.glyphicon').addClass('glyphicon-circle-arrow-down').removeClass('glyphicon-circle-arrow-up').trigger('hover');
            $(this).parents('.lead-dropdown').find('span.glyphicon').trigger('hover');
        }
    });
    $('.lead-sidebar .list-group-item').on('click', function() {
        var highlightedMenus = $('.menu-highlighted');
        $.each(highlightedMenus, function() {
            $(this).removeClass('menu-highlighted');
        });
        $(this).addClass('menu-highlighted');
        // $(this).children('a').first().trigger('click');
        url = $(this).children('a').first().attr('href');
        $(location).attr('href', url);
    });
    var lastScrollTop = 100;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            $('#top-link-block').removeClass('hidden');
        } else {
            $('#top-link-block').addClass('hidden');
        }
        lastScrollTop = 100;
    });
    // if (($(window).height()) < $(document).height()) {
    //     $('#top-link-block').removeClass('hidden').affix({
    //         // how far to scroll down before link "slides" into view
    //         offset: {
    //             top: 100
    //         }
    //     });
    // }
    var navBarButtons = $('.navbar-collapse.lead-navbar > ul > li > a > strong');
    navBarButtons.on('click', function() {
        navBarButtons.each(function() {
            $(this).removeClass('lead-navbar-active');
        });
        $(this).addClass('lead-navbar-active');
        waitForContentLoad();
    });
    $('.pagetools #ca-nstab-main').hide();
    $('.toolbar #p-search').addClass('pull-right');
    // WebFontConfig = {
    //     google: {
    //         families: ['Open+Sans::latin']
    //     }
    // };
    // (function() {
    //     var wf = document.createElement('script');
    //     wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    //     wf.type = 'text/javascript';
    //     wf.async = 'true';
    //     var s = document.getElementsByTagName('script')[0];
    //     s.parentNode.insertBefore(wf, s);
    // })();
    var query = window.location.search.substring(1);
    // alert(query);
    if (query.indexOf('title') != -1) {
        var eqOper = query.indexOf('=');
        if (eqOper != -1) {
            var ampPos = query.indexOf('&');
            if (ampPos != -1) {
                var visPage = query.substr(ampPos + 1, query.length);
            } else {
                var visPage = query.substr(eqOper + 1, query.length);
            }
            if (visPage.indexOf('title') != -1) {
                visPage = visPage.substr(visPage.indexOf('=') + 1, visPage.length);
            }
            highlightPageButton(visPage);
        }
    }
    var triggerClick = function(elem) {
        elem.trigger('click');
    };

    function highlightPageButton(page) {
        page = page.toLowerCase();
        var query = window.location.search.substring(1);
        page = "?title=" + page;
        if (query.indexOf('cat') != -1) {
            page = '?' + query;
        }
        var pageAnchor = $('a[href="' + page + '"]');
        var sideBarUrl = pageAnchor.closest('.lead-sidebar');
        var headerBarUrl = pageAnchor.closest('.lead-navbar');
        var headerBarUrl2 = pageAnchor.closest('.main-header-menu');
        var toolBarUrl = pageAnchor.closest('.toolbar');
        var footerUrl = pageAnchor.closest('.footer-navbar');
        var contentUrl = pageAnchor.closest('.lead-content');
        if (sideBarUrl.length) {
            pageAnchor.parents('.list-group-item').first().addClass('menu-highlighted');
            pageAnchor.parents('.panel-collapse').first().removeClass('collapse').addClass('collapse in');
            var leadCat = query.substr('?cat', query.indexOf('&'));
            leadCat = leadCat.substr(leadCat.indexOf('=') + 1, leadCat.length);
            leadCat = leadCat.replace(/_/g, ' ');
            leadCat = leadCat.replace(/%26/g, '&');
            headerAnchor = $('.lead-navbar a:contains(' + leadCat.toUpperCase() + ')');
            headerAnchor.closest('li').addClass('menu-highlighted');
            headerAnchor.closest('.main-header-menu').css('background-color', '#0B98D6');
            headerAnchor.closest('.main-header-menu').children('button').addClass('important-highlight');
            //fuck !important css tags!
            headerAnchor.closest('.main-header-menu').children('button').attr('aria-expanded', 'gosho');
        } else if (headerBarUrl.length || headerBarUrl2.length) {
            var name = query.substr(query.indexOf('title'), query.length);
            name = name.substr(name.indexOf('=') + 1, name.length)
            name = name.replace(/_/g, ' ');
            name = name.replace(/%26/g, '&');
            var selectedMenu = $('.lead-sidebar h4 a:contains(' + name + ')');
            if (selectedMenu.length) {
                selectedMenu.trigger('click');
            }
            pageAnchor.closest('li').addClass('menu-highlighted');
            pageAnchor.closest('.main-header-menu').css('background-color', '#0B98D6');
            pageAnchor.closest('.main-header-menu').children('button').addClass('important-highlight');
            //fuck !important css tags!
            pageAnchor.closest('.main-header-menu').children('button').attr('aria-expanded', 'gosho');
        } else {
            pageAnchor.trigger('focus');
        }
    }
    // $('head').append("<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>");
    $('.lead-navbar .dropdown button a').on('click', function() {
        $(location).attr('href', $(this).attr('href'));
    });
    $('.lead-navbar .dropdown button').on('click', function() {
        $(location).attr('href', $(this).children('a').first().attr('href'));
    });
});
/** Custom JavaScript for Page Preferences END**/
