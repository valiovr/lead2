    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
        //Content height
        mm_contentHeight();
    })();
    $(window).load(function() {
        $('.dropdown-toggle').on('click', function(event) {
            var anchorElem = $(this).children('a').first();
            var elemUrl = anchorElem.attr('href');
            window.location.href = elemUrl;
        });
    });
    // script to mass edit
    // $.each($('.mw-allpages-chunk').find('a'), function() {
    //     $(this).attr('href', $(this).attr('href') + '&action=edit');
    // });
    $(function() {
        var pageTitle = getUrlParameter('title');
        pageTitle = pageTitle.toLowerCase();
        pageTitle = pageTitle.replace('_', ' ');
        pageTitle = pageTitle.trim();
        var headerMenuButtons = $('.lead-navbar').find('.dropdown-toggle');
        var menuButtonsNoDropdown = $('.lead-navbar').find('.no-dropdown-menu');
        $.each(headerMenuButtons, function() {
            var menuButtonAnchor = $(this).children('a').first();
            var menuButtonAnchorTitle = menuButtonAnchor.text();
            menuButtonAnchorTitle = menuButtonAnchorTitle.toLowerCase();
            menuButtonAnchorTitle = menuButtonAnchorTitle.trim();
            if (pageTitle.indexOf(menuButtonAnchorTitle) != -1) {
                $(this).addClass('important-highlight');
            }
        });

        $('.accordionContent').find('.list-group-item').on('click', function() {
            window.location.href = $(this).children('a').first().attr('href');
        });

        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    });
    /*******************************************************
     ******************* Container Height *******************
     ********************************************************/
    function mm_contentHeight() {
        var mm_containerheight = $('div.container-fluid').height();
        var mm_navbar_height = $('div.container-fluid div.row nav.lead-navbar').height();
        var mm_toolbar_height = $('div.container-fluid div.toolbar').height();
        var mm_footer_height = $('div.footer-navbar').height();
        var header_height = mm_navbar_height + mm_toolbar_height;
        var sidebar_height = mm_containerheight - (header_height + mm_footer_height);
        var lead_content_height = $('div.lead-content').height();
        if (lead_content_height > sidebar_height) {
            sidebar_height = lead_content_height;
        }
        $('div#leftCol').css({
            'min-height': sidebar_height + 'px',
            'height': 'auto !important'
        }); //, 'height':sidebar_height+'px'
    }