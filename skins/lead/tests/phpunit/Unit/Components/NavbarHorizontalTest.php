<?php
/**
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke, mwjames
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Tests\Unit\Components;

use Skins\lead\Components\NavbarHorizontal;
use Skins\lead\Tests\Util\MockupFactory;

/**
 * @coversDefaultClass \Skins\lead\Components\NavbarHorizontal
 * @covers ::<private>
 * @covers ::<protected>
 *
 * @group   skins-lead
 * @group   mediawiki-databaseless
 *
 * @author mwjames
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 * @ingroup Test
 */
class NavbarHorizontalTest extends GenericComponentTestCase {

	protected $classUnderTest = '\Skins\lead\Components\NavbarHorizontal';

	/**
	 * @covers ::getHtml
	 */
	public function testGetHtml_containsNavElement() {

		$element = $this->getMockBuilder( '\DOMElement' )
			->disableOriginalConstructor()
			->getMock();

		$message = $this->getMockBuilder( '\Message' )
			->disableOriginalConstructor()
			->getMock();

		$leadTemplate = $this->getMockBuilder( '\Skins\lead\leadTemplate' )
			->disableOriginalConstructor()
			->getMock();

		$leadTemplate->expects( $this->any() )
			->method( 'getMsg' )
			->will( $this->returnValue( $message ) );

		$instance = new NavbarHorizontal(
			$leadTemplate,
			$element
		);

		$matcher = array( 'tag' => 'nav' );
		$this->assertTag( $matcher, $instance->getHtml() );
	}

	/**
	 * @covers ::getHtml
	 * @dataProvider domElementProviderFromSyntheticLayoutFiles
	 */
	public function testGetHtml_LoggedInUserHasNewMessages( $domElement ) {

		$factory = MockupFactory::makeFactory( $this );
		$factory->set( 'UserIsLoggedIn', true );
		$factory->set( 'UserNewMessageLinks', array( 'foo' ) );
		$leadTemplate = $factory->getleadSkinTemplateStub();

		/** @var $instance Component */
		$instance = new $this->classUnderTest ( $leadTemplate, $domElement );

		$matcher = array( 'class' => 'navbar-newtalk-available' );
		$this->assertTag( $matcher, $instance->getHtml() );
	}

	/**
	 * @covers ::getHtml
	 * @dataProvider domElementProviderFromSyntheticLayoutFiles
	 */
	public function testGetHtml_LoggedInUserHasNoNewMessages( $domElement ) {

		$factory = MockupFactory::makeFactory( $this );
		$factory->set( 'UserIsLoggedIn', true );
		$factory->set( 'UserNewMessageLinks', array() );
		$leadTemplate = $factory->getleadSkinTemplateStub();

		/** @var $instance Component */
		$instance = new $this->classUnderTest ( $leadTemplate, $domElement );

		$matcher = array( 'class' => 'navbar-newtalk-not-available' );
		$this->assertTag( $matcher, $instance->getHtml() );
	}

}
