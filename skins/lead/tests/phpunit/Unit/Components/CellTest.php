<?php
/**
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Tests\Unit\Components;

/**
 * @coversDefaultClass \Skins\lead\Components\Cell
 * @covers ::<private>
 * @covers ::<protected>
 *
 * @group   skins-lead
 * @group   mediawiki-databaseless
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 * @ingroup Test
 */
class CellTest extends GenericComponentTestCase {

	protected $classUnderTest = '\Skins\lead\Components\Cell';

	/**
	 * @covers ::__construct
	 * @dataProvider provideSpanAttributeValues
	 * @param string $in
	 * @param string $expected
	 */
	public function testSpanAttribute( $in, $expected ) {

		$leadTemplate = $this->getMockBuilder( '\Skins\lead\leadTemplate' )
			->disableOriginalConstructor()
			->getMock();

		$domElement = $this->getMockBuilder( '\DOMElement' )
			->disableOriginalConstructor()
			->getMock();

		$domElement->expects( $this->any() )
			->method( 'getAttribute' )
			->will( $this->returnValueMap( array( array( 'span', $in ) ) ) );

		$instance = new $this->classUnderTest ( $leadTemplate, $domElement );

		$this->assertEquals(
			"col-lg-$expected",
			$instance->getClassString()
		);

	}

	/**
	 * @covers ::getClassString
	 */
	public function testGetClassString_WithoutSetting() {

		$leadTemplate = $this->getMockBuilder( '\Skins\lead\leadTemplate' )
			->disableOriginalConstructor()
			->getMock();

		$instance = new $this->classUnderTest ( $leadTemplate );

		$this->assertTrue( $instance->getClassString() === 'col-lg-12' );

	}

	public function provideSpanAttributeValues() {
		return array(
			array( '9', '9' ),
			array( '-1', '12' ),
			array( '42', '12' ),
			array( 'foo', '12' ),
			array( '10.5', '12' ),
		);
	}

}
