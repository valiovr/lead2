<?php
/**
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke, mwjames
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Tests\Unit\Components;

use Skins\lead\Components\MainContent;

/**
 * @coversDefaultClass \Skins\lead\Components\MainContent
 * @covers ::<private>
 * @covers ::<protected>
 *
 * @group   skins-lead
 * @group   mediawiki-databaseless
 *
 * @author  mwjames
 * @since 1.0
 * @ingroup Skins
 * @ingroup Test
 */
class MainContentTest extends GenericComponentTestCase {

	protected $classUnderTest = '\Skins\lead\Components\MainContent';

	/**
	 * @covers ::getHtml
	 */
	public function testGetHtml_OnEmptyDataProperty() {

		$leadTemplate = $this->getleadSkinTemplateStub();

		$leadTemplate->data = array(
			'subtitle'         => '',
			'undelete'         => '',
			'printfooter'      => '',
			'dataAfterContent' => ''
		);

		$instance = new MainContent( $leadTemplate );
		$this->assertInternalType( 'string', $instance->getHtml() );
	}
}
