<?php
/**
 * The lead skin. A Mediawiki skin using Twitter Bootstrap.
 *
 * @see     https://www.mediawiki.org/wiki/Skin:lead
 *
 * @author  Stephan Gambke
 * @version 1.0-alpha
 *
 */

/**
 * This is the main file of the lead skin
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 *
 * @codeCoverageIgnore
 */

call_user_func( function () {

	if ( !defined( 'MEDIAWIKI' ) ) {
		die( 'This file is part of a MediaWiki extension, it is not a valid entry point.' );
	}

	if ( !defined( 'BS_VERSION' ) ) {
		die( '<b>Error:</b> The <a href="https://www.mediawiki.org/wiki/Skin:lead">lead</a> skin depends on the Bootstrap extension. You need to install the <a href="https://www.mediawiki.org/wiki/Extension:Bootstrap">Bootstrap</a> extension first.' );
	}

	// define the skin's version
	define( 'lead_VERSION', '1.1.4' );

	// set credits
	$GLOBALS[ 'wgExtensionCredits' ][ 'skin' ][ ] = array(
		'path'           => __FILE__,
		'name'           => 'lead',
		'descriptionmsg' => 'lead-desc',
		'author'         => '[http://www.mediawiki.org/wiki/User:F.trott Stephan Gambke]',
		'url'            => 'https://www.mediawiki.org/wiki/Skin:lead',
		'version'        => lead_VERSION,
		'license-name'   => 'GPLv3+',
	);

	// register skin
	$GLOBALS[ 'wgValidSkinNames' ][ 'lead' ] = 'lead';

	// register message file for i18n
	$GLOBALS[ 'wgExtensionMessagesFiles' ][ 'lead' ] = __DIR__ . '/lead.i18n.php';
    $GLOBALS[ 'wgMessagesDirs' ][ 'lead' ] = __DIR__ . '/lead/resources/i18n';

	/**
	 * Using callbacks for hook registration
	 *
	 * The hook registry should contain as less knowledge about a process as
	 * necessary therefore a callback is used as Factory/Builder that instantiates
	 * a business / domain object.
	 *
	 * GLOBAL state should be encapsulated by the callback and not leaked into
	 * a instantiated class
	 */

	/**
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/SetupAfterCache
	 */
	$GLOBALS[ 'wgHooks' ][ 'SetupAfterCache' ][ ] = function() {

		$setupAfterCache = new \Skins\lead\Hooks\SetupAfterCache(
			\Bootstrap\BootstrapManager::getInstance(),
			$GLOBALS
		);

		$setupAfterCache->process();
	};

	// set default skin layout
	$GLOBALS[ 'egleadLayoutFile' ] = dirname( __FILE__ ) . '/layouts/standard.xml';

	// enable the VisualEditor for this skin
	$GLOBALS[ 'egleadEnableVisualEditor' ] = true;

    $GLOBALS[ 'wgResourceModules' ]['skin.lead.css'] = array(
        'styles' => array(
            'lead/resources/assets/plugins/bootstrap/css/bootstrap.min.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/plugins/font-awesome/css/font-awesome.min.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/fonts/style.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/css/main.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/css/main-responsive.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/plugins/iCheck/skins/all.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/css/theme_light.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/css/lead.css' => array( 'media' => 'screen' ),
            'lead/resources/assets/css/print.css' => array( 'media' => 'print' ),
            'lead/resources/assets/css/custom-responsive.css' => array( 'media' => 'print' ),
            'lead/resources/assets/css/custom.css' => array( 'media' => 'screen' ),
        ),
        'remoteSkinPath' => &$GLOBALS['wgStylePath'],
        'localBasePath' => &$GLOBALS['wgStyleDirectory'],
    );

    $GLOBALS[ 'wgResourceModules' ]['skin.lead.js'] = array(
        'scripts' => array(
//            'lead/resources/assets/plugins/jQuery-lib/2.0.3/jquery.min.js',
            'lead/resources/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js',
            'lead/resources/assets/plugins/bootstrap/js/bootstrap.min.js',
            'lead/resources/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'lead/resources/assets/plugins/blockUI/jquery.blockUI.js',
            'lead/resources/assets/plugins/iCheck/jquery.icheck.min.js',
            'lead/resources/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js',
            'lead/resources/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js',
            'lead/resources/assets/plugins/less/less-1.5.0.min.js',
            'lead/resources/assets/plugins/jquery-cookie/jquery.cookie.js',
            'lead/resources/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js',
            'lead/resources/assets/js/main.js',
            'lead/resources/assets/js/page-scripts/meta-object-definitions.js',
            'lead/resources/assets/js/custom.js',
        ),
        'dependencies' => array(
            // In this example, awesome.js needs the jQuery UI dialog stuff
            //'jquery.ui.dialog',
        ),
        'remoteBasePath' => &$GLOBALS['wgStylePath'],
        'localBasePath' => &$GLOBALS['wgStyleDirectory'],
    );
	
} );
