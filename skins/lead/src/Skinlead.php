<?php
/**
 * File holding the Skinlead class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */
use Skins\lead\ComponentFactory;

/**
 * SkinTemplate class for the lead skin
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class Skinlead extends SkinTemplate {

	public $skinname = 'lead';
	public $stylename = 'lead';
	public $template = '\Skins\lead\leadTemplate';
	public $useHeadElement = true;

	private $componentFactory;
	private $output;

	/**
	 * @param $out OutputPage object
	 */
	function setupSkinUserCss( OutputPage $out ) {

		$this->output = $out;

		// load Bootstrap styles
		$out->addModuleStyles(
			array(
//				'ext.bootstrap.styles',
				'skin.lead.css'
			)
		);
	}

	/**
	 * @param \OutputPage $out
	 */
	function initPage( OutputPage $out ) {

		parent::initPage( $out );

		// Enable responsive behaviour on mobile browsers

		$out->addMeta( 'viewport', 'width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0' );
		$out->addMeta( 'apple-mobile-web-app-capable', 'yes' );
		$out->addMeta( 'apple-mobile-web-app-status-bar-style', 'black' );
		$out->addStyle( '//fonts.googleapis.com/css?family=Raleway:400,300,200,100,500,600,700,800,900' );
		$out->addStyle( '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' );
	}

	/**
	 * @return ComponentFactory
	 */
	public function getComponentFactory() {

		if ( ! isset( $this->componentFactory ) ) {
			$this->componentFactory = new \Skins\lead\ComponentFactory( $GLOBALS['egleadLayoutFile'] );
		}

		return $this->componentFactory;
	}

	public function addSkinModulesToOutput() {
		// load Bootstrap scripts
		$out = $this->output;
		$out->addModules( array(
			//'ext.bootstrap.scripts'
			'skin.lead.js'
		) );
		$out->addModules( $this->getComponentFactory()->getRootComponent()->getResourceLoaderModules() );

	}


	/**
	 * @param Title $title
	 * @return string
	 */
	public function getPageClasses( $title ) {
		$layoutName = Sanitizer::encodeAttribute( 'layout-' . basename( $GLOBALS['egleadLayoutFile'], '.xml' ) );
		return implode( ' ', array( parent::getPageClasses( $title ), $layoutName.' footer-fixed' ) );
	}
}
