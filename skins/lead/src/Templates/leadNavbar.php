<div class="footer-inner">

                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=terms_conditions">
                    Terms &amp; Conditions
                    </a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=privacy_policy">
                    Privacy Policy</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=cookie_policy">
                    Cookie Policy</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=legal_disclaimer">
                    Legal Disclaimer</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=get_certified">
                    Get Certified</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=get_access">
                    Get Access To More Content</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=conferences_events">
                    Conferences &amp; Events</a>
                    </p>
                    <p class="navbar-text pull-left ">
                    <a class="" href="?title=contact_us">
                    Contact</a>
                    </p>
<!--                    <p class="navbar-text pull-right" style="color:black;">-->
                    <!-- <a class="" id="copyright" href="#"> -->
<!--                    &copy; LEADing Practice. All rights reserved.-->
                    <!-- </a> -->
<!--                    <img class="ssl-logo img pull-right img-responsive img-header-navbar" src="{path_to_ssl_badge}">-->
<!--                    </p>-->
        </div>

<div class="footer-items">
<span class="go-top">
<i class="clip-chevron-up"></i>
</span>
</div>