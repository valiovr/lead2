<style type="text/css">
    @font-face{font-family:'Glyphicons Halflings';src:url(/skins/lead/resources/assets/plugins/bootstrap/fonts/glyphicons-halflings-regular.eot);src:url(/skins/lead/resources/assets/plugins/bootstrap/fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(/skins/lead/resources/assets/plugins/bootstrap/fonts/glyphicons-halflings-regular.woff) format('woff'),url(/skins/lead/resources/assets/plugins/bootstrap/fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(/skins/lead/resources/assets/plugins/bootstrap/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')}
    @font-face {
        font-family: 'clip-font';
        src:url('/skins/lead/resources/assets/fonts/fonts/clip-font.eot');
        src:url('/skins/lead/resources/assets/fonts/fonts/clip-font.eot?#iefix') format('embedded-opentype'),
        url('/skins/lead/resources/assets/fonts/fonts/clip-font.woff') format('woff'),
        url('/skins/lead/resources/assets/fonts/fonts/clip-font.ttf') format('truetype'),
        url('/skins/lead/resources/assets/fonts/fonts/clip-font.svg#clip-font') format('svg');
        font-weight: normal;
        font-style: normal;
    }
    
</style>

<!--        <div class="">-->
                <ul class="nav navbar-nav">
                    <li>
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Category:Objects">OBJECTS</a>
                                <span class="hidden caret"></span>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="?title=Objects:Enterprise_Ontology">Enterprise Ontology</a></li>
                                <li><a href="?title=Objects:Enterprise_Semantics">Enterprise Semantics</a></li>
                            </ul>
                        </div>
                        <!-- <a id="objects" href="?cat=objects">

                        OBJECTS
                        </a> -->
                    </li>
                    <li class="no-dropdown-menu">
                        <!--    <a href="?title=Category:Maps">
                           MAPS
                           </a> -->
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Category:Maps">MAPS</a>
                                <span class="hidden caret"></span>
                        </div>
                    </li>
                    <li class="no-dropdown-menu">
                        <!-- <a href="?title=Category:Matrices">
                        MATRICES
                        </a> -->
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Category:Matrices">MATRICES</a>
                                <span class="hidden caret"></span>
                        </div>
                    </li>
                    <li class="no-dropdown-menu">
                        <!--                     <a href="?title=Category:Models">
                                            MODELS
                                            </a> -->
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Category:Models">MODELS</a>
                                <span class="hidden caret"></span>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Layers:Main">LAYERS</a>
                                <span class="hidden caret"></span>
                            <ul class="dropdown-menu " aria-labelledby="dropdownMenu1">
                                <li><a href="?title=Layers:Business_Layer">Business Layer</a></li>
                                <li><a href="?title=Layers:Information_Layer">Information Layer</a></li>
                                <li><a href="?title=Layers:Technology_Layer">Technology Layer   </a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Category:Plans">PLANS</a>
                                <span class="hidden caret"></span>
                            <ul class="dropdown-menu " aria-labelledby="dropdownMenu1">
                                <li><a href="?title=Plans:Portfolio_%26_Program_Management">Portfolio & Program Management</a></li>
                                <li><a href="?title=Plans:Project_Management">Project Management</a></li>
                                <li><a href="?title=Plans:Lifecycle_Management">Lifecycle Management</a></li>
                                <li><a href="?title=Plans:Governance_Management">Governance Management</a></li>
                                <li><a href="?title=Plans:Transformation_Management">Transformation Management</a></li>
                                <li><a href="?title=Plans:Innovation_Management">Innovation Management</a></li>
                                <li><a href="?title=Plans:Change_Management">Change Management</a></li>
                                <!--                    <li><a href="?cat=layers&title=Business_Layer">Business Layer</a></li>
                                                        <li><a href="?cat=layers&title=Application_Layer">Application Layer</a></li>
                                                        <li><a href="?cat=layers&title=Technology_Layer">Technology Layer   </a></li>  -->
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?title=Enterprise_Standards:Main">ENTERPRISE STANDARDS</a>
                                <span class="hidden caret"></span>
                            <ul class="dropdown-menu " aria-labelledby="dropdownMenu1">
                                <li><a href="?title=Enterprise_Standards:Enterprise_Management_Standards">Enterprise Management Standards</a></li>
                                <li><a href="?title=Enterprise_Standards:Enterprise_Modelling_Standards">Enterprise Modelling Standards</a></li>
                                <li><a href="?title=Enterprise_Standards:Enterprise_Engineering_Standards">Enterprise Engineering Standards</a></li>
                                <li><a href="?title=Enterprise_Standards:Enterprise_Architecture_Standards">Enterprise Architecture Standards</a></li>
                                <li><a href="?title=Enterprise_Standards:Enterprise_Information_%26_Technology_Standards">Enterprise Information & Technology Standards</a></li>
                                <li><a href="?title=Enterprise_Standards:Enterprise_Transformation_%26_Innovation_Standards">Enterprise Transformation & Innovation Standards</a></li>

                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown header-menu main-header-menu">
                                <a href="?cat=industry_standards&title=Industry_Standards:Main">INDUSTRY STANDARDS</a>
                                <span class="hidden caret"></span>

                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="?title=Industry_Standards:Financial_Standards_%26_User_Groups">Financial Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Industrial_Standards_%26_User_Groups">Industrial Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Consumer_Goods_Standards_%26_User_Groups">Consumer Goods Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Consumer_Services_Standards_%26_User_Groups">Consumer Services Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Energy_Standards_%26_User_Groups">Energy Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Government_Standards_%26_User_Groups">Government Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Healthcare_Standards_%26_User_Groups">Healthcare Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Utilities_Standards_%26_User_Groups">Utilities Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Transportation_Standards_%26_User_Groups">Transportation Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Telecommunication_Standards_%26_User_Groups">Telecommunication Standards & User Groups</a></li>
                                <li><a href="?title=Industry_Standards:Information_Technology_Standards_%26_User_Groups">Information Technology Standards & User Groups</a></li>

                            </ul>
                        </div>
                    </li>
                </ul>
<!--        </div>-->
