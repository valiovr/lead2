<div class="navbar-content">
    <!-- start: SIDEBAR -->
    <ul class="main-navigation navbar-collapse collapse">
        <!-- start: MAIN MENU TOGGLER BUTTON -->
        <div class="navigation-toggler">
            <i class="clip-chevron-left"></i>
            <i class="clip-chevron-right"></i>
        </div>
        <!-- end: MAIN MENU TOGGLER BUTTON -->

        <ul class="main-navigation-menu">
            {sidebarData}
            <li class="{active}">
                <a href="javascript:void(0)">

                    <span class="title"> {category_name} </span>
                    <i class="icon-arrow"></i>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    {pages}
                    <li class="{active_page}">
                        <a href="?title={namespace}:{page_url}">
                            <span class="title"> {page_title}</span>
                        </a>
                    </li>
                    {/pages}

                </ul>
            </li>
            {/sidebarData}
        </ul>
    </ul>
</div>
<!--</div>-->
