<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 24.08.2015
 * Time: 17:31
 */

namespace Skins\lead\Components;


class Breadcrumb extends Component {
    private $_data = array();

    /**
     * Builds the HTML code for this component
     *
     * @return String the HTML code
     */
    public function getHtml() {
        global $wgTitle;
        global $wgExtraNamespaces;
        global $wgUser;

        $this->_data['page_data_seg_1'] = array();
        $this->_data['page_data_seg_2'] = array();
        $this->_data['page_data_seg_3'] = array();

        if($wgUser->getId() != 0){

        $pageData = $this->_getPageData($wgTitle->mUrlform, $wgTitle->mNamespace);
//        foreach($pageData as $pgData){
//            var_dump($pgData); die;
//        }
//        echo '<pre>';
//        var_dump($pageData); die;
        if ($pageData->cl_from != NULL && ($wgTitle->mUrlform != 'Main_page' && $wgTitle->mUrlform != 'Main_Page')) {

            $this->_data['page_data_seg_1'][0]['bc_namespace'] = str_replace('_', ' ', $wgExtraNamespaces[$wgTitle->mNamespace]);

            $this->_data['page_data_seg_2'][0]['bc_name'] = str_replace('_', ' ', $pageData->cl_to);
            $this->_data['page_data_seg_2'][0]['bc_class'] = 'active';
            $this->_data['page_data_seg_2'][0]['bc_cat_url'] = "http://" . $_SERVER['SERVER_NAME'] . '/index.php?title=Category:' . urlencode($pageData->cl_to);

            $this->_data['page_data_seg_3'][0]['bc_class'] = 'active';
            $this->_data['page_data_seg_3'][0]['bc_name'] = str_replace('_', ' ', $wgTitle->mTextform);

        } else {

            if ($pageData->page_namespace >= 5000) {
                $this->_data['page_data_seg_1'][0]['bc_namespace'] = str_replace('_', ' ', $wgExtraNamespaces[$pageData->page_namespace]);
                $this->_data['page_data_seg_3'][0]['bc_class'] = 'active';
                $this->_data['page_data_seg_3'][0]['bc_name'] = str_replace('_', ' ', $wgTitle->mTextform);
            } else if ($pageData->cl_from == NULL) {
                $ns_id = $this->_getCategoryPages(str_replace('_', ' ', $wgTitle->mTextform));

                $this->_data['page_data_seg_1'][0]['bc_namespace'] = str_replace('_', ' ', $wgExtraNamespaces[$ns_id]);
                $this->_data['page_data_seg_3'][0]['bc_class'] = 'active';
                $this->_data['page_data_seg_3'][0]['bc_name'] = str_replace('_', ' ', $wgTitle->mTextform);
            } else {
                $this->_data['page_data_seg_1'][0]['bc_namespace'] = str_replace('_', ' ', $wgTitle->mTextform);
            }


        }
    }
        $ret = $this->ci_parser->parse('breadcrumb', $this->_data, true);

        return $ret;
    }

    /**
     * Get Page information
     *
     * @param $title
     * @param $namespace
     * @return array|bool|\stdClass
     */
    private function _getPageData($title, $namespace) {
        $dbr = wfGetDB(DB_SLAVE);
        $list = array();
        $table = $dbr->tableName('page');
        $tableJoin = $dbr->tableName('categorylinks');
        $res = $dbr->select(array($table, $tableJoin),
            array('*'),
            array('page_title' => urldecode($title), 'page_namespace' => $namespace),
            __METHOD__,
            array(),
            array($tableJoin => array('LEFT JOIN', array(
                $table . '.page_id=' . $tableJoin . '.cl_from'))));
        foreach ($res as $row) {
            $result = $row;
        }
        return $result;
    }

    /**
     * Get Category members
     *
     * @param $cat_name
     * @return int
     */
    private function _getCategoryPages($cat_name) {
        global $wgRequest;
        global $wgGroupPermissions;
       global $wgUser;

        $ns = 0;
        if($wgUser->getId() != 0){
        $params = new \DerivativeRequest(
            $wgRequest, array(
            'action' => 'query',
            'list' => 'categorymembers',
            'cmtitle' => 'Category:' . $cat_name,
        ), true
        );

        $api = new \ApiMain($params, true); // default is false
        $api->execute();

        $data = &$api->getResultData();

        foreach ($data['query']['categorymembers'] as $page) {
            if ($ns != $page['ns']) {
                $ns = $page['ns'];
            }
        }
        }
        return $ns;
    }
}