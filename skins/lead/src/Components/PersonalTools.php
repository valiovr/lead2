<?php
/**
 * File holding the PersonalTools class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup   Skins
 */

namespace Skins\lead\Components;

/**
 * The PersonalTools class.
 *
 * An unordered list of personal tools: <ul id="p-personal" >...
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class PersonalTools extends Component {

	/**
	 * Builds the HTML code for this component
	 *
	 * @return String the HTML code
	 */
	public function getHtml() {

		$ret = $this->indent() . '<!-- personal tools -->' .
			   $this->indent() . '<li class="dropdown pagetools p-personal ' . $this->getClassString() . '" id="p-personal" >';
	    $ret .= 
		    '<a class="dropdown-toggle  clip-user-5 floatLeft" href="#" id="personalDropDownMenu" data-toggle="dropdown" aria-expanded="true">
			
			 
			</a>';
		// include message to a user about new messages on their talkpage
		// TODO: make including the NewTalkNotifier dependent on an option (PREPEND, APPEND, OFF)
		$newtalkNotifier = new NewtalkNotifier( $this->getSkinTemplate(), null, $this->getIndent() + 2 );

		$ret .= 
			$this->indent( 1 ) . 
			'<ul 
				role="menu" 
				aria-labelledby="personalDropDownMenu" 
				class="dropdown-menu p-personal-tools"
			>';

		$this->indent( 1 );

		// add personal tools (links to user page, user talk, prefs, ...)
		foreach ( $this->getSkinTemplate()->getPersonalTools() as $key => $item ) {
			$ret .= $this->indent() . $this->getSkinTemplate()->makeListItem( $key, $item );
		}

		$ret .= $this->indent( -1 ) . '</ul>' .
				$this->indent() . '<div class="newtalk-notifier">' . $newtalkNotifier->getHtml() .
				$this->indent() . '</div>' .
				// $this->indent( -1 ) . '</div>' . "\n" . '</div>' . "\n";
				$this->indent( -1 ) . '</li>' . "\n";

		return $ret;
	}

}
