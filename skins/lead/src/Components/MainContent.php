<?php
/**
 * File holding the MainContent class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup   Skins
 */

namespace Skins\lead\Components;

use Skins\lead\IdRegistry;

/**
 * The NavbarHorizontal class.
 *
 * A horizontal navbar containing the sidebar items.
 * Does not include standard items (toolbox, search, language links). They need to be added to the page elsewhere
 *
 * The navbar is a list of lists wrapped in a nav element: <nav role="navigation" id="p-navbar" >
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class MainContent extends Component {

    /**
     * Builds the HTML code for this component
     *
     * @return String the HTML code
     */
    public function getHtml() {
        global $wgTitle;
        global $wgUser;

        if ($wgTitle->getNamespace() == -1 || in_array('sysop', $wgUser->mGroups)){
            $hasPermissions = true;
    }else{
            $hasPermissions = $this->_checkPermissions($wgTitle->getArticleID());
        }

        $skintemplate = $this->getSkinTemplate();
        $idRegistry = IdRegistry::getRegistry();

        // START content
        $ret =
            $this->indent() . '<!-- start the content area -->' .
            $this->indent() . $idRegistry->openElement('div',
                array('id' => 'content', 'class' => 'mw-body ' . $this->getClassString())
            ) .

            $idRegistry->element('a', array('id' => 'top')) .

            $this->indent() . '<div ' . \Html::expandAttributes(array(
                    'id' => $idRegistry->getId('mw-js-message'),
                    'style' => 'display:none;'
                )
            ) . $skintemplate->get('userlangattributes') . '></div>';

        if ($hasPermissions || $wgTitle->getNamespace() == -1 || in_array('sysop', $wgUser->mGroups)) {
            $ret .= $this->buildContentHeader();
            $ret .= $this->buildContentBody();
            $ret .= $this->buildCategoryLinks();
        } else {

            $ret .= $this->buildHeaderNoPermissions();
            $ret .= $this->buildBodyNoPermissions();
        }
        $ret .= $this->indent(-1) . '</div>' . "\n";
        // END content

        return $ret;
    }

    /**
     * @return string
     */
    protected function buildContentHeader() {

        $skintemplate = $this->getSkinTemplate();
        $idRegistry = IdRegistry::getRegistry();

        $ret = $this->indent(1) . '<div class ="page-header">' .

            $this->indent(1) . '<!-- title of the page -->' .
            $this->indent() . $idRegistry->element('h1', array('id' => '', 'class' => ''), $skintemplate->get('title')) . //array( 'id' => 'firstHeading', 'class' => 'firstHeading' )

            $this->indent() . '<!-- tagline; usually goes something like "From WikiName" primary purpose of this seems to be for printing to identify the source of the content -->' .
            $this->indent() . $idRegistry->element('div', array('id' => 'siteSub'), $skintemplate->getMsg('tagline')->escaped());

        if ($skintemplate->get('subtitle')) {

            // TODO: should not use class 'small', better use class 'contentSub' and do styling in a less file
            $ret .=
                $this->indent() . '<!-- subtitle line; used for various things like the subpage hierarchy -->' .
                $this->indent() . $idRegistry->element('div', array('id' => 'contentSub', 'class' => 'small'), $skintemplate->get('subtitle'));

        }

        if ($skintemplate->get('undelete')) {
            // TODO: should not use class 'small', better use class 'contentSub2' and do styling in a less file
            $ret .=
                $this->indent() . '<!-- undelete message -->' .
                $this->indent() . $idRegistry->element('div', array('id' => 'contentSub2', 'class' => 'small'), $skintemplate->get('undelete'));
        }

        // TODO: Do we need this? Seems to be an accessibility thing. It's used in vector to jump to the nav wich is at the bottom of the document, but our nav is usually at the top
        $ret .= $idRegistry->element('div', array('id' => 'jump-to-nav', 'class' => 'mw-jump'),
            $skintemplate->getMsg('jumpto')->escaped() . '<a href="#mw-navigation">' . $skintemplate->getMsg('jumptonavigation')->escaped() . '</a>' .
            $skintemplate->getMsg('comma-separator')->escaped() . '<a href="#p-search">' . $skintemplate->getMsg('jumptosearch')->escaped() . '</a>'
        );

        $ret .= $this->indent(-1) . '</div>';
        return $ret;
    }

    /**
     * @return string
     */
    protected function buildContentBody() {
        $test = $this->getSkinTemplate();
//        echo '<pre>';
//        var_dump($test); die;
        return IdRegistry::getRegistry()->element('div', array('id' => 'bodyContent','class'=>'clearfix'),
            $this->indent() . '<!-- body text -->' . "\n" .
            $this->indent() . $this->getSkinTemplate()->get('bodytext') .
            $this->buildDataAfterContent()
        );
    }

    /**
     * @return string
     */
    protected function buildCategoryLinks() {
        // TODO: Category links should be a separate component, but
        // * dataAfterContent should come after the the category links.
        // * only one extension is known to use it dataAfterContent and it is geared specifically towards MonoBook
        // => provide an attribut hideCatLinks for the XML and -if present- hide category links and assume somebody knows what they are doing
        return
            $this->indent() . '<!-- category links -->' .
            $this->indent() . $this->getSkinTemplate()->get('catlinks');
    }

    /**
     * @return string
     */
    protected function buildDataAfterContent() {

        $skinTemplate = $this->getSkinTemplate();

        if ($skinTemplate->get('dataAfterContent')) {
            return
                $this->indent() . '<!-- data blocks which should go somewhere after the body text, but not before the catlinks block-->' .
                $this->indent() . $skinTemplate->get('dataAfterContent');
        }

        return '';
    }

    /**
     * Check if user has permission to see this page
     * @param $page_id
     */
    private function _checkPermissions($page_id) {
        global $wgLoadApi;
        global $wgUser;

        $userId = $wgUser->getId();

        //Api Url
        $url = $wgLoadApi['url'] . '/check_permissions/' . $page_id . '/' . $userId;

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        return $res['has_permissions'];
    }

    /**
     * No Permission header text
     * @return string
     */
    protected function buildHeaderNoPermissions() {

        $idRegistry = IdRegistry::getRegistry();

        $ret = $this->indent(1) . '<div class ="page-header">' .

            $this->indent(1) . '<!-- title of the page -->' .
            $this->indent() . $idRegistry->element('h1', array('id' => '', 'class' => ''), 'Permission denied!'); //array( 'id' => 'firstHeading', 'class' => 'firstHeading' )

        $ret .= $this->indent(-1) . '</div>';
        return $ret;
    }

    /**
     * No Permission body text
     * @return string
     */
    protected function buildBodyNoPermissions() {
        return IdRegistry::getRegistry()->element('div', array('id' => 'bodyContent'),
            $this->indent() . '<!-- body text -->' . "\n" .
            $this->indent() . 'You do not have permission to view this page.' .
            $this->buildDataAfterContent()
        );
    }
}