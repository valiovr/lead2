<?php
/**
 * File holding the Html class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Components;
ini_set('display_errors', '0');     # don't show any errors...
error_reporting(E_ALL | E_STRICT);  # ...but do log them
/**
 * The Html class.
 *
 * This component allows insertion of raw HTML into the page.
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class SidebarLead extends Component
{
    
    const PATH_TO_API = '/api.php';
    
    // private $baseUrl = ;
    
    private $allSidebars = array('sideBarObjects', 'sideBarMaps', 'sideBarMatrices', 'sideBarModels', 'sideBarLayers', 'sideBarEnterpriseStandards', 'sideBarIndustryStandards', 'sideBarObjects',);
    private $_data = array('path_to_logo' => '/images/2/29/Logo_resized.png',);
    
    /**
     * Builds the HTML code for the main container
     *
     * @return String the HTML code
     */
    public function getHtml() {
        $hiddenSidebars = '';
        if (!isset($_GET['cat'])) {
            $page = 'objects';
        } 
        else {
            $page = $_GET['cat'];
        }
        
        //api call
        $title = $this->getSkin()->getTitle();
        
        $full_title_arr = explode(':', $title->getPrefixedText());
        
            // echo'<pre>';var_dump($full_title_arr[0]);die;
        if ($title->mNamespace >= 5000 && $title->mNamespace <= 6000) {
            $this->_data['sidebarData'] = $this->getSidebarDataByNamespace($title->mNamespace, trim($title->mTextform), true);
        } else if ($title->mNamespace == 14) {
            $categoryMemberNamespaceId = $this->getNamespaceByCategoryMembers($title->getPrefixedText());
            $this->_data['sidebarData'] = $this->getSidebarDataByNamespace($categoryMemberNamespaceId, trim($title->mTextform), true);
        } else {
            global $wgExtraNamespaces;
            $this->_data['sidebarData'] = $this->getSidebarDataByNamespace(5000, $wgExtraNamespaces[5000], false);
        }
        
        $folder = 'sidebars/';
        $layout = 'sideBarObjects';
        
        $layout = 'sidebar';
        
        $this->_data['base_url'] = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        
        $ret = $this->ci_parser->parse($folder . $layout, $this->_data, true);
        
        if ($this->getDomElement() !== null) {
            
            $dom = $this->getDomElement()->ownerDocument;
            
            foreach ($this->getDomElement()->childNodes as $node) {
                $ret.= $dom->saveHTML($node);
            }
        }
        
        return $ret;
    }
    
    public function getSidebarDataByNamespace($namespace_id, $current_page_title, $isCustomNS) {
        global $wgExtraNamespaces;
        $dbr = wfGetDB(DB_SLAVE);
        $current_page_title = html_entity_decode($current_page_title);
        $table = $dbr->tableName('page');
        $tableJoin = $dbr->tableName('categorylinks');
        $res = $dbr->select(array($table, $tableJoin), array('*'), array('page_namespace' => $namespace_id), __METHOD__, array('ORDER BY' => 'menu_position DESC'), array($tableJoin => array('LEFT JOIN', array($table . '.page_id=' . $tableJoin . '.cl_from'))));
        $sidebarData = array();
        foreach ($res as $key => $row) {
            if ($row->cl_to != '') {
                $cat_title = str_replace('_', ' ', $row->cl_to);
                $sidebarData[$row->cl_to]['category_name'] = $cat_title;
                $sidebarData[$row->cl_to]['category_url'] = $row->cl_to;
                $page_title = str_replace('_', ' ', $row->page_title);

                if (!isset($sidebarData[$row->cl_to]['open_class']) && $isCustomNS == true) {
                    $sidebarData[$row->cl_to]['active'] = '';
                    if (($current_page_title == $page_title) || ($current_page_title == $cat_title)) {
                        $sidebarData[$row->cl_to]['open_class'] = 'open';
                        $sidebarData[$row->cl_to]['active'] = 'active';
                    }
                    if($current_page_title == $page_title) {
                        $sidebarData[$row->cl_to]['pages'][$key]['active_page'] = 'open active';
                    } else {
                        $sidebarData[$row->cl_to]['pages'][$key]['active_page'] = '';
                    }
                }else{
                    $sidebarData[$row->cl_to]['pages'][$key]['active_page'] = '';
                }
                $sidebarData[$row->cl_to]['pages'][$key]['namespace'] = $wgExtraNamespaces[$row->page_namespace];
                $sidebarData[$row->cl_to]['pages'][$key]['page_title'] = $page_title;
                $sidebarData[$row->cl_to]['pages'][$key]['page_url'] = urlencode($row->page_title);
            }
        }
//        var_dump($sidebarData);die;
        return $sidebarData;
    }
    /*
        GETs the namespace of the first member of a category
        assumes all members of a category belong to the same namespace
    */
    private function getNamespaceByCategoryMembers($categoryName) {
        // get CURL
        $curl = curl_init();
        // get that evil global request variable
        global $wgRequest;

        // /* $_GET Parameters to Send */
        $paramArray = array(
             'action' => 'query',
             'list' => 'categorymembers', // request cat members
             'format' => 'json',
             'cmtitle' => $categoryName,
             'cmtype' => 'page', // request cat members that are pages explicitly
             'cmlimit' => '500' // set the max limit for *users*
         );
        $params = new \DerivativeRequest(
            $wgRequest, $paramArray, true
        );
        $api = new \ApiMain($params, true); // default is false
        $api->execute();

        $data = &$api->getResultData();

        return $data['query']['categorymembers'][0]['ns'];
    }
}

