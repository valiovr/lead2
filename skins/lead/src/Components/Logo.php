<?php
/**
 * File holding the Logo class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup   Skins
 */

namespace Skins\lead\Components;

use Linker;
use Skins\lead\IdRegistry;

/**
 * The Logo class.
 *
 * The logo image as a link to the wiki main page wrapped in a div: <div id="p-logo" role="banner">
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class Logo extends Component {


	private $_data = array(
		'path_to_logo' => '/images/2/29/Logo_resized.png',

	);

	/**
	 * Builds the HTML code for this component
	 *
	 * @return String the HTML code
	 */

	public function getHtml() {
		$this->_data['base_url'] = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		global $wgExtraNamespaces;

		$title = $this->getSkin()->getTitle();


		$ret = $this->ci_parser->parse('LeadLogo', $this->_data, true);


		if ( $this->getDomElement() !== null ) {

			$dom = $this->getDomElement()->ownerDocument;

			foreach ( $this->getDomElement()->childNodes as $node ) {
				$ret .= $dom->saveHTML( $node );
			}
		}

		return $ret;
	}


//	public function getHtml() {
//
//		$attribs  = array_merge(
//			array( 'href' => $this->getSkinTemplate()->data[ 'nav_urls' ][ 'mainpage' ][ 'href' ] ),
//			Linker::tooltipAndAccesskeyAttribs( 'p-logo' )
//		);
//
//		$contents = \Html::element( 'img',
//			array(
//				'src' => $this->getSkinTemplate()->data[ 'logopath' ],
//				'alt' => $GLOBALS[ 'wgSitename' ]
//			)
//		);
//
//		return
//			$this->indent() . '<!-- logo and main page link -->' .
//			$this->indent() . \Html::openElement( 'div',
//				array(
//					'id'    => IdRegistry::getRegistry()->getId( 'p-logo' ),
//					'class' => 'p-logo ' . $this->getClassString(),
//					'role'  => 'banner'
//				)
//			) .
//			$this->indent( 1 ) . \Html::rawElement( 'a', $attribs, $contents ) .
//			$this->indent( -1 ) . '</div>' . "\n";
//	}

}
