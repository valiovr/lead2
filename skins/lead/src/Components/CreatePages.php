<?php
/**
* File holding the Html class
*
* This file is part of the MediaWiki skin lead.
*
* @copyright 2013 - 2014, Stephan Gambke
* @license   GNU General Public License, version 3 (or any later version)
*
* The lead skin is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* The lead skin is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @file
* @ingroup Skins
*/
namespace Skins\lead\Components;
/**
* The Html class.
*
* This component allows insertion of raw HTML into the page.
*
* @author Stephan Gambke
* @since 1.0
* @ingroup Skins
*/
class CreatePages extends Component
{

    const PATH_TO_API = '/api.php';

    /**
    * Builds the HTML code for the main container
    *
    * @return String the HTML code
    */
    public function getHtml() {
        // global $wgUser;
        // $token = $wgUser->editToken();
        // // Get cURL resource
        // $curl = curl_init();

        // /* $_GET Parameters to Send */
        // $params = array(
            //     'prop' => 'query',
            //     'action' => 'edit',
            //     'format' => 'jsonfm',
            //     'title' => 'Gosho',
            //     'text' => '[[Category:Gosho]] blqblqqqqqqqqqqq',
            //     'User-Agent' => "Lead.info/1.0 (http://leadingpractice.com; dobromir@orpheus.bg) Lead/1.0",
            //     'token' => $token);
        // var_dump($token);
        // /*
            //     get API address
        // */
        // global $wgServer;
        // $baseUrl = $wgServer;
        // // $url = $baseUrl . self::PATH_TO_API;
        // $url = 'mediawiki.dev' . self::PATH_TO_API;
        // // var_dump(http_build_query($params));die;
        // // set url
        // curl_setopt($curl, CURLOPT_URL, $url);
        // // set post request
        // curl_setopt($curl, CURLOPT_POST, 1);
        // // set post body
        // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));

        // curl_setopt($curl, CURLOPT_VERBOSE, 1);
        // curl_setopt($curl, CURLOPT_HEADER, 1);

        // /* Tell cURL to return the output */
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // // Send the request & save response to $resp
        // $resp = curl_exec($curl);

        // //  Check HTTP Code
        // $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // print_r($resp);
        // // Close request to clear up some resources
        $pageCategories = array();

        $pageCategories['OBJECTS'] = array();
        $pageCategories['OBJECTS']['Objects'] = array();
        $pageCategories['OBJECTS']['Enterprise Ontology'] = array();
        $pageCategories['OBJECTS']['Enterprise Semantics'] = array();

        $pageCategories['MAPS'] = array();
        $pageCategories['MAPS']['Maps'] = array();

        $pageCategories['MATRICES'] = array();
        $pageCategories['MATRICES']['Matrices'] = array();

        $pageCategories['MODELS'] = array();
        $pageCategories['MODELS']['Models'] = array();

        $pageCategories['LAYERS'] = array();
        $pageCategories['LAYERS']['Business Layer'] = array();
        $pageCategories['LAYERS']['Business Layer'] = array();
        $pageCategories['LAYERS']['Business Layer'] = array();
        $pageCategories['LAYERS']['Business Layer'] = array();
        $pageCategories['LAYERS']['Application Layer'] = array();
        $pageCategories['LAYERS']['Application Layer'] = array();
        $pageCategories['LAYERS']['Technology Layer'] = array();
        $pageCategories['LAYERS']['Technology Layer'] = array();

        $pageCategories['PLAN'] = array();
        $pageCategories['PLAN']['Plans']  = array();

        $pageCategories['PLAN'] = array();
        $pageCategories['PLAN']['Plans'] = array();

        $pageCategories['ENTERPRISE STANDARDS'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards'] = array();
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards'] = array();

        $pageCategories['INDUSTRY STANDARDS'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Financial Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Consumer Services Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Energy Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Healthcare Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Utilities Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Transportation Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Telecommunication Standards & User Groups'] = array();
        $pageCategories['INDUSTRY STANDARDS']['Information Technology Standards & User Groups'] = array();

        $pageCategories['OBJECTS']['Objects']['About Meta Objects'] = '';
        $pageCategories['OBJECTS']['Objects']['Meta Object Descriptions'] = '';
        $pageCategories['OBJECTS']['Objects']['Meta Object Nature'] = '';
        $pageCategories['OBJECTS']['Objects']['Enterprise Notations'] = '';
        $pageCategories['OBJECTS']['Objects']['Business Model Notations'] = '';
        $pageCategories['OBJECTS']['Objects']['Public Sector Heat Maps'] = '';
        $pageCategories['OBJECTS']['Objects']['X-BPMN Notations'] = '';
        $pageCategories['OBJECTS']['Objects']['Organizational Role Notations'] = '';
        $pageCategories['OBJECTS']['Objects']['Social Media Notations'] = '';
        $pageCategories['OBJECTS']['Enterprise Ontology']['About Enterprise'] = '';
        $pageCategories['OBJECTS']['Enterprise Ontology']['Ontology Rules'] = '';
        $pageCategories['OBJECTS']['Enterprise Ontology']['Meta Meta Model'] = '';
        $pageCategories['OBJECTS']['Enterprise Ontology']['Meta Models'] = '';
        $pageCategories['OBJECTS']['Enterprise Ontology']['Models'] = '';
        $pageCategories['OBJECTS']['Enterprise Semantics']['About Enterprise Semantics'] = '';
        $pageCategories['OBJECTS']['Enterprise Semantics']['Semantic Rules'] = '';
        $pageCategories['OBJECTS']['Enterprise Semantics']['The Semantic Relationships of Meta Objects'] = '';
        $pageCategories['MAPS']['Maps']['Forces & Drivers Map'] = '';
        $pageCategories['MAPS']['Maps']['Vision, Mission & Goals Map'] = '';
        $pageCategories['MAPS']['Maps']['Requirement Map'] = '';
        $pageCategories['MAPS']['Maps']['Stakeholder Map'] = '';
        $pageCategories['MAPS']['Maps']['Strategy Map'] = '';
        $pageCategories['MAPS']['Maps']['Value Map'] = '';
        $pageCategories['MAPS']['Maps']['Balanced Scorecard Map'] = '';
        $pageCategories['MAPS']['Maps']['Performance Map'] = '';
        $pageCategories['MAPS']['Maps']['Measurement & Reporting Map'] = '';
        $pageCategories['MAPS']['Maps']['Competency/Business Model '] = '';
        $pageCategories['MAPS']['Maps']['Revenue Map'] = '';
        $pageCategories['MAPS']['Maps']['Cost Map'] = '';
        $pageCategories['MAPS']['Maps']['Operating  Map'] = '';
        $pageCategories['MAPS']['Maps']['Information Map'] = '';
        $pageCategories['MAPS']['Maps']['Role Map'] = '';
        $pageCategories['MAPS']['Maps']['Owner Map'] = '';
        $pageCategories['MAPS']['Maps']['Organizational Chart Map'] = '';
        $pageCategories['MAPS']['Maps']['Object Map'] = '';
        $pageCategories['MAPS']['Maps']['Workflow Map'] = '';
        $pageCategories['MAPS']['Maps']['Rule Map'] = '';
        $pageCategories['MAPS']['Maps']['Risk Map'] = '';
        $pageCategories['MAPS']['Maps']['Security Map'] = '';
        $pageCategories['MAPS']['Maps']['Case Map'] = '';
        $pageCategories['MAPS']['Maps']['Channel Map'] = '';
        $pageCategories['MAPS']['Maps']['Media Map'] = '';
        $pageCategories['MAPS']['Maps']['Process Map'] = '';
        $pageCategories['MAPS']['Maps']['Service Map'] = '';
        $pageCategories['MAPS']['Maps']['Application Map'] = '';
        $pageCategories['MAPS']['Maps']['Application Service Map'] = '';
        $pageCategories['MAPS']['Maps']['Application Roles Map'] = '';
        $pageCategories['MAPS']['Maps']['Application Rules Map'] = '';
        $pageCategories['MAPS']['Maps']['System Measurements/Report'] = '';
        $pageCategories['MAPS']['Maps']['Application Interface Map'] = '';
        $pageCategories['MAPS']['Maps']['Application Screen Map'] = '';
        $pageCategories['MAPS']['Maps']['Compliance Map'] = '';
        $pageCategories['MAPS']['Maps']['Data Map'] = '';
        $pageCategories['MAPS']['Maps']['Data Service Map'] = '';
        $pageCategories['MAPS']['Maps']['Data Rules Map'] = '';
        $pageCategories['MAPS']['Maps']['Platform Map'] = '';
        $pageCategories['MAPS']['Maps']['Platform Service Map'] = '';
        $pageCategories['MAPS']['Maps']['Platform Rules Map'] = '';
        $pageCategories['MAPS']['Maps']['Platform Distribution Map'] = '';
        $pageCategories['MAPS']['Maps']['Infrastructure Map'] = '';
        $pageCategories['MAPS']['Maps']['Infrastructure Service Map'] = '';
        $pageCategories['MAPS']['Maps']['Infrastructure Rules Map'] = '';
        $pageCategories['MATRICES']['Matrices']['Forces & Drivers Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Vision, Mission & Goals Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Requirement Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Stakeholder Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Strategy Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Value Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Balanced Scorecard Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Performance Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Measurement & Reporting Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Competency/Business Model Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Revenue Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Cost Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Operating Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Information Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Role Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Owner Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Organizational Chart Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Object Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Workflow Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Rule Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Risk Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Security Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Case Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Channel Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Media Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Process Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['BPM Notations Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Service Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Service Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Roles Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Rules Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['System Measurements/Reporting Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Interface Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Application Screen Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Compliance Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Data Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Data Service Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Data Rules Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Platform Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Platform Service Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Platform Rules Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Platform Distribution Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Infrastructure Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Infrastructure Service Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Infrastructure Rules Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['Virtualization Matrix'] = '';
        $pageCategories['MATRICES']['Matrices']['High Availability Matrix'] = '';
        $pageCategories['MODELS']['Models']['Forces & Drivers Model'] = '';
        $pageCategories['MODELS']['Models']['Vision, Mission & Goals Model'] = '';
        $pageCategories['MODELS']['Models']['Requirement Model'] = '';
        $pageCategories['MODELS']['Models']['Stakeholder Model'] = '';
        $pageCategories['MODELS']['Models']['Strategy Model'] = '';
        $pageCategories['MODELS']['Models']['Value Model'] = '';
        $pageCategories['MODELS']['Models']['Balanced Scorecard Model'] = '';
        $pageCategories['MODELS']['Models']['Performance Model'] = '';
        $pageCategories['MODELS']['Models']['Measurement & Reporting Model'] = '';
        $pageCategories['MODELS']['Models']['Competency/Business Model Model'] = '';
        $pageCategories['MODELS']['Models']['Revenue Model'] = '';
        $pageCategories['MODELS']['Models']['Cost Model'] = '';
        $pageCategories['MODELS']['Models']['Operating Model'] = '';
        $pageCategories['MODELS']['Models']['Information Model'] = '';
        $pageCategories['MODELS']['Models']['Role Model'] = '';
        $pageCategories['MODELS']['Models']['Owner Model'] = '';
        $pageCategories['MODELS']['Models']['Organizational Chart Model'] = '';
        $pageCategories['MODELS']['Models']['Object Model'] = '';
        $pageCategories['MODELS']['Models']['Workflow Model'] = '';
        $pageCategories['MODELS']['Models']['Rule Model'] = '';
        $pageCategories['MODELS']['Models']['Risk Model'] = '';
        $pageCategories['MODELS']['Models']['Security Model'] = '';
        $pageCategories['MODELS']['Models']['Case Model'] = '';
        $pageCategories['MODELS']['Models']['Channel Model'] = '';
        $pageCategories['MODELS']['Models']['Media Model'] = '';
        $pageCategories['MODELS']['Models']['Process Model'] = '';
        $pageCategories['MODELS']['Models']['BPM Notations Model'] = '';
        $pageCategories['MODELS']['Models']['Service Model'] = '';
        $pageCategories['MODELS']['Models']['Application Model'] = '';
        $pageCategories['MODELS']['Models']['Application Service Model'] = '';
        $pageCategories['MODELS']['Models']['Application Rules Model'] = '';
        $pageCategories['MODELS']['Models']['System Measurements/Reporting Model'] = '';
        $pageCategories['MODELS']['Models']['Application Interface Model'] = '';
        $pageCategories['MODELS']['Models']['Application Screen Model'] = '';
        $pageCategories['MODELS']['Models']['Compliance Model'] = '';
        $pageCategories['MODELS']['Models']['Data Model'] = '';
        $pageCategories['MODELS']['Models']['Data Service Model'] = '';
        $pageCategories['MODELS']['Models']['Data Rules Model'] = '';
        $pageCategories['MODELS']['Models']['Platform Model'] = '';
        $pageCategories['MODELS']['Models']['Platform Service Model'] = '';
        $pageCategories['MODELS']['Models']['Platform Rules Model'] = '';
        $pageCategories['MODELS']['Models']['Platform Distribution Model'] = '';
        $pageCategories['MODELS']['Models']['Infrastructure Model'] = '';
        $pageCategories['MODELS']['Models']['Infrastructure Service Model'] = '';
        $pageCategories['MODELS']['Models']['Infrastructure Rules Model'] = '';
        $pageCategories['MODELS']['Models']['Virtualization Model'] = '';
        $pageCategories['MODELS']['Models']['High Availability Model'] = '';
        $pageCategories['LAYERS']['Business Layer']['Purpose & Goal (Value) Sub-layer'] = '';
        $pageCategories['LAYERS']['Business Layer']['Business Competency Sub-layer'] = '';
        $pageCategories['LAYERS']['Business Layer']['Business Service Sub-layer'] = '';
        $pageCategories['LAYERS']['Business Layer']['Business Process Sub-layer'] = '';
        $pageCategories['LAYERS']['Application Layer']['Application Sub-layer'] = '';
        $pageCategories['LAYERS']['Application Layer']['Data Sub-layer'] = '';
        $pageCategories['LAYERS']['Technology Layer']['Platform Sub-layer'] = '';
        $pageCategories['LAYERS']['Technology Layer']['Infrastructure Sub-layer'] = '';
        $pageCategories['PLAN']['Plans']['Portfolio & Program Management'] = '';
        $pageCategories['PLAN']['Plans']['Project Management'] = '';
        $pageCategories['PLAN']['Plans']['Lifecycle Management'] = '';
        $pageCategories['PLAN']['Plans']['Governance Management'] = '';
        $pageCategories['PLAN']['Plans']['Transformation Management'] = '';
        $pageCategories['PLAN']['Plans']['Innovation Management'] = '';
        $pageCategories['PLAN']['Plans']['Change Management'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Strategy Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Growth: Core Differentiating & Core Competitive Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Value Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Performance Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Executive Communication & Story Telling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Evaluation & Audit Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Planning Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Procurement Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Human Resource Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Production Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Product Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Marketing Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Sales & Customer Service Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Call Center Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Supply Chain & Logistics Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Compliance Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Risk Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Governance Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Portfolio Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Program Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Project Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Financial Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Risk Ontology Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Management Standards']['Policy Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Ontology Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Drivers & Forces (external/internal) Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Stakeholder Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Requirement Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Business Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Business Process Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Revenue Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Value Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Service Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Performance Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Operating Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Cost Model Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Role Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Competency Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Measurement Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Workflow Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Channel Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Capability Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Enterprise Sustainability Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Case Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Modelling Standards']['Business Ontology Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Decomposition & Composition Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Lifecycle Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Testing Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Enterprise Requirement Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Quality Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Geographical Information System (GIS) Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Agile Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Categorization & Classification Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Engineering Standards']['Enterprise Tier Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Layered Enterprise Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Business Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Value Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Process Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Service-Oriented Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Application Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Information Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Data Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Platform Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Infrastructure Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['EA Governance Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Security Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Architecture Standards']['Cloud Architecture Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['IT Strategy Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Business Model of IT Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['IT Process Map Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['IT Center of Competency Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Cloud Computing Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Cyber Security Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Knowledge Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Analytic Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Reporting Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Application Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Application Modernization & Optimization Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['ERP Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Software Testing Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Information Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Data Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Rule Modelling Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Service-Oriented Computing Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Platform Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Infrastructure Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Social Media Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Blueprinting Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Implementation Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Information & Technology Standards']['Cloud Ontology Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Alignment & Unity Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Change Management Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Maturity Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Continuous Improvement Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Organizational Development Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Optimization Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Effectiveness Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Efficiency Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Re-engineering Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Root Cause Analysis Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Transformation Benchmarking Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Innovation Reference Content'] = '';
        $pageCategories['ENTERPRISE STANDARDS']['Enterprise Transformation & Innovation Standards']['Alignment of Portfolio & Program & Project Management Reference Content'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Financial Standards & User Groups']['Central Bank'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Financial Standards & User Groups']['Commercial Bank'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Financial Standards & User Groups']['Insurance'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Financial Standards & User Groups']['Real Estate'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Aerospace & Defense'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Automotive'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Chemicals'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Forestry & Paper'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Mining'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Construction & Materials'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Electronic & Electrical Equipment'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Industrial Standards & User Groups']['Industrial Engineering'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups']['Automobiles & Parts'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups']['Food'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups']['Beverage'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups']['Tobacco'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Goods Standards & User Groups']['Consumer Durable & Apparel Goods'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Services Standards & User Groups']['Retail'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Services Standards & User Groups']['Media'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Services Standards & User Groups']['Postal'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Consumer Services Standards & User Groups']['Travel & Leisure'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Energy Standards & User Groups']['Oil & Gas'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Energy Standards & User Groups']['Oil & Gas Producers'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Energy Standards & User Groups']['Oil Equipment & Services & Distribution'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Energy Standards & User Groups']['Alternative Energy'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Defence (Public)'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Finance & Treasury'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Customs & Border Services'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Foreign Affairs & Trade'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Health'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Agriculture & Food'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Labor & Social Services'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Secret Intelligence Services'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Social Services'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Energy & Natural Resources'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Education'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Environment'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Tourism'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Transport & Infrastructure'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Justice'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Culture'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Government Standards & User Groups']['Local Government'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Healthcare Standards & User Groups']['Health Care Services & Equipment'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Healthcare Standards & User Groups']['Pharmaceuticals & Biotechnology'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Healthcare Standards & User Groups']['Life-science'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Utilities Standards & User Groups']['Electricity Utilities'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Utilities Standards & User Groups']['Gas & Water & Multi-utilities'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Utilities Standards & User Groups']['Power Producers'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Transportation Standards & User Groups']['Airline'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Transportation Standards & User Groups']['Railway'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Transportation Standards & User Groups']['Shipping'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Telecommunication Standards & User Groups']['Telecommunication Standards & User Groups'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Information Technology Standards & User Groups']['Software & Services'] = '';
        $pageCategories['INDUSTRY STANDARDS']['Information Technology Standards & User Groups']['Technology Hardware & Equipment'] = '';
        // curl_close($curl);
        global $wgUser;
        global $wgRequest;
         ini_set('display_errors', 'On');
        error_reporting(E_ALL);

        // $curl = curl_init();
        // // /* $_GET Parameters to Send */
        // $params = array(
        //      'prop' => 'info',
        //      'action' => 'query',
        //      'format' => 'jsonfm',
        //      'titles' => 'Objects:About_Enterprise',
        //      // 'text' => '[[Category:Gosho]] blqblqqqqqqqqqqq',
        //      // 'User-Agent' => "Lead.info/1.0 (http://leadingpractice.com; dobromir@orpheus.bg) Lead/1.0",
        //      // 'token' => $token);
        //      );
        // var_dump($token);
        // /*
        //  //     get API address
        // */
        // global $wgServer;
        // $baseUrl = $wgServer;
        // // $url = $baseUrl . self::PATH_TO_API;
        // $url = 'mediawiki.dev' . self::PATH_TO_API;
        // // var_dump(http_build_query($params));die;
        // // set url
        // curl_setopt($curl, CURLOPT_URL, $url);
        // // set post request
        // curl_setopt($curl, CURLOPT_POST, 1);
        // // set post body
        // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));

        // curl_setopt($curl, CURLOPT_VERBOSE, 1);
        // curl_setopt($curl, CURLOPT_HEADER, 1);

        // /* Tell cURL to return the output */
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // // Send the request & save response to $resp
        // $resp = curl_exec($curl);

        // //  Check HTTP Code
        // $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // print_r($resp);
        // // Close request to clear up some resources
        // die;
        // foreach($pageCategories as $namespace => $secondLevelArray) {
        //     $params = new \DerivativeRequest(
        //             $wgRequest, array(
        //             'action' => 'edit',
        //             // 'prop' => 'categories',
        //             'title'=> $namespace . ':Main',
        //              'text' => '{{FULLPAGENAME}}',
        //             // 'ns' => $namespace,
        //             'token'=> $token
        //         ), true
        //     );
        //     $api = new \ApiMain($params, true); // default is false
        //     $api->execute();
        //     $data = &$api->getResultData();
        //     echo '<pre>';
        //     print_r($data);
        //     // $namespaces are created in LocalSettings.php
        //     foreach($secondLevelArray as $secondLevelCategory => $thirdLevelArray) {
        //         $params = new \DerivativeRequest(
        //                 $wgRequest, array(
        //                 'action' => 'edit',
        //                 // 'prop' => 'categories',
        //                 'title'=> $namespace . ':' . $secondLevelCategory,
        //                 'text' => '{{FULLPAGENAME}}',
        //                 // 'ns' => $namespace,
        //                 'token'=> $token
        //             ), true
        //         );
        //         $api = new \ApiMain($params, true); // default is false
        //         $api->execute();
        //         $params = new \DerivativeRequest(
        //                 $wgRequest, array(
        //                 'action' => 'edit',
        //                 // 'prop' => 'categories',
        //                 'title'=> 'Category:' . $secondLevelCategory,
        //                 // 'text' => 'Category:' . $firstLevelCategory,
        //                 'text' => '{{FULLPAGENAME}}',
        //                 // 'ns' => $namespace,
        //                 'token'=> $token
        //             ), true
        //         );
        //         $api = new \ApiMain($params, true); // default is false
        //         $api->execute();
        //         foreach($thirdLevelArray as $thirdLevelCategory => $emptyString) {
        //             $params = new \DerivativeRequest(
        //                     $wgRequest, array(
        //                     'action' => 'edit',
        //                     // 'prop' => 'categories',
        //                     'title'=>$namespace . ':' . $thirdLevelCategory,
        //                     'text' => '[[Category:' . $secondLevelCategory . ']]',
        //                     'token'=> $token
        //                 ), true
        //             );
        //             $api = new \ApiMain($params, true); // default is false
        //             $api->execute();
        //         }
        //     }

        // }
    }
}