<?php
/**
 * File holding the Html class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Components;
ini_set('display_errors', 'On');
error_reporting(E_ALL);
/**
 * The Html class.
 *
 * This component allows insertion of raw HTML into the page.
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class TopMenuLead extends Component {
	// private $baseUrl = ;

	private $_data = array(
		'path_to_logo' => '/images/2/29/Logo_resized.png',
		
	);
	/**
	 * Builds the HTML code for the main container
	 *
	 * @return String the HTML code
	 */
	public function getHtml() {
		$this->_data['base_url'] = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        global $wgExtraNamespaces;
//        echo '<pre>';
//        var_dump($wgExtraNamespaces);die;

        $title = $this->getSkin()->getTitle();

       // $this->generateMainMenuData($fbfgh);

		$ret = $this->ci_parser->parse('headerTopMenu', $this->_data, true);

		
		if ( $this->getDomElement() !== null ) {

			$dom = $this->getDomElement()->ownerDocument;

			foreach ( $this->getDomElement()->childNodes as $node ) {
				$ret .= $dom->saveHTML( $node );
			}
		}

		return $ret;
	}

    private function _getPageCategory($title){
//        global $wgRequest;

//        $dbr = wfGetDB( DB_SLAVE );
//        $res = $dbr->select(
//            'category',                                   // $table
//            array( 'cat_title', 'cat_pages' ),            // $vars (columns of the table)
//            '',                              // $conds
//            __METHOD__,                                   // $fname = 'Database::select',
//            array( 'ORDER BY' => 'cat_title ASC','LIMIT'=>99999999 )        // $options = array()
//        );
//        foreach( $res as $row ) {
//            $output .= 'Category ' . $row->cat_title . ' contains ' . $row->cat_pages . " entries.\n";
//        }


//        echo '<pre>';
//        var_dump($output); die;

//        $params = new \DerivativeRequest(
//            $wgRequest, array(
//            'action' => 'query',
//            'prop' => 'categories',
//            'titles'=>$title->getPrefixedText(),
//        ), true
//        );

//        $params = new \DerivativeRequest(
//            $wgRequest, array(
//            'action' => 'query',
//            'list' => 'allpages',
//            'apnamespace'=>$title->mNamespace,
//            'generator'=>'categories',
//            'aplimit'=>999999999999,
//        ), true
//        );

//        $api = new \ApiMain($params, true); // default is false
//        $api->execute();

//        $data = &$api->getResultData();
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>'; die;
//        foreach($data['query']['pages'] as $page){
//            $category = $page['categories'][0]['title'];
//        }
//        return $category;
    }


    public function generateMainMenuData( $namespace ) {
        $dbr   = wfGetDB(DB_SLAVE);
        $list  = array();
        $table = $dbr->tableName( 'page' );
        $tableJoin = $dbr->tableName( 'categorylinks' );
        $res   = $dbr->select( array($table, $tableJoin),
            array('*'),
            array('page_namespace'=>$namespace),
            __METHOD__,
            array(),
            array($tableJoin => array( 'LEFT JOIN', array(
            $table.'.page_id='.$tableJoin.'.cl_from' ) )) );

        foreach( $res as $row ){
            echo '<pre>';var_dump($row);
        }// $list[] = $row->page_title;
        return $list;
    }
}
