<?php
/**
 * File holding the Html class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Components;

/**
 * The Html class.
 *
 * This component allows insertion of raw HTML into the page.
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class ToolbarTools extends Component {
	// private $baseUrl = ;

	private $_data = array(
		'path_to_logo' => '/images/2/29/Logo_resized.png',
		
	);
	/**
	 * Builds the HTML code for the main container
	 *
	 * @return String the HTML code
	 */
	public function getHtml() {
		$this->_data['base_url'] = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$ret = $this->ci_parser->parse('toolBarTools', $this->_data, true);
		
		
		if ( $this->getDomElement() !== null ) {

			$dom = $this->getDomElement()->ownerDocument;

			foreach ( $this->getDomElement()->childNodes as $node ) {
				$ret .= $dom->saveHTML( $node );
			}
		}

		return $ret;
	}

}
