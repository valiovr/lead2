<?php
/**
 * File containing the SetupAfterCache class
 *
 * This file is part of the MediaWiki skin lead.
 *
 * @copyright 2013 - 2014, Stephan Gambke, mwjames
 * @license   GNU General Public License, version 3 (or any later version)
 *
 * The lead skin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * The lead skin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 * @ingroup Skins
 */

namespace Skins\lead\Hooks;

use Bootstrap\BootstrapManager;
use RuntimeException;

/**
 * @see https://www.mediawiki.org/wiki/Manual:Hooks/SetupAfterCache
 *
 * @since 1.0
 *
 * @author mwjames
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class SetupAfterCache {

	protected $bootstrapManager = null;
	protected $configuration = array();

	/**
	 * @since  1.0
	 *
	 * @param BootstrapManager $bootstrapManager
	 * @param array $configuration
	 */
	public function __construct( BootstrapManager $bootstrapManager, array &$configuration ) {
		$this->bootstrapManager = $bootstrapManager;
		$this->configuration = &$configuration;
	}

	/**
	 * @since  1.0
	 *
	 * @return self
	 */
	public function process() {

		$this->setInstallPaths();
		$this->addLateSettings();
		$this->registerCommonBootstrapModules();
		$this->registerExternalLessModules();
		$this->registerExternalLessVariables();

		return $this;
	}

	/**
	 * @since 1.0
	 *
	 * @param array $configuration
	 */
	public function adjustConfiguration( array &$configuration ) {

		foreach ( $this->configuration as $key => $value ) {
			$configuration[ $key ] = $value;
		}
	}

	/**
	 * Set local and remote base path of the lead skin
	 */
	protected function setInstallPaths() {

		$dirParts = explode( DIRECTORY_SEPARATOR, __DIR__ );
		array_pop( $dirParts );
		array_pop( $dirParts );

		$this->configuration[ 'leadLocalPath' ] = implode( '/', $dirParts );
		$this->configuration[ 'leadRemotePath' ] = str_replace( $this->configuration[ 'IP' ], $this->configuration[ 'wgScriptPath' ], $this->configuration[ 'leadLocalPath' ] );
	}

	protected function addLateSettings() {

		// if Visual Editor is installed and there is a setting to enable or disable it
		if ( $this->hasConfiguration( 'wgVisualEditorSupportedSkins' ) && $this->hasConfiguration( 'egleadEnableVisualEditor' ) ) {

			// if VE should be enabled
			if ( $this->configuration[ 'egleadEnableVisualEditor' ] === true ) {

				// if lead is not yet in the list of VE-enabled skins
				if ( !in_array( 'lead', $this->configuration[ 'wgVisualEditorSupportedSkins' ] ) ) {
					$this->configuration[ 'wgVisualEditorSupportedSkins' ][ ] = 'lead';
				}

			} else {
				// remove all entries of lead from the list of VE-enabled skins
				$this->configuration[ 'wgVisualEditorSupportedSkins' ] = array_diff(
					$this->configuration[ 'wgVisualEditorSupportedSkins' ],
					array( 'lead' )
				);
			}
		}

		$this->configuration[ 'wgResourceModules' ][ 'skin.lead.jquery-sticky' ] = array(
			'localBasePath' => $this->configuration[ 'leadLocalPath' ] . '/resources/js',
			'remoteBasePath' => $this->configuration[ 'leadRemotePath' ] . '/resources/js',
			'group' => 'skin.lead',
			'skinScripts' => array( 'lead' => array( 'jquery-sticky/jquery.sticky.js', 'Components/Modifications/sticky.js' ) )
		);

	}

	protected function registerCommonBootstrapModules() {

		$this->bootstrapManager->addAllBootstrapModules();

		if ( file_exists( $this->configuration[ 'wgStyleDirectory' ] . '/common/shared.css' ) ) { // MW < 1.24
			$this->bootstrapManager->addExternalModule(
				$this->configuration[ 'wgStyleDirectory' ] . '/common/shared.css',
				$this->configuration[ 'wgStylePath' ] . '/common/'
			);
		} else {
			if ( file_exists( $this->configuration[ 'IP' ] . '/resources/src/mediawiki.legacy/shared.css' ) ) { // MW >= 1.24
				$this->bootstrapManager->addExternalModule(
					$this->configuration[ 'IP' ] . '/resources/src/mediawiki.legacy/shared.css',
					$this->configuration[ 'wgScriptPath' ] . '/resources/src/mediawiki.legacy/'
				);
			}
		}

		$this->bootstrapManager->addExternalModule(
			$this->configuration[ 'leadLocalPath' ] . '/resources/styles/core.less',
			$this->configuration[ 'leadRemotePath' ] . '/resources/styles/'
		);
	}

	protected function registerExternalLessModules() {

		if ( $this->hasConfigurationOfTypeArray( 'egleadExternalStyleModules' ) ) {

			foreach ( $this->configuration[ 'egleadExternalStyleModules' ] as $localFile => $remotePath ) {

				list( $localFile, $remotePath ) = $this->matchAssociativeElement( $localFile, $remotePath );

				$this->bootstrapManager->addExternalModule(
					$this->isReadableFile( $localFile ),
					$remotePath
				);
			}
		}
	}

	protected function registerExternalLessVariables() {

		if ( $this->hasConfigurationOfTypeArray( 'egleadExternalLessVariables' ) ) {

			foreach ( $this->configuration[ 'egleadExternalLessVariables' ] as $key => $value ) {
				$this->bootstrapManager->setLessVariable( $key, $value );
			}
		}
	}

	/**
	 * @param $id
	 * @return bool
	 */
	private function hasConfiguration( $id ) {
		return isset( $this->configuration[ $id ] );
	}

	/**
	 * @param string $id
	 * @return bool
	 */
	private function hasConfigurationOfTypeArray( $id ) {
		return $this->hasConfiguration( $id ) && is_array( $this->configuration[ $id ] );
	}

	/**
	 * @param $localFile
	 * @param $remotePath
	 * @return array
	 */
	private function matchAssociativeElement( $localFile, $remotePath ) {

		if ( is_integer( $localFile ) ) {
			return array( $remotePath, '' );
		}

		return array( $localFile, $remotePath );
	}

	/**
	 * @param string $file
	 * @return string
	 */
	private function isReadableFile( $file ) {

		if ( is_readable( $file ) ) {
			return $file;
		}

		throw new RuntimeException( "Expected an accessible {$file} file" );
	}

}
