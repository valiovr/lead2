/**
 * Created by Miroslav Marinov on 04.09.2015.
 */
'use strict';

/**
 * @ngdoc function
 * @name LeadApp.controller:MyObjectModifyController
 * @description
 * # MyObjectModifyController
 * Controller of the LeadApp
 */
app.controller('MyObjectModifyController', function ($http, $scope, $rootScope, $modalInstance, $translate, config, ApiService, Flash, id, user_object) {

    $scope.formProcessing = false;
    $scope.user_object = {};
    $scope.types = {};
    $scope.subtypes = {};

    /**
     * Init controller default state
     */
    var init = function () {
        $scope.id = id;
        $scope.user_object = user_object;

        fillFormData($scope.user_object);
    };

    var fillFormData = function (user_object_data) {
        var stereotype_id = user_object_data.stereotype_id;
        var type_id = user_object_data.type_id;

        if (user_object_data.stereotype_id != null) {
            angular.forEach(user_object_data.meta_object.stereotype, function (value, key) {
                if (value.id == stereotype_id) {
                    $scope.types = value.children;
                    return false;
                }
            });


            if ($scope.types != null) {
                angular.forEach($scope.types, function (value, key) {
                    if (value.id == type_id) {
                        $scope.subtypes = value.children;
                        return false;
                    }
                });
            }
        }
    };

    /**
     * Cancel Revert Modal Window
     */
    $scope.cancelObjectModify = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.modifyUserObject = function(user_object){
        $scope.formProcessing = true;

        var modified_data = {};
        modified_data.id = user_object.id;
        modified_data.name = user_object.name;
        modified_data.icon = user_object.icon;
        modified_data.stereotype_id = user_object.stereotype_id? parseInt(user_object.stereotype_id):'';
        modified_data.type_id = user_object.type_id ? parseInt(user_object.type_id):'';
        modified_data.subtype_id = user_object.subtype_id ? parseInt(user_object.subtype_id):'';

        ApiService.post(config.api.url + config.urlSep + 'user_object_modify', modified_data)
            .then(function (data, status, headers, config) {
                $scope.formProcessing = false;

                if (typeof data.success !='undefined' && data.success == 'success_user_object_modified') {
                    var success_message = $translate.instant('success_object_modification_reverted');
                    Flash.create('success', success_message, 'formMessage');
                }else{
                    var authorization_error_message = $translate.instant('user_object_modification_error');
                    Flash.create('danger', authorization_error_message, 'formMessage');
                }
                $modalInstance.close(data);

            }), function (data, status, headers, config) {
            $scope.formProcessing = false;
            console.log(data);
            console.log(status);
        };
    };


    /**
     * Watch for changes of User Object Stereotype
     */
    $scope.$watch('user_object.stereotype_id', function (new_val, old_val) {
        if(new_val !=old_val){
           if(new_val==null ||new_val==''){
               $scope.types = {};
               $scope.subtypes = {};

           }
            $scope.user_object.type_id = null;
            $scope.user_object.subtype_id = null;

            angular.forEach($scope.user_object.meta_object.stereotype, function (value, key) {
                if (value.id == new_val) {
                    $scope.types = value.children;
                    return false;
                }
            });
        }

    });

    /**
     * Watch for changes of User Object Type
     */
    $scope.$watch('user_object.type_id', function (new_val, old_val) {
        if(new_val !=old_val){
            if(new_val==null ||new_val==''){
                $scope.subtypes = {};
            }
            $scope.user_object.subtype_id = null;
            if ($scope.types != null) {
                angular.forEach($scope.types, function (value, key) {
                    if (value.id == new_val) {
                        $scope.subtypes = value.children;
                        return false;
                    }
                });
            }
        }
    });

    //init controller
    init();
});