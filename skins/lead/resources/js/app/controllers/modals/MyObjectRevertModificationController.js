/**
 * Created by Miroslav Marinov on 03.09.2015.
 */
'use strict';

/**
 * @ngdoc function
 * @name LeadApp.controller:MyObjectRevertModificationController
 * @description
 * # MyObjectRevertModificationController
 * Controller of the LeadApp
 */
app.controller('MyObjectRevertModificationController', function ($http, $scope, $modalInstance, $translate, config, ApiService, Flash, id, user_id) {

    $scope.formProcessing = false;

    /**
     * Init controller default state
     */
    var init = function () {
        $scope.id = id;
        $scope.user_id = user_id;
    };

    /**
     * Cancel Revert Modal Window
     */
    $scope.cancelRevertModification = function () {
        $modalInstance.dismiss('cancel');
    };
    /**
     * Revert User Object Modification
     * @param id
     */
    $scope.revertUserObjectModification = function (id) {
        $scope.formProcessing = true;
        var params = {"id": id, "user_id": $scope.user_id};
        ApiService.post(config.api.url + config.urlSep + 'revert_user_object_modification', params)
            .then(function (data, status, headers, config) {
                $scope.formProcessing = false;

                if (typeof data.success !='undefined' && data.success == 'success_object_modification_reverted') {
                    var success_message = $translate.instant('success_object_modification_reverted');
                    Flash.create('success', success_message, 'formMessage');
                }
                else if (typeof data.error !='undefined' && data.error=='not_authorized_to_revert') {
                    var authorization_error_message = $translate.instant('not_authorized_to_revert');
                    Flash.create('danger', authorization_error_message, 'formMessage');
                }else if (typeof data.error !='undefined' && data.error=='missing_user_object') {
                    var authorization_error_message = $translate.instant('missing_user_object');
                    Flash.create('danger', authorization_error_message, 'formMessage');
                }else{
                    var authorization_error_message = $translate.instant('user_object_revert_not_saved');
                    Flash.create('danger', authorization_error_message, 'formMessage');
                }
                $modalInstance.close(data);

            }), function (data, status, headers, config) {
            $scope.formProcessing = false;
            console.log(data);
            console.log(status);
        };
    };

    //init controller
    init();
});