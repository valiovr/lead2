/**
 * Created by Miroslav Marinov on 03.09.2015.
 */

'use strict';

/**
 * @ngdoc function
 * @name LeadApp.controller:MyObjectDeleteModificationController
 * @description
 * # MyObjectDeleteModificationController
 * Controller of the LeadApp
 */
app.controller('MyObjectDeleteModificationController', function($http, $scope, $modalInstance, $translate,config, ApiService, Flash, id, user_id) {

    $scope.formProcessing = false;

    /**
     * Init controller default state
     */
    var init = function(){
        $scope.id = id;
        $scope.user_id = user_id;
    };

    /**
     * Cancel Cancel Delete Modal Window
     */
    $scope.cancelDeleteModification =function(){
        $modalInstance.dismiss('cancel');
    };

    /**
     * Delete user object
     * @param id
     */
    $scope.deleteUserObjectModification = function(id) {
        $scope.formProcessing = true;

        ApiService.destroy(config.api.url + config.urlSep + 'delete_user_object_modification' + config.urlSep + id + config.urlSep +$scope.user_id)
            .then(function (data, status, headers, config) {
                $scope.formProcessing = false;
                if(data ==1){
                    var success_message = $translate.instant('success_deleted_user_object_modification');
                    Flash.create('success', success_message, 'formMessage');
                }else if(data ==3){
                    var authorization_error_message = $translate.instant('error_not_authorized_to_delete_modification');
                    Flash.create('danger', authorization_error_message, 'formMessage');
                }else{
                    var error_message = $translate.instant('error_deleted_user_object_modification');
                    Flash.create('danger', error_message, 'formMessage');
                }
                $modalInstance.close(data);

            }),function (data, status, headers, config) {
            $scope.formProcessing = false;
            console.log(data);
            console.log(status);
        };

    };

    //init controller
    init();

});