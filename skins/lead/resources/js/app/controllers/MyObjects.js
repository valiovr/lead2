'use strict';

/**
 * @ngdoc function
 * @name LeadApp.controller:MyObjectsController
 * @description
 * # MyObjectsController
 * Controller of the LeadApp
 */

app.controller('MyObjectsController', function ($http, $scope, config, $modal, $translate, Flash) {
    $scope.userObjects = [];
    $scope.user_id     = null;
    $scope.isCollapsed = true;
    $scope.instance_id =null;
    
    /**
     * Get All Users Objects
     */
    $scope.initUserObjects = function () {
        if($scope.instance_id>0){
            $http.get(config.api.url + config.urlSep + 'get_user_instance_objects' + config.urlSep + $scope.user_id + "/" + $scope.instance_id).then(function (response) {
                $scope.userObjects = response.data;
            });
        }
        else if($scope.instance_id==-1){
            $http.get(config.api.url + config.urlSep + 'get_user_my_objects' + config.urlSep + $scope.user_id).then(function (response) {
                $scope.userObjects = response.data;
            });
        }
        else if($scope.instance_id==0){
            var error_message = $translate.instant('error_get_objects');
            Flash.create('warning', error_message, 'formMessage');
        }
    };

    /**
     *  Open delete Modal window
     * @param size
     * @param id
     */
    $scope.openDeleteModal = function (size, id) {

        var modalInstance = $modal.open({
            backdrop: true,
            animation: true,
            templateUrl: config.appFolder + '/views/deleteMyObjectModal.html',
            controller: 'MyObjectsDeleteController',
            size: size,
            resolve: {
                id: function () {
                    return id;
                },
                user_id: function () {
                    return $scope.user_id;
                }
            }
        });

        //On close modal reload User Objects list if delete is success
        modalInstance.result.then(function (data) {
            if (data == 1) {
                $scope.initUserObjects();
            }
        });

    };
    /**
     * Open "Delete Modification" Confirmation Modal window
     * @param size
     * @param id
     */
    $scope.openDeleteModificationModal = function (size, id, index, user_object_index) {
        //console.log($scope.userObjects[user_object_index]);
        //console.log(index); return false;
        var modalInstance = $modal.open({
            backdrop: true,
            animation: true,
            templateUrl: config.appFolder + '/views/DeleteMyObjectModificationModal.html',
            controller: 'MyObjectDeleteModificationController',
            size: size,
            resolve: {
                id: function () {
                    return id;
                },
                user_id: function () {
                    return $scope.user_id;
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (data == 1) {
                $scope.userObjects[user_object_index].user_object_changes.splice(index, 1);
            }
        });
    };
    /**
     * Revert User Object Modification
     * @param size
     * @param id
     */
    $scope.openRevertModificationModal = function (size, id) {
        var modalInstance = $modal.open({
            backdrop: true,
            animation: true,
            templateUrl: config.appFolder + '/views/revertMyObjectModificationModal.html',
            controller: 'MyObjectRevertModificationController',
            size: size,
            resolve: {
                id: function () {
                    return id;
                },
                user_id: function () {
                    return $scope.user_id;
                }
            }
        });
        modalInstance.result.then(function (data) {
            if (typeof data.success != 'undefined') {
                $scope.initUserObjects();
            }
        });
    };
    /**
     * Open User object modify Modal
     * @param size
     * @param id
     * @param index
     */
    $scope.openModifyModal = function (size, id, index) {

        var modalInstance = $modal.open({
            backdrop: true,
            animation: true,
            templateUrl: config.appFolder + '/views/modifyMyObjectModal.html',
            controller: 'MyObjectModifyController',
            size: size,
            resolve: {
                id: function () {
                    return id;
                },
                user_object: function () {

                    return angular.copy($scope.userObjects[index]); //copy object
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (typeof data.success != 'undefined') {
                $scope.initUserObjects();
            }
        });
    };
    /**
     * Watch if usr id is set
     */
    $scope.$watch('user_id', function (val) {
        if (val != null) {
            $scope.initUserObjects();

        }
    });

});