'use strict';

 var app = angular
    .module('LeadApp', [
        'ngAnimate',
        'ngAria',
        'ngResource',
        'ngSanitize',
        'ui.bootstrap',
        'pascalprecht.translate',
         'flash'
    ], function ($interpolateProvider) {
        //Integrate Angularjs with Blade
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }).config(function ( $httpProvider, config ,$translateProvider) {
		//Append CSRF Token To Request heades
        $httpProvider.defaults.headers.common['lead-api'] = config.api.key;

         //config translate module
        var lang_path = config.appFolder+'/languages/';
         $translateProvider.useStaticFilesLoader({
             prefix: lang_path,
             suffix: '.json'
         });
         $translateProvider.useSanitizeValueStrategy('escaped');
         $translateProvider.preferredLanguage('en_US');
 });