'use strict';

angular.module('LeadApp')
	.constant('config',
	/** @typedef {Object} config */
	{
		//Add your configuration here
		api: {
			url: "http://lead.api",
			key: 'm1A7DXi8KbRvEOK6cGSm6tXmjO7b6CQn1tp1tnd98LkVwNbbWjpiVvEtBrVz'
		},
		urlSep: '/',
		appFolder:"/skins/lead/resources/js/app"
	}
);