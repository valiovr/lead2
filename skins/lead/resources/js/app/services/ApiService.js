/**
 * Created by Miroslav Marinov on 02.09.2015.
 */
'use strict'

angular.module('LeadApp').service('ApiService', function($http,$q){

    return {
        /**
         *  Get data
         *
         * @param string url
         * @returns {*}
         */
        get: function(url){
           var deferred = $q.defer();

            $http.get(url).success(function (data) {

                deferred.resolve(data);
            }).error(function (data, status) {

                var error = {"status": status, "data": data};

                deferred.reject(error);
            });

            return deferred.promise;
        },
        /**
         * Save/Update data
         *
         * @param url
         * @param data
         * @returns {*}
         */
        save: function(url,data){
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(data)
            }).success(function (response) {
                deferred.resolve(response);
            })
                .error(function (response, status) {
                    var error = {"status": status, "data": response};
                    deferred.reject(error);
                });

            return deferred.promise;
        },
        /**
         * Delete Row
         * @param url
         * @returns {*}
         */
        destroy : function(url) {
            var deferred = $q.defer();

            $http.delete(url).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status) {
                var error = {"status": status, "data": data};

                deferred.reject(error);
            });

            return deferred.promise;

        },
        /**
         * REST Post call
         *
         * @param url
         * @param data
         * @returns {*}
         */
        post : function(url,data){
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(data)
            }).success(function (response) {
                deferred.resolve(response);
            })
                .error(function (response, status) {
                    var error = {"status": status, "data": response};
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
});
