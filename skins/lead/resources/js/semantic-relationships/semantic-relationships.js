/**
 * Created by valio on 31.7.2015 г..
 */

$(document).ready(function () {

    var secondary_table = $('.secondary-table');

    //Primary layers
    $('body').on('click', "button[name = 'layer-button']", function (layer) {
        $('.meta-objects-table').removeClass('active');
        $('.sec-sublayer-button').removeAttr('disabled');
        layer.preventDefault();
        $('.selected-td-hr').hide();
        //$('.sec-sublayer-button').attr('disabled','disabled');
        $('#meta-objects-select').attr('disabled','disabled');
        $('td.selected-meta-object').empty();
        var selected_layer = $(this).attr('data-layer-id');

        $('.sublayer-box').hide();
        //$('.meta-objects-table').hide();
        $('.layer-box-' + selected_layer + '').removeAttr('style');


    });
    //Secondary layers
    $('body').on('click', "button[name = 'secondary-layer-button']",function(second_layer) {
        second_layer.preventDefault();

        $('.sec-sublayer-button').removeAttr('disabled');
        var selected_layer = $(this).attr('data-secondary-layer-id');
        //$('.sec-sublayer-button').attr('disabled','enabled');
        $('.secondary-sublayer-box').hide();
        $('.secondary-layer-box-' + selected_layer + '').removeAttr('style');
        //$('.secondary-layer-box-' + selected_layer + '').css('margin-right','15px');
    });

//Primary sub-layers
    $('body').on('click', "button[name = 'sublayer-button']", function (sublayer) {
        sublayer.preventDefault();
        var active_table = $('.meta-objects-table.active');

        var active_span =  $('.meta-objects-table').find('span.select2');

        var button_sublayer_id = $(this).attr('data-sub-layer-id');

        if(active_span.length) {

            var active_tbl = $('.meta-objects-table.sublayer-'+button_sublayer_id+'');
            var find_select = $(active_span).siblings('select').not('.hidden')
            var meta_object = $(find_select).find(':selected').val();

            var active_select = $(find_select).attr('data-sublayer-id');

            var row = $('.related-objects[data-id="' + meta_object + '"]').clone();

            var selected_table = $(active_tbl).find('td.related-objects');
            var meta_objects = [];
            $(selected_table).each(function () {
                meta_objects.push($(this).attr('data-id'));
            });

            var data = {};
            data.selected_object = meta_object;
            data.active_meta_objects = meta_objects;

            //return false;
            ajaxCall(data,row, active_tbl, button_sublayer_id, meta_object, active_select);

        }


        if(secondary_table.is(':visible')){


            var active2 = $('.meta-objects-table.sublayer-'+button_sublayer_id+'');



            var selected_table = $('.meta-objects-table.sublayer-'+button_sublayer_id+'').find('td.related-objects');
            
            var meta_objects = [];
            var fresh = $('.fresh').find(':selected');
            button_sublayer_id = $(fresh).parent().attr('data-sublayer-id');

            var meta_object = $(fresh).val();
            var row = $('.related-objects[data-id="' + meta_object + '"]').clone();

            secondary_table.hide();

            $(selected_table).each(function () {
                meta_objects.push($(this).attr('data-id'));
            });
            data = {};
            data.selected_object = $('.fresh').find(':selected').val();
            data.active_meta_objects = meta_objects;
            console.log('before ajax');
            ajaxCall(data,row,active2, button_sublayer_id, meta_object);
        }



        if($('td.dropdown-td').length >0 ){
            $('td.dropdown-td').html('');

        }

        $('.meta-objects-select').removeAttr('disabled');
        $('.selected-td-hr').hide();
        $('td.selected-meta-object').empty();
        var selected_sub_layer = $(this).attr('data-sub-layer-id');

        $('.meta-objects-table').hide();
        $('.meta-objects-table').removeClass('active');
        $('.sublayer-' + selected_sub_layer + '').removeAttr('style');
        $('.sublayer-' + selected_sub_layer + '').addClass('active');

        var main_table  = $('.meta-objects-table.active table.main-table tr:first-child td:last-child');
        main_table.removeAttr('class');
        main_table.addClass('dropdown-td');

        var meta = $('.meta-objects');
        var clone = meta.clone(true);
        //clone.removeClass('hidden');

        clone.appendTo(main_table);
        setTimeout(function () {

            main_table.find('select.meta-objects-select').addClass('meta-objects-select-cloned');
        },100);

    });

    //Secondary sub-layers
    $('body').on('click', "button[name = 'secondary-sublayer-button']", function (secondary_sublayer) {
        $('.sec-sublayer-button').removeAttr('disabled');
        secondary_sublayer.preventDefault();
        $('td.foo').html('');
        $('span.relation-types').html('');

        secondary_sublayer_id = $(this).attr('data-sub-layer-id');


        //check for active table
        //var check = $('.meta-objects-table.active');
        var check = $('.meta-objects-table').is(':visible');
        if($(check).length > 0) {
            var foo = $('.meta-objects-select-cloned[data-sublayer-id="' + secondary_sublayer_id + '"]');
            $('.select2-container').remove();
            $('.dropdown-td > .meta-objects').removeClass('hidden');
            $('.meta-objects-select').addClass('hidden');
            foo.removeClass('hidden');
            foo.select2({
                templateResult: formatResult,
                templateSelection: formatSelected,

            });

        }else {
            
            //secondary_table.find('td.secondary-foo').html('22123');
            var meta_fresh_dropdown = $('.meta-objects-select[data-sublayer-id="' + secondary_sublayer_id + '"]').not('.meta-objects-select-cloned');
            
            var clone_dropdown = meta_fresh_dropdown.clone();
            clone_dropdown.removeAttr('class');
            clone_dropdown.addClass('fresh');
            td_foo = secondary_table.find('td.secondary-foo');

            $(this).attr('disabled','disabled');
            secondary_table.show();
            td_foo.empty().html(clone_dropdown);
            clone_dropdown.removeClass('hidden');
            clone_dropdown.removeAttr('disabled');

            clone_dropdown.select2({
                templateResult: formatResult,
                templateSelection: formatSelected,

            });
        }

    });


    $('.meta-objects-select').on('change', function () {
        $('td.foo').html('');

        var meta_object = $(this).val();
        var row = $('.related-objects[data-id="' + meta_object + '"]').clone();

        $(row).css('display', 'table-cell');
        $(row).css('vertical-align', 'middle');

        var active_table = $('.meta-objects-table.active');

        var selected_table = $(active_table).find('td.related-objects');
        var meta_objects = [];
        $(selected_table).each(function () {
            meta_objects.push($(this).attr('data-id'));
        });

        var data = {};
        data.selected_object = meta_object;
        data.active_meta_objects = meta_objects;

        ajaxCall(data, row, active_table);

    });


    function ajaxCall(data, row, active_table, button_sublayer_id, meta_object, active_select){
        $.post(mw.util.wikiScript(), {
            action: 'ajax',
            rs: 'LeadApi\\Ajax\\AjaxSemanticRelationships::getObjectRelations',
            data: data
        }, function (resp) {
            var response = $.parseJSON(resp);
            var relation_types = $(active_table).find('span.relation-types');
            $(relation_types).html('');
            $('.td-hr').remove();

            $.each(response, function(idx, elem){

                if ($('.relation-types[data-object-id="' + elem.related_object_id + '" ]').length > 0){
                    var span = $('.relation-types[data-object-id="' + elem.related_object_id + '" ]');
                    span.html(elem.relation_type.name);
                    var related_last_td = $(span).parents('td').siblings(':last-of-type');
                    if(!related_last_td.hasClass('dropdown-td')){
                        $(related_last_td).html(row.html());
                    }
                }
            });
            $('.selected-td-hr').show();

            if(button_sublayer_id) {
                if(active_select) {
                    var fresh_select = $('.meta-objects-select-cloned[data-sublayer-id="' + active_select + '"]');
                }else {
                    var fresh_select = $('.meta-objects-select-cloned[data-sublayer-id="' + button_sublayer_id + '"]');
                }

                fresh_select.val(meta_object);
                $('.select2-container').remove();

                $('.dropdown-td > .meta-objects').removeClass('hidden');
                $('.meta-objects-select').addClass('hidden');
                fresh_select.removeClass('hidden');
                fresh_select.select2({
                    templateResult: formatResult,
                    templateSelection: formatSelected,
                });
            }

        });
    };

    function formatResult (state) {
        var bar = state.element;
        var icon = $(bar).attr('name');
        var name = $(bar).text();
        if(!icon) {return name}
        var $state = $(
            //'<span><img src="images/meta_objects/' + icon+ '" class="img-responsive icon-img" style="display:inline" />'+name+'</span>'
            '<span style="display:block; float: left;"> <img src="images/meta_objects/'+icon+'" class="img-responsive icon-img" /> </span><span class="icons-text">'+name+'</span>'
        );
        return $state;
    };

    function formatSelected (state) {
        var bar = state.element;
        var icon = $(bar).attr('name');
        var name = $(bar).text();
        if(!icon) {return name}
        var $state = $(
            '<span><img src="images/meta_objects/' + icon+ '" class="img-responsive-selected icon-img" style="display:inline" />'+name+'</span>'
        );
        return $state;
    };

});
