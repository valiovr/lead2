/**
 * Created by Miroslav Marinov on 26.08.2015.
 */

var MetaObjectDefinitions = {
    metaObject: null,
    moTypes: null,
    moSubtypes: null,
    moNotation: null,
    /**
     * Display Options Buttons of Meta Object
     */
    showOptionButtons: function () {
        $('div.metaObjectBox').on('mouseenter', function () {
            $(this).find('div.metaObjectOptions').fadeIn(200);
        });

        $('div.metaObjectBox').on('mouseleave', function () {
            $(this).find('div.metaObjectOptions').hide();
        });

    },

    /**
     * Customize Existing Object
     */
    customizeExistingObjectModal: function () {
        //global object instance
        var obj = this;

        $('#metaObjectModalCreateInstance').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var meta_object = button.data('meta-object') // Extract info from data-* attributes

            //Call method to get Meta Object By ID
            obj.getMetaObjectById(meta_object);
        });

    },
    /**
     * Get Meta Object By ID
     * @param id
     */
    getMetaObjectById: function (id) {
        var data = {};
        //global object instance
        var obj = this;
        data.id = id;
        $.post(mw.util.wikiScript(), {
            action: 'ajax',
            rs: 'LeadApi\\Ajax\\AjaxMetaObjectDefinitions::getMetaObjectById',
            data: data
        }, function (res) {
            var response = $.parseJSON(res);

            //save response to metaObject property
            obj.metaObject = response;
            $('input[name="meta_object_id"]').val(response.id);
            $('input[name="meta_object_notation"]').val(response.icon);
            $('input#metaObjectName').val(response.name);
            $('div.metaObjectDescription').html(response.description);

            //Display Notation in form
            var notation = $('img.metaObjectNotationIcon');
            var notation_src = notation.attr('src');
            var notation_src_replaced = notation_src.replace('default.png',response.icon);
            notation.attr({"src":notation_src_replaced});

            //Set Notation Property
            obj.moNotation = response.icon;

            if (response.stereotype.length > 0) {
                $.each(response.stereotype, function (index, value) {
                    if (index == 0) {
                        var default_option = new Option('---', '');
                        $('select#stereotype').append(default_option);

                    }
                    //Create dynamic stereotype options
                    var option = new Option(value.name, value.id);
                    $('select#stereotype').append(option);

                });
            } else {
                if (index == 0) {
                    var default_option = new Option('---', '');
                    $('select#stereotype').append(default_option);

                }
            }

        });

    },
    /**
     * Set Object Properties to default state
     */
    onCloseModal: function () {
        //global object instance
        var obj = this;

        $('#metaObjectModalCreateInstance').on('hide.bs.modal', function (event) {
            //clear object properties data on modal window is closed
            obj.metaObject = null;
            obj.moTypes = null;
            obj.moSubtsypes = null;

            //return form to default state
            $('select#stereotype option').remove();
            $('div.stereotypeDescription').html('');

            $('select#moType option').remove();
            $('div.moTypeDescriptionBox').html('');

            $('select#moSubtype option').remove();
            $('div.moSubtypeDescriptionBox').html('');


            $('div.stereotypeDescriptionBox div.stereotypeDescription').html('');
            $('div.stereotypeDescriptionBox').addClass('hidden');

            $('div.moSubtypeDescriptionBox div.moSubtypeDescription').html('');
            $('div.typeBox').addClass('hidden');
            $('div.moSubtypeDescriptionBox').addClass('hidden');

            $('div.moSubtypeDescriptionBox div.moSubtypeDescription').html('');
            $('div.subtypeBox').addClass('hidden');
            $('div.moSubtypeDescriptionBox').addClass('hidden');

            //Reset default Notation form state
            var notation = $('img.metaObjectNotationIcon');
            var notation_src = notation.attr('src');
            var notation_src_replaced = notation_src.replace(obj.moNotation,'default.png');
            notation.attr({"src":notation_src_replaced});

            //Reset moNotation Property to default
            obj.moNotation = null;

            //Clear form messages
            $('form#customizeExistingObject div.modal-body div.alert-success').html('').addClass('hidden');
            $('form#customizeExistingObject div.modal-body div.alert-danger').html('').addClass('hidden');

            //reload location
            window.location.href=window.location.href;
        });
    },
    /**
     * On change stereotype  -Display Selected stereotype descriptions and
     * display types of current selected stereotype  if they exists
     */
    onChangeStereotype: function () {
        //global object instance
        var obj = this;

        $('select#stereotype').on('change', function () {
            var selected_stereotype = $(this).val();
            $.each(obj.metaObject.stereotype, function (index, value) {
                if (value.id == selected_stereotype) {
                    // display description of selected stereotype
                    $('div.stereotypeDescriptionBox div.stereotypeDescription').html(value.description);
                    $('div.stereotypeDescriptionBox').removeClass('hidden');


                    //Set Types to object properties value
                    obj.moTypes = value.children;

                    //clear type select options
                    $('select#moType option').remove();

                    if (value.children.length > 0) {
                        //build  type select options
                        $.each(obj.moTypes, function (index, value) {

                            //create default type option
                            if (index == 0) {
                                var default_option = new Option('---', '');
                                $('select#moType').append(default_option);
                            }

                            //Create dynamic type options
                            var option = new Option(value.name, value.id);
                            $('select#moType').append(option);

                        });

                        //show types section
                        $('div.typeBox').removeClass('hidden');
                    } else {
                        obj.moSubtypes = null;
                        obj.moTypes = null;

                        //hide subtypes section
                        $('div.subtypeBox').addClass('hidden');
                        //hide subtype description and empty content
                        $('div.moSubtypeDescriptionBox div.moSubtypeDescription').html('');
                        $('div.moSubtypeDescriptionBox').addClass('hidden');

                        //hide types section
                        $('div.typeBox').addClass('hidden');
                        //hide type description and empty content
                        $('div.moTypeDescriptionBox div.moTypeDescription').html('');
                        $('div.moTypeDescriptionBox').addClass('hidden');
                    }
                    //Break loop
                    return false;
                }
            });

        });
    },
    /**
     * On change type select - display selected type description and
     * display subtypes of selected type if they exists
     */
    onChangeMoType: function () {
        //global object instance
        var obj = this;

        //Get current selected  type object on changed select value
        $('select#moType').on('change', function () {
            //get current selected type
            var selected_type = $(this).val();


            // foreach to find current selected type  object
            $.each(obj.moTypes, function (index, value) {
                if (value.id == selected_type) {
                    // display description of selected type
                    $('div.moTypeDescriptionBox div.moTypeDescription').html(value.description);
                    $('div.moTypeDescriptionBox').removeClass('hidden');

                    //Set Subtypes to object properties value
                    obj.moSubtypes = value.children;

                    //clear subtype select options
                    $('select#moSubtype option').remove();

                    //check if type has suptypes
                    if (value.children.length > 0) {
                        //build  type select options
                        $.each(obj.moSubtypes, function (index, value) {

                            //create default subtype option
                            if (index == 0) {
                                var default_option = new Option('---', '');
                                $('select#moSubtype').append(default_option);
                            }

                            //Create dynamic type options
                            var option = new Option(value.name, value.id);
                            $('select#moSubtype').append(option);
                        });

                        //show subtypes section
                        $('div.subtypeBox').removeClass('hidden');
                    } else {
                        obj.moSubtypes = null;
                        //hide subtypes section
                        $('div.subtypeBox').addClass('hidden');
                        //hide subtype description and empty content
                        $('div.moSubtypeDescriptionBox div.moSubtypeDescription').html('');
                        $('div.moSubtypeDescriptionBox').addClass('hidden');
                    }

                    //Break loop
                    return false;
                }
            });
        });
    },
    /**
     * On change subtype select - display selected subtype description
     */
    onChangeSubtype: function(){
        //global object instance
        var obj = this;

        //Get current selected  subtype object on changed select value
        $('select#moSubtype').on('change', function () {
            //get current selected subtype
            var selected_subtype = $(this).val();

            // foreach to find current selected type  object
            $.each(obj.moSubtypes, function (index, value) {
                if (value.id == selected_subtype) {
                    // display description of selected type
                    $('div.moSubtypeDescriptionBox div.moSubtypeDescription').html(value.description);
                    $('div.moSubtypeDescriptionBox').removeClass('hidden');
                }
            });
        });
    },
    /**
     * Save Meta Object Instance data
     */
    metaObjectInstanceSave: function(){
        $("button.createMetaObjectInstance").on('click',function(e){
            e.preventDefault();
            //Build Create form data object
            var data = {
            "name":$('input#metaObjectName').val(),
            "icon":$('input[name="meta_object_notation"]').val(),
            "stereotype_id":$('select#stereotype').val(),
            "type_id":$('select#moType').val(),
            "subtype_id":$('select#moSubtype').val(),
            "meta_object_id":$('input[name="meta_object_id"]').val()
            };
            //Clear form messages
            $('form#customizeExistingObject div.modal-body div.alert-success').html('').addClass('hidden');
            $('form#customizeExistingObject div.modal-body div.alert-danger').html('').addClass('hidden');

            //Send Post request to LeadAPI extention
            $.post(mw.util.wikiScript(), {
                action: 'ajax',
                rs: 'LeadApi\\Ajax\\AjaxMetaObjectDefinitions::createMetaObjectInstance',
                data: data
            }, function (resp) {
                var response = $.parseJSON(resp);
                if(typeof response.success != 'undefined'){
                    $('form#customizeExistingObject div.modal-body div.alert-success').html(response.success).removeClass('hidden');
                }
                if(typeof response.errors != 'undefined'){
                    var list = $('<ul/>').appendTo('form#customizeExistingObject div.modal-body div.alert-danger');
                    $.each(response.errors, function( index, value ) {
                        list.append('<li>'+value+'</li>');
                    });

                    $('form#customizeExistingObject div.modal-body div.alert-danger').removeClass('hidden');
                }

                console.log(resp);
            });

        });
    }
};

$(document).ready(function () {

    //Check If Loaded Page is Meta Object Definitions
    if ($('div.sublayer-box').length > 0) {
        //MetaObjectDefinitions.showOptionButtons();
        MetaObjectDefinitions.customizeExistingObjectModal();
        MetaObjectDefinitions.onCloseModal();
        MetaObjectDefinitions.onChangeStereotype();
        MetaObjectDefinitions.onChangeMoType();
        MetaObjectDefinitions.onChangeSubtype();
        MetaObjectDefinitions.metaObjectInstanceSave();

    }

});

