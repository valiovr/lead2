<img src="{page_notation}" alt="">

=== Description ===

{page_description}

=== Layer ===

[[la_layer::Layers: {page_layer} | {page_layer_name}]]

=== Sub-layer ===

[[la_layer::Layers: {page_sublayer} | {page_sublayer_name}]]

=== Object Class ===

[[la_class::Objects:Meta Object Definitions | Meta Object Definitions]]

=== Semantic Relationships ===
{semantic_relationships}
<ul>
    <li>
"{meta_object}" {relation_type} "[[{rel_object}]]"
    </li>
</ul>
{/semantic_relationships}
