<div class="" data-ng-app="LeadApp">
    <div class="" data-ng-controller="MyObjectsController" data-ng-init="user_id={user_id};instance_id={instance_id}">
        <div flash-message="5000"></div>
       
    <form>
        <div class="form-group col-md-12">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-search"></i></div>
                <input type="text" class="form-control" placeholder="Search the objects by anything" ng-model="searchObjects">
            </div>      
        </div>
    </form>
    <div class="clearfix"></div>
        <table class="table table-striped table-bordered" data-ng-cloak="">
            <tr>
                <th ng-click="sortType = 'id'; sortReverse = !sortReverse" class="sortable" >
                    <span class="tooltips" data-original-title="Click to Sort">ID</span>
                    <span ng-show="sortType == 'id'" class="fa fa-caret-down"></span>
                    <span ng-show="sortType == 'id' && sortReverse" class="fa fa-caret-up"></span>
                </th>
                <th>
                    <div class="col-md-1">
                        
                    </div>
                    <div class="col-md-5 sortable tooltips" ng-click="sortType = 'name'; sortReverse = !sortReverse" >
                        <span class="tooltips" data-original-title="Click to Sort">[['object_name' | translate]]</span>
                        <span ng-show="sortType == 'name'" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
                    </div>
                    <div class="col-md-3 sortable" ng-click="sortType = 'created_at'; sortReverse = !sortReverse">
                         <span class="tooltips" data-original-title="Click to Sort">[['created'| translate]]</span>
                        <span ng-show="sortType == 'created_at'" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'created_at' && sortReverse" class="fa fa-caret-up"></span>
                    </div>
                    <div class="col-md-3 sortable"  ng-click="sortType = 'updated_at'; sortReverse = !sortReverse">
                         <span class="tooltips" data-original-title="Click to Sort">[['last_modified' | translate]]</span>
                        <span ng-show="sortType == 'updated_at'" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'updated_at' && sortReverse" class="fa fa-caret-up"></span>
                    </div>
                </th>
            </tr>
            <tbody>

            <tr data-ng-repeat="userObject in userObjects | orderBy:sortType:sortReverse | filter:searchObjects">

                <td style="background-color:#48d0ff !important;" >
                    [[userObject.id]]
                </td>
                <td>
                    <div class="row">
                        <div class="col-md-1 text-center">
                            <img data-ng-src="images/meta_objects/[[userObject.icon]]" width="50px" height="auto">
                        </div>
                        <div class="col-md-5" >
                            [[userObject.name]]
                            <div class="userObjectActions">
                                <span class="text-info actionButtons disabled" data-ng-show="userObject.user_object_changes.length==0">[['all_versions'| translate]]</span>
                                <span class="text-info actionButtons" data-ng-click="isCollapsed = !isCollapsed" data-ng-show="userObject.user_object_changes.length>0">[['all_versions'| translate]]</span>
                                <span>&nbsp;|&nbsp;</span>
                                <span class="text-info actionButtons" data-ng-click="openModifyModal('lg', userObject.id, $index)">[['modify'| translate]]</span>
                                <span>&nbsp;|&nbsp;</span>
                                <span class="text-info actionButtons" data-ng-click="openDeleteModal('sm', userObject.id)">[['delete' | translate]]</span>
                            </div>
                        </div>
                        <div class="col-md-3" >
                            [[userObject.created_at]]
                        </div>
                        <div class="col-md-3">
                            [[userObject.updated_at]]
                        </div>
                    </div>
                    <!-- Begin User Object Changes-->
                    <div class="row" data-collapse="isCollapsed">
                        <div class="col-md-12 modificationBox">
                                <div class="row userObjectModificationRow" data-ng-repeat="userObjectChange in userObject.user_object_changes">
                                    <div class="col-md-1 text-center">
                                        <img data-ng-src="images/meta_objects/[[userObjectChange.icon]]" width="25px"
                                             height="auto">
                                    </div>
                                    <div class="col-md-5">
                                        [[userObjectChange.name]]
                                    </div>
                                    <div class="col-md-3">
                                        <span class="userObjectModificationActions">
                                            <span class="text-info modificationActionButtons" data-ng-click="openRevertModificationModal('sm',userObjectChange.id)">[['revert'| translate]]</span>
                                            <span>&nbsp;|&nbsp;</span>
                                            <span class="text-info modificationActionButtons" data-ng-click="openDeleteModificationModal('sm',userObjectChange.id,$index,userObjects.indexOf(userObject))">[['delete' | translate]]</span>
                                        </span>
                                    </div>
                                    <div class="col-md-3">
                                        [[userObjectChange.created_at]]
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- End User Object Changes-->
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>
<link type="text/css" rel="stylesheet"
      href="/skins/lead/resources/js/angular/modules/angular-flash-alert/angular-flash.min.css"/>
<!-- include angular files modules -->
<script type="text/javascript" src="/skins/lead/resources/js/angular/angular.min.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/angular/angular-animate.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/angular/angular-aria.min.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/angular/angular-resource.min.js"></script>
<script type="text/javascript"
        src="/skins/lead/resources/js/angular/modules/angular-bootstrap/ui-bootstrap-0.13.3.min.js"></script>
<script type="text/javascript"
        src="/skins/lead/resources/js/angular/modules/angular-bootstrap/ui-bootstrap-tpls-0.13.3.min.js"></script>
<script type="text/javascript"
        src="/skins/lead/resources/js/angular/modules/angular-translate/angular-translate.min.js"></script>
<script type="text/javascript"
        src="/skins/lead/resources/js/angular/modules/angular-translate/angular-translate-loader-static-files.js"></script>
<script type="text/javascript"
        src="/skins/lead/resources/js/angular/modules/angular-flash-alert/angular-flash.min.js"></script>

<!-- include front controller angular app.js -->
<script type="text/javascript" src="/skins/lead/resources/js/app/app.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/app/services/config.js"></script>
<!-- include my objects services -->
<script type="text/javascript" src="/skins/lead/resources/js/app/services/ApiService.js"></script>

<!-- include my objects controllers -->
<script type="text/javascript" src="/skins/lead/resources/js/app/controllers/MyObjects.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/app/controllers/modals/MyObjectsDeleteController.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/app/controllers/modals/MyObjectDeleteModificationController.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/app/controllers/modals/MyObjectRevertModificationController.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/app/controllers/modals/MyObjectModifyController.js"></script>
