<div class="row">
    <form>
        <div class="col-md-3 no-left-padding ">
            <div class="form-group">
                <select name="layers" class="form-control" id="layers-list">
                    <option value="">---</option>
                    {layers}
                    <option value="{id}">{name}</option>
                    {/layers}
                </select>
            </div>
        </div>
        <div class="col-md-3 no-right-padding">
            <select name="sublayers" class="form-control" id="sublayers-list">
                <option value="">---</option>
            </select>
        </div>
    </form>
</div>
{meta_objects_data}
<div id="sublayer-content-{sub_id}" class="row sublayer-box layer-box-{main_layer_id}">
    <div class="col-md-12 nopadding">
        <div class="coloredTitle" style="background-color: {sub_color}; ">
            <p>{sub_title}</p>
        </div>
        <div class="dynamicPageContent clearfix">
            <div class=" col-md-4 box-with-icon no-left-padding">
                {col_one}
                <div class="metaObjectBox">
                <span style="display:block; float: left; margin-right: 6px; text-align:center">
                    <img alt="{name}" src="images/meta_objects/{icon}" class="img-responsive icon-img">
                    <span class="notModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                    <span class="yesModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                </span>
                    <span class="icons-text"><a href="index.php?title={name}">{name}</a> <span
                            class="modifiedText text-danger {is_modified_mo}">Modified</span></span>
                    <dl>
                        <dd>
                            <!-- Begin Action Buttons -->
                            <div class="userObjectActions">
                        <span class="text-info actionButtonsObjects">
                            <a data-toggle="modal"
                               data-target="#metaObjectModalCreateInstance" data-meta-object="{id}">New instance
                            </a>
                        </span>
                                <span>&nbsp;|&nbsp;</span>
                                <span class="text-info actionButtonsObjects"><a href="index.php?title=My_Objects&instance={id}">View
                                        all instances</a></span>
                            </div>
                            <!-- End Action Buttons -->
                            {description}
                        </dd>
                    </dl>
                    <!--                    <div class="metaObjectOptions">-->
                    <!--                        <div class="metaObjectOptionsBg" style="background: {sub_color};"></div>-->
                    <!--                        <div class="metaObjectOptionsContent btn-group btn-group-xs">-->
                    <!--                            <button type="button" class="btn btn-xs btn-default" data-toggle="modal"-->
                    <!--                                    data-target="#metaObjectModalCreateInstance" data-meta-object="{id}">Create new-->
                    <!--                                instance (copy) of the object-->
                    <!--                            </button>-->
                    <!--                            <a href="index.php?title=My_Objects" type="button" class="btn btn-xs btn-default">View all-->
                    <!--                                instances</a>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>
                {/col_one}
            </div>
            <div class=" col-md-4 box-with-icon no-left-padding">
                {col_two}
                <div class="metaObjectBox">
                <span style="display:block; float: left; margin-right: 6px; text-align:center">
                    <img alt="{name}" src="images/meta_objects/{icon}" class="img-responsive icon-img">
                    <span class="notModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                    <span class="yesModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                </span>
                    <span class="icons-text"><a href="index.php?title={name}">{name}</a> <span
                            class="modifiedText text-danger {is_modified_mo}">Modified</span></span>
                    <dl>
                        <dd>
                            <!-- Begin Action Buttons -->
                            <div class="userObjectActions">
                        <span class="text-info actionButtonsObjects">
                            <a data-toggle="modal"
                               data-target="#metaObjectModalCreateInstance" data-meta-object="{id}">New instance
                            </a>
                        </span>
                                <span>&nbsp;|&nbsp;</span>
                                <span class="text-info actionButtonsObjects"><a href="index.php?title=My_Objects&instance={id}">View
                                        all instances</a></span>
                            </div>
                            <!-- End Action Buttons -->
                            {description}
                        </dd>
                    </dl>

                </div>
                {/col_two}
            </div>
            <div class=" col-md-4 box-with-icon no-left-padding">
                {col_three}
                <div class="metaObjectBox">
                <span style="display:block; float: left; margin-right: 6px; text-align:center">
                    <img alt="{name}" src="images/meta_objects/{icon}" class="img-responsive icon-img">
                    <span class="notModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                    <span class="yesModified {is_modified_mo}" style="font-weight: bold;">{id}</span>
                </span>
                    <span class="icons-text"><a href="index.php?title={name}">{name}</a> <span
                            class="modifiedText text-danger {is_modified_mo}">Modified</span></span>
                    <dl>
                        <dd>
                            <!-- Begin Action Buttons -->
                            <div class="userObjectActions">
                        <span class="text-info actionButtonsObjects">
                            <a data-toggle="modal"
                               data-target="#metaObjectModalCreateInstance" data-meta-object="{id}">New instance
                            </a>
                        </span>
                                <span>&nbsp;|&nbsp;</span>
                                <span class="text-info actionButtonsObjects"><a href="index.php?title=My_Objects&instance={id}">View
                                        all instances</a></span>
                            </div>
                            <!-- End Action Buttons -->
                            {description}
                        </dd>
                    </dl>
                </div>
                {/col_three}
            </div>

        </div>
    </div>
</div>
{/meta_objects_data}

{main_layers}
<ul id="layer-{layer_id}" class="hidden">
    {sublayers}
    <li class="sublayer-{id}">{name}</li>
    {/sublayers}
</ul>
{/main_layers}

<!-- Begin Modal Box -->
<div id="metaObjectModalCreateInstance" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="metaObjectModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Begin Meta Object Form -->
            <form role="form" id="customizeExistingObject">
                <input name="meta_object_id" type="hidden" value=""/>
                <input name="meta_object_notation" type="hidden" value=""/>
                <!-- Begin Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create new instance (copy) of the object </h4>
                </div>
                <!-- End Modal Header -->
                <!-- Begin Modal Content -->
                <div class="modal-body">
                    <div class="alert alert-success  hidden" role="alert"></div>
                    <div class="alert alert-danger hidden" role="alert"></div>
                    <!-- Begin Meta Object Notation -->
                    <div class="row">
                        <div class="col-md-1">
                            <!--
                            IMPORTANT:DO NOT CHANGE NAME OF DEFAULT IMAGE(default.php)
                            NAME IS USED IN JAVASCRIPT TO BE REPLACED WITH META OBJECT NOTATION
                             -->
                            <img src="/images/meta_objects/default.png" class="metaObjectNotationIcon img-responsive"
                                 alt=""/>
                        </div>
                    </div>
                    <!-- End Meta Object Notation -->
                    <!-- Begin Name and Description Row -->
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="metaObjectName">Meta object name</label>
                                <input name="meta_object_name" type="text" value="" class="form-control"
                                       id="metaObjectName"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <label>Meta object description</label>

                            <div class="metaObjectDescription"></div>
                        </div>
                    </div>
                    <!-- End Name and Description Row -->
                    <!-- Begin Stereotype, Type and Subtype Row -->
                    <div class="row stereotypeRow">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="stereotype">Select stereotype</label>
                                <select name="stereotype" class="form-control" id="stereotype"></select>
                            </div>
                            <div class="stereotypeDescriptionBox hidden">
                                <label>Stereotype description</label>

                                <div class="stereotypeDescription"></div>
                            </div>
                        </div>
                        <div class="col-md-4 typeBox hidden">
                            <div class="form-group">
                                <label for="moType">Select type</label>
                                <select name="type" class="form-control" id="moType"></select>
                            </div>
                            <div class="moTypeDescriptionBox hidden">
                                <label>Type description</label>

                                <div class="moTypeDescription"></div>
                            </div>
                        </div>
                        <div class="col-md-4 subtypeBox hidden">
                            <div class="form-group">
                                <label for="moSubtype">Select subtype</label>
                                <select name="subtype" class="form-control" id="moSubtype"></select>
                            </div>
                            <div class="moSubtypeDescriptionBox hidden">
                                <label>Type description</label>

                                <div class="moSubtypeDescription"></div>
                            </div>
                        </div>
                    </div>
                    <!-- End Stereotype, Type and Subtype Row -->
                </div>
                <!-- End Modal Content -->
                <!-- Begin Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary createMetaObjectInstance">Create instance</button>
                </div>
                <!-- End Modal Footer -->
            </form>
            <!-- End Meta Object Form -->
        </div>
    </div>
</div>

<!-- End Modal Box -->




