<!-- Begin X-BPMN NOtations Content -->
<div class="row">
    <!-- Begin Column 1-->
    <div class="col-md-3 nopadding">
        <!-- Begin Task Types -->
        {task_types_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/task_types_arr}
        <!-- End Task Types -->

</div>
<!-- End Column 1-->

<!-- Begin Column 2-->
<div class="col-md-3">
    <!-- Begin Activities-->
    {activities_arr}
    <!-- Begin Page Category Box -->
    <div class="relPageCategoryBox">
        <!-- Begin Category Name -->
        <div class="relPageCategoryName">
            <p>
                <b>{category_name}</b>
            </p>
        </div>
        <!-- End Category Name -->
        <!-- Begin Pages in category -->
        {cat_pages}
        <div class="media-notations relPageBox">
            <!-- Begin Title Box -->
            <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}" class="activitiesIcon">
            </span>

             <span class="icons-text relPageTitle activitiesTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
            </div>
            <!-- End Title Box -->

            <!-- Begin Description Box -->
            <div class="relPageDescription">{page_description}</div>
            <!-- End Description Box -->
        </div>
        {/cat_pages}
        <!-- End Pages in category -->
    </div>
    <!-- End Page Category Box -->
    {/activities_arr}
    <!-- End Activities-->
    <!-- Begin Markers-->
    {markers_arr}
    <!-- Begin Page Category Box -->
    <div class="relPageCategoryBox">
        <!-- Begin Category Name -->
        <div class="relPageCategoryName">
            <p>
                <b>{category_name}</b>
            </p>
        </div>
        <!-- End Category Name -->
        <!-- Begin Pages in category -->
        {cat_pages}
        <div class="media-notations relPageBox">
            <!-- Begin Title Box -->
            <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
            </div>
            <!-- End Title Box -->

            <!-- Begin Description Box -->
            <div class="relPageDescription">{page_description}</div>
            <!-- End Description Box -->
        </div>
        {/cat_pages}
        <!-- End Pages in category -->
    </div>
    <!-- End Page Category Box -->
    {/markers_arr}
    <!-- End Markers-->

    <!-- Begin Flows-->
    {flows_arr}
    <!-- Begin Page Category Box -->
    <div class="relPageCategoryBox">
        <!-- Begin Category Name -->
        <div class="relPageCategoryName">
            <p>
                <b>{category_name}</b>
            </p>
        </div>
        <!-- End Category Name -->
        <!-- Begin Pages in category -->
        {cat_pages}
        <div class="media-notations relPageBox">
            <!-- Begin Title Box -->
            <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
            </div>
            <!-- End Title Box -->

            <!-- Begin Description Box -->
            <div class="relPageDescription">{page_description}</div>
            <!-- End Description Box -->
        </div>
        {/cat_pages}
        <!-- End Pages in category -->
    </div>
    <!-- End Page Category Box -->
    {/flows_arr}
    <!-- End Flows-->
</div>
<!-- End Column 2-->

<!-- Begin Column 3-->
<div class="col-md-3 nopadding">
    <!-- Begin Objects-->
    {objects_arr}
    <!-- Begin Page Category Box -->
    <div class="relPageCategoryBox">
        <!-- Begin Category Name -->
        <div class="relPageCategoryName">
            <p>
                <b>{category_name}</b>
            </p>
        </div>
        <!-- End Category Name -->
        <!-- Begin Pages in category -->
        {cat_pages}
        <div class="media-notations relPageBox">
            <!-- Begin Title Box -->
            <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
            </div>
            <!-- End Title Box -->

            <!-- Begin Description Box -->
            <div class="relPageDescription">{page_description}</div>
            <!-- End Description Box -->
        </div>
        {/cat_pages}
        <!-- End Pages in category -->
    </div>
    <!-- End Page Category Box -->
    {/objects_arr}
    <!-- End Objects-->
</div>
<!-- End Column 3-->

<!-- Begin Column 4-->
<div class="col-md-3">
    <!-- Begin Objects-->
    {events_arr}
    <!-- Begin Page Category Box -->
    <div class="relPageCategoryBox">
        <!-- Begin Category Name -->
        <div class="relPageCategoryName">
            <p>
                <b>{category_name}</b>
            </p>
        </div>
        <!-- End Category Name -->
        <!-- Begin Pages in category -->
        {cat_pages}
        <div class="media-notations relPageBox">
            <!-- Begin Title Box -->
            <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
            </div>
            <!-- End Title Box -->

            <!-- Begin Description Box -->
            <div class="relPageDescription">{page_description}</div>
            <!-- End Description Box -->
        </div>
        {/cat_pages}
        <!-- End Pages in category -->
    </div>
    <!-- End Page Category Box -->
    {/events_arr}
    <!-- End Objects-->
</div>
<!-- End Column 4-->
</div>
<!-- Begin Row 2 -->
<div class="row">
    <!-- Begin Column 1 -->
    <div class="col-md-3 nopadding">

        <!-- Begin Gateways-->
        {gateways_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/gateways_arr}
        <!-- End Gateways-->
    </div>
    <!-- End Column 1 -->

    <!-- Begin Column 2 -->
    <div class="col-md-3">
        <!-- Begin Conversations-->
        {conversations_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/conversations_arr}
        <!-- End Conversations-->
    </div>
    <!-- End Column 1 -->
</div>
<!-- End Row 2 -->
<div class="row">
    <div class="col-md-12">
    <img src="/images/b/b4/XPMN.png" class="img-responsive" alt="" />
    </div>
</div>
<!-- End X-BPMN NOtations Content -->