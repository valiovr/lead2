<img src="{page_notation}" alt="">

=== Description ===

{page_description}

=== Layer ===

[[la_layer::Layers: {page_layer} | {page_layer_name}]]

=== Sub-layer ===

[[la_layer::Layers: {page_sublayer} | {page_sublayer_name}]]

=== Object Class ===

[[la_class::Objects:{page_object_class} | {page_object_class}]]

=== Object Category ===
[[la_category::{page_object_category}]]