<?php
/**
 * Created by PhpStorm.
 * User: valio
 * Date: 22.7.2015 г.
 * Time: 17:21 ч.
 */

?>
<!-- Google fonts -->
<link href="http://fonts.googleapis.com/css?family=Oswald:400,300" rel="stylesheet" type="text/css" />
<!-- Reseting styles -->
<link rel="stylesheet" type="text/css" href="/skins/lead/resources/styles/Periodic-table/reset.css" />
<!-- Responsive styles -->
<link rel="stylesheet" type="text/css" href="/skins/lead/resources/styles/Periodic-table/responsive.css" />


<style type="text/css">


    /* Style in inline because it needs interaction with PHP */
    /* Values came directly from table-setup-data.php */

    body {
        background: #fff;
    }

    /*h1, h2, h3 { font-family: georgia, serif; line-height: 1; }*/

    /*h1 { font-size: 2.5em; color: #666; margin-top: 0.5em; }*/

    p { margin: 2em 0; }

    div { box-sizing: border-box; }

    #periodic {
        background: #fff;
        width: {width_table}{unit}; margin: auto;
    }

    .top {
        background: #fff;
        float: left;
        -moz-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        -o-transform:rotate(180deg);
        -ms-transform:rotate(180deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=0);
    }

    .element {
        border: 2px solid #fff;
        width: {width_cel}{unit}; height:{width_cel}{unit}; background: #ededed; float: left;
        -moz-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        -o-transform:rotate(180deg);
        -ms-transform:rotate(180deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=0);

        position: relative;
        font-family: 'Open Sans', sans-serif;
        font-weight: bold;
        font-size: 12px;
        text-align: center;
        padding-top: 1px; padding-right: 3px;
        line-height: 1;
    }

    .element small {
        position: absolute;
        left: 3px; bottom: 3px;
        font-size: 12px;
        display: block;
    }

    .element img {
        position: absolute; right: 3px; bottom: 3px;
        width: {ceil} height: auto;
    }

    .element:hover {
        -webkit-transform: scale(1.6) rotate(180deg);
        -moz-transform: scale(1.6) rotate(180deg);
        -ms-transform: scale(1.6) rotate(180deg);
        -o-transform: scale(1.6) rotate(180deg);
        transform: scale(1.6) rotate(180deg);
        z-index: 9999;
        box-shadow: 0 0 30px rgba(0,0,0,0.9)
    }

    .layer {
        float: left; background: #fff; color: #fff; 
    }

    .business-competency {  float: left; width: {width_cel-x3}; height: auto; }
    .business-service {     float: left; width: {width_cel-x2}; height: auto; }
    .business-process {     float: left; width: {width_cel-x2}; height: auto; }
    .information-system {   float: left; width: {width_cel-x4}; height: auto; color: #000; }


    .data {                 float: left; width: {width_cel-x2}; height: auto; }
    .platform {             float: left; width: {width_cel-x2}; height: auto; }
    .infrastructure {       float: left; width: {width_cel-x2}; height: auto; }

    .e-18 { float: right; }
    .e-34 { float: right; }

    .purpose-goal {
        margin-top: 30px; float: left; clear:both; width:{width_cel-x17}; height: auto;
        -moz-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        -o-transform:rotate(180deg);
        -ms-transform:rotate(180deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=0);
    }

    .layer-caption { float: left; margin-top: 10px; }
    .layer-caption-middle { margin-left: 30px; margin-right: 30px; }
    .legend { padding-top: 10px; float: left; width: 180px;line-height: 2 }
    .legend .color { margin-right: 10px; float: left; width: 28px; height: 28px; -moz-border-radius: 50%; -khtml-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%; }
    .description { margin-left: 38px; }

    .layer-caption-middle .legend, .layer-caption-right .legend { width: 160px; }

    .layer-caption-left { width: 40%; }
    .layer-caption-middle { width: 25%; }
    .layer-caption-right { width: 25%; }

    .info-group { clear: both; padding-top: 10px; padding-bottom: 30px;}
    .info-box { float: left; }
    .info-box-1 { width: 28%; }
    .info-box-2 { width: 15%; padding-top: 2em; }
    .info-box-3 { width: 50%; }
    .mini-box {
        margin: auto;
        width: 90px; height: 90px; display: block; background: #dedede;
        position: relative;
    }
    .mini-title { text-align: center; padding-top: 2px; }
    .mini-number { display: block; position: absolute; left: 5px; bottom: 5px; }
    .mini-symbol { position: absolute; right: 5px; bottom: 5px; background: #fff; width: 60%; height: 60%; text-align: center; line-height: 55px; -moz-border-radius: 50%; -khtml-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%; }

    .escondido { display: none; }

    #bloco-carregando { height: 500px; position: relative; }
    #bloco-carregando img { position: absolute; top: 49%; left: 49%; }
</style>
<div >
    <div id="periodic" class="clearfix">

<!--        <h1>The Periodic Table of Enterprise Elements</h1>-->

        <p> The periodic table is a tabular arrangement of the enterprise elements, organized on the basis of their properties. The standard form of the table consists of a grid of elements laid out by the three main enterprise layers, namely the business, information and technology layer, within them are found their specific categories in a color code of eight groups. The table can be used where the eight color coded category of groups are sorted by their relationship to the layers and thereby the modelling, engineering and architecture concepts applied to them.</p>

        <script>
            $(window).load(function(){
                $('#bloco-carregando').hide();
                $('.escondido').fadeIn();
                mm_contentHeight();
            });
        </script>
        <div id="bloco-carregando">
            <img src="/images/periodic_table/ajax-loader.gif" />
        </div>
        </div>

        <div class="escondido">
            <div class="clearfix x-hd-and-desktop pull-left">

                <div class="top clearfix">

                    {layers}

                    <div class="layer {layer_name}">
                        {meta_objects}
                        <div class="element e-{l_id}" style="background: {layer_color}">

                            {name}
                            <small>{l_id}</small>
                            <img src="/images/meta_objects/{icon}">
                        </div>
                        {/meta_objects}
                    </div>

                    {/layers}

                </div>

            <div class="layer purpose-goal clearfix x-hd">
                {first_layer}
                {first_meta_objects}
                <div class="element e-{l_id}" style="color: #fff; background: {first_layer_color};">
                   {name}
                    <small>{l_id}</small>
                    <img src="/images/meta_objects/{icon}">
                </div>
                {/first_meta_objects}
                {/first_layer}

           </div>

                <div class="layer purpose-goal clearfix x-desktop">
                    {first_layer_desktop}
                    {first_meta_objects}
                    <div class="element e-{l_id}" style="color: #000; background: {first_layer_color};">
                        {name}
                        <small>{l_id}</small>
                        <img src="/images/meta_objects/{icon}">
                    </div>
                    {/first_meta_objects}
                    {/first_layer_desktop}

                </div>

        </div>
            <div class="clearfix x-mobile">
                <div class="top clearfix">
                {mobile_layers}


                <div class="layer {mobile_layer_name}">
                    {mobile_meta_objects}
                    <div class="element e-{l_id}" style="color: #fff; background: {mobile_layer_color}">

                        {mobile_name}
                        <small>{l_id}</small>
                        <img src="/images/meta_objects/{icon}">
                    </div>
                    {/mobile_meta_objects}
                </div>
                {/mobile_layers}

                </div>
            </div>
        </div>

        <p class="periodic-bottom-text pull-left">The Periodic Table of Enterprise Elements is organized into blocks.<span class="inline-desktop"> The lowest block and the three blocks to the left belong to the business layer, where the block in the middle belong to the information layer and the block to the right belong to the technology layer.</span><span class="inline-mobile"> See captions below for information.</span></p>

        <div class="caption clearfix">

            <div class="layer-caption layer-caption-left clearfix">
                <h2>Business Layer</h2>
                <div class="legend clearfix">
                    <div class="color" style="background: #48d0ff;">
                        &nbsp;
                    </div>
                    <div class="description">Purpose & Goal</div>
                </div>
                <div class="legend clearfix">
                    <div class="color" style="background: #0b98d6;">
                        &nbsp;
                    </div>
                    <div class="description">Business Competency</div>
                </div>
                <div class="legend clearfix">
                    <div class="color" style="background: #014480;">
                        &nbsp;
                    </div>
                    <div class="description">Business Service</div>
                </div>
                <div class="legend clearfix">
                    <div class="color" style="background: #1f992d;">
                        &nbsp;
                    </div>
                    <div class="description">Business Process</div>
                </div>
            </div>

            <div class="layer-caption layer-caption-middle clearfix">
                <h2>Information Layer</h2>
                <div class="legend clearfix">
                    <div class="color" style="background: #f7b206;">
                        &nbsp;
                    </div>
                    <div class="description">Application</div>
                </div>
                <div class="legend clearfix">
                    <div class="color" style="background: #f08d00;">
                        &nbsp;
                    </div>
                    <div class="description">Data</div>
                </div>
            </div>

            <div class="layer-caption layer-caption-right clearfix">
                <h2>Technology Layer</h2>
                <div class="legend clearfix">
                    <div class="color" style="background: #dc5028;">
                        &nbsp;
                    </div>
                    <div class="description">Platform</div>
                </div>
                <div class="legend clearfix">
                    <div class="color" style="background: #a0281e;">
                        &nbsp;
                    </div>
                    <div class="description">Infrastructure</div>
                </div>
            </div>

            <div class="info-group clearfix">

                <div class="info-box info-box-1">
                    <p>The meta object elements are presented in order of number in their layer and group category marked by color code. The meta object elements are listed with the symbol in each box.</p>
                </div>

                <div class="info-box info-box-2">
                    <div class="mini-box">
                        <div class="mini-title">Title</div>
                        <div class="mini-number">#</div>
                        <div class="mini-symbol">Symbol</div>
                    </div>
                </div>

                <div class="info-box info-box-3">
                    <p>Since, by definition, a periodic table incorporates recurring relations, the table can be used to derive semantic relationships between the properties of the meta object. As a result, the periodic table of the enterprise elements provides a useful framework for the enterprise ontology, enterprise taxonomy and enterprise semantic relations.</p>
                </div>

            </div>

        </div>
<div class="clearfix"></div>