<!-- Begin Social Media Notations Content -->
<div class="row">
    <!-- Begin Usability Content -->
    <div class="col-md-4 nopadding">
        {usability_services_arr}
        <!-- Begin Layer Title-->
        <div class="titleBox">
            <div class="smnTitleBox" style="background-color: {sublayer_color};">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
        </div>
        <!-- End Layer Title-->
        <!-- Begin Content-->
        <div class="col-md-12 no-left-padding">
            {cat_pages}
            <div class="media-notations">
                <span class="tooltips" data-original-title="{page_name}"
                      style="display:block; float: left; margin-right: 6px;">
                    <img width="30" height="30" src="{page_notation}" alt="{page_name}"/>
                </span>
                <span class="icons-text">
                    <a href="{page_url}" title="{page_name}">
                        <b>{page_name}</b>
                    </a>
                </span>
                <dl>
                    <dd>{page_description}
                    </dd>
                </dl>
            </div>
            {/cat_pages}
        </div>
        <!-- End Content -->

        {/usability_services_arr}
    </div>
    <!-- End Usability Content -->
    <div class="col-md-4 no-right-padding">
        <!-- Begin Communications Category -->
        {communication_arr}
        <div class="sublayer">
            <!-- Begin Category Title-->
            <div class="titleBox">
                <div class="smnTitleBox" style="background-color: {sublayer_color};">
                    <p>
                        <b>{category_name}</b>
                    </p>
                </div>
            </div>
            <!-- End Category Title-->
            {cat_pages}
            <div class="media-notations">
                <span class="tooltips" data-original-title="{page_name}"
                      style="display:block; float: left; margin-right: 6px;">
                    <img width="30" height="30" src="{page_notation}" alt="{page_name}" />
                </span>
                <span class="icons-text">
                     <a href="{page_url}" title="{page_name}">
                         <b>{page_name}</b>
                     </a>
                </span>
                <dl>
                    <dd>{page_description}
                    </dd>
                </dl>
            </div>
            {/cat_pages}
        </div>
        {/communication_arr}
        <!-- End Communications Category -->

        <!-- Begin Social Media Websites Category -->
        {social_media_websites_arr}
        <div class="sublayer">
            <!-- Begin Category Title-->
            <div class="titleBox">
                <div class="smnTitleBox" style="background-color: {sublayer_color};">
                    <p>
                        <b>{category_name}</b>
                    </p>
                </div>
            </div>
            <!-- End Category Title-->
            {cat_pages}
            <div class="media-notations">
                <span class="tooltips" data-original-title="{page_name}"
                      style="display:block; float: left; margin-right: 6px;">
                    <img width="30" height="30" src="{page_notation}" alt="{page_name}" />
                </span>
                <span class="icons-text">
                     <a href="{page_url}" title="{page_name}">
                         <b>{page_name}</b>
                     </a>
                </span>
                <dl>
                    <dd>{page_description}
                    </dd>
                </dl>
            </div>
            {/cat_pages}
        </div>
        {/social_media_websites_arr}
        <!-- End Social Media Websites Category -->
        </div>
    <!-- Begin Right Column Content -->
    <div class="col-md-4">
        {content_services_arr}

        <div class=titleBox"">
            <div class="smnTitleBox" style="background-color: {sublayer_color};">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
        </div>
        <!-- End Layer Title-->
        <!-- Begin Column-->
        <div class="col-md-12 nopadding">
            {cat_pages}
            <div class="media-notations">
                <span class="tooltips" data-original-title="{page_name}"
                      style="display:block; float: left; margin-right: 6px;">
                    <img width="30" height="30" src="{page_notation}" alt="{page_name}"/>
                </span>
                <span class="icons-text">
                     <a href="{page_url}" title="{page_name}">
                         <b>{page_name}</b>
                     </a>
                </span>
                <dl>
                    <dd>{page_description}
                    </dd>
                </dl>
            </div>
            {/cat_pages}
        </div>
        <!-- End Column -->
        {/content_services_arr}
    </div>
    <!-- End Right Column Content -->
</div>
<!-- End Social Media Notations Content -->