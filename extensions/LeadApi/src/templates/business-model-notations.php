<!-- Begin Business Model NOtations Content -->
<div class="row">
    <!-- Begin Column 1-->
    <div class="col-md-4 nopadding">
        <!-- Begin Task Types -->
        {revenue_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/revenue_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 1-->
    <!-- Begin Column 2-->
    <div class="col-md-4">
        <!-- Begin Task Types -->
        {value_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/value_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 2-->

    <!-- Begin Column 3-->
    <div class="col-md-4">
        <!-- Begin Task Types -->
        {service_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/service_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 3-->

    <!-- Begin Column 4-->
    <div class="col-md-4 nopadding">
        <!-- Begin Task Types -->
        {cost_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/cost_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 4-->

    <!-- Begin Column 5-->
    <div class="col-md-4">
        <!-- Begin Task Types -->
        {performance_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/performance_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 5-->

    <!-- Begin Column 6-->
    <div class="col-md-4">
        <!-- Begin Task Types -->
        {operating_model_arr}
        <!-- Begin Page Category Box -->
        <div class="relPageCategoryBox">
            <!-- Begin Category Name -->
            <div class="relPageBusinessCategoryName">
                <p>
                    <b>{category_name}</b>
                </p>
            </div>
            <!-- End Category Name -->
            <!-- Begin Pages in category -->
            {cat_pages}
            <div class="media-notations relPageBox">
                <!-- Begin Title Box -->
                <div class="relPageTitleBox">
                <span class="tooltips" data-original-title="{page_name}"
                      style="">
                <img src="{page_notation}" alt="{page_name}">
            </span>

             <span class="icons-text relPageTitle">
                <a title="{page_name}" href="{page_url}">{page_name}</a>
             </span>
                </div>
                <!-- End Title Box -->

                <!-- Begin Description Box -->
                <div class="relPageDescription">{page_description}</div>
                <!-- End Description Box -->
            </div>
            {/cat_pages}
            <!-- End Pages in category -->
        </div>
        <!-- End Page Category Box -->
        {/operating_model_arr}
        <!-- End Task Types -->

    </div>
    <!-- End Column 6-->

</div>
<!-- End Business Model NOtations Content -->