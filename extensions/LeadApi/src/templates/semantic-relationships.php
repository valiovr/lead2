<?php
/**
 * Created by PhpStorm.
 * User: Valentin Damyanov
 * Date: 31.7.2015 г.
 * Time: 11:22 ч.
 */

?>
<!-- Custom Js -->
<link href="/skins/lead/resources/styles/select2/select2.css" rel="stylesheet" />
<link href="/skins/lead/resources/styles/semantic-relationships/semantic-relationships.css" rel="stylesheet" />
<script type="text/javascript" src="/skins/lead/resources/js/select2/select2.js"></script>
<script type="text/javascript" src="/skins/lead/resources/js/semantic-relationships/semantic-relationships.js"></script>

<!-- html content  start-->
<style type="text/css">
    .sublayer-box {padding-left: 15px !important;}
</style>

<div class="row">

<div>
    <div class="col-md-5" style="padding-left: 15px;">

        <h3 style="padding-left: 15px;">Choose Primary Layer</h3>

        <form name="layer-buttons">
            <div class="no-left-padding ">
                <div class="form-group">
                    {layers}
                    <div class="col-md-4">

                        <button name="layer-button" data-layer-id="{id}" value="{name}"
                                class="btn btn-lg btn-block meta-rel-button" style="background: {color}; color: white;font-size: 14px;">{name}</button>
                    </div>
                    {/layers}
                </div>
            </div>
        </form>
    </div>
    <!--    secondary layers-->
    <div class="col-md-5 pull-right" style="padding-left: 15px;">

        <h3 style="padding-left: 15px;">Choose Secondary Layer</h3>

        <form name="secondary-layer-buttons">
            <div class="no-left-padding ">
                <div class="form-group">
                    {secondary_layers}
                    <div class="col-md-4">

                        <button name="secondary-layer-button" data-secondary-layer-id="{id}" value="{name}"
                                class="btn btn-lg btn-block meta-rel-button" style="background: {color}; color: white;font-size: 14px;">{name}</button>
                    </div>
                    {/secondary_layers}
                </div>
            </div>
        </form>
    </div>
    <!--    <div style="padding-left: 15px;" class="col-md-5 pull-right"> <h3 style="padding-left: 15px;">Choose Main Layer</h3> <form name="layer-buttons"> <div class="no-left-padding "> <div class="form-group"> <div class="col-md-4"> <button style="background: #0b98d6; color: white;font-size: 14px;" class="btn btn-lg btn-block meta-rel-button" value="Business" data-layer-id="1" name="layer-button">Business</button> </div> <div class="col-md-4"> <button style="background: #f7b206; color: white;font-size: 14px;" class="btn btn-lg btn-block meta-rel-button" value="Application" data-layer-id="2" name="layer-button">Application</button> </div> <div class="col-md-4"> <button style="background: #a0281e; color: white;font-size: 14px;" class="btn btn-lg btn-block meta-rel-button" value="Technology" data-layer-id="3" name="layer-button">Technology</button> </div> </div> </div> </form> </div>-->

    {main_layers}

    <div class="sublayer-box layer-box-{layer_id} col-md-6" style="display: none;">
        <h3 style="padding-left: 15px;">Choose Primary Sub-Layer</h3>

        <form>

            <div class="no-left-padding">
                <div class="form-group col-md-12">
                    {sublayers}
                        <button name="sublayer-button" data-sub-layer-id="{id}"
                                class="btn btn-sm meta-rel-button" style="background: {color}; color: white;font-size: 14px;">{name}</button>
                     {/sublayers}
                </div>
            </div>

        </form>
    </div>
    {/main_layers}

    {secondary_main_layers}

    <div class="secondary-sublayer-box secondary-layer-box-{layer_id} col-md-5 pull-right no-right-padding" style="display: none;">
        <h3 style="padding-left: 30px;">Choose Secondary Sub-Layer</h3>

        <form>

            <div class="no-left-padding">
                <div class="form-group " style="padding-left: 15px;">
                    {sublayers}

                        <button name="secondary-sublayer-button" data-sub-layer-id="{id}"
                                class="btn btn-sm meta-rel-button sec-sublayer-button" style="background: {color}; color: white;font-size: 14px;">{name}</button>
                    {/sublayers}
                </div>
            </div>

        </form>
    </div>
    {/secondary_main_layers}

</div>
<div class="clearfix"></div>

<div class="meta-objects clearfix hidden">
    <form>
        <div class="no-left-padding">
            <div class="form-group">

                {dropdown_data}
                <select class="form-control meta-objects-select hidden" name="sublayer-{sub_layer_id}" data-sublayer-id="{sub_layer_id}" disabled="disabled">

                    <option value="" selected="selected"  disabled="disabled" style="font-weight:bold"><b>{sub_layer_name} (Please select object)</b></option>
                    {meta_objects}
                    <option value="{id}" name="{icon}">{name}</option>
                    {/meta_objects}
                </select>
                {/dropdown_data}

            </div>
        </div>
    </form>
</div>



<div class="relation-div">
    <div class="col-md-12">

        {meta_objects_data}
        <div class="meta-objects-table sublayer-{sub_layer_id}" style="display: none;">
                <table class="table table-bordered table-striped main-table" style="margin-top: 45px;">
                    <thead><th>Meta Objects</th>
                    <th>Semantic Relationship</th>
                    <th>
                        Choose Meta Object
                    </th>
                    </thead>
                    {meta_objects}

                    <tr class="related-tr">
                        <td class="col-md-5 related-objects" style="vertical-align:middle;" data-id="{id}">
                        <span style="display:block; float: left; margin-right: 6px;">
                        <img alt="{name}" src="images/meta_objects/{icon}" class="img-responsive icon-img">
                        </span>
                        <span class="related-object icons-text">
                        {name}
                        </span>
                        </td>
                        <td style="vertical-align:middle;" class="col-md-2">
                        <span style="padding-right: 10px;" class="relation-types" data-object-id="{id}">
                        </span>

                        </td>
                        <td style="vertical-align:middle;" class="col-md-5 foo">

                        </td>
                    </tr>
                    {/meta_objects}

                </table>
            <div class="col-md-4">
                <div style="display: table; height:inherit;" >
                    <table class="selected-meta-table">
                        <tr >
                            <td class="selected-meta-object">

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>



        </div>
        {/meta_objects_data}


    </div>


</div>
<div class="secondary-table" style="display: none">
        <table class="table table-bordered table-striped main-table" style="margin-top: 45px;">
            <thead><th>Meta Objects</th>
            <th>Semantic Relationship</th>
            <th>
                Choose Meta Object
            </th>
            </thead>

            <tr class="related-tr">
                <td class="related-objects" style="vertical-align:middle; width:40%;"">

                </td>
                <td style="vertical-align:middle; width:25%;">


                </td>
                <td style="vertical-align:middle;" class="secondary-foo">

                </td>
            </tr>

        </table>
</div>
    </div>
