<h3><span class="mw-headline"> Objects in this category</span></h3>

<div class="smw-column" style="width:33%;">
    {col_1}
    <div class="smw-column-header">{letter_symbol}</div>
    <ul>
        {letter_members}
        <li><a href="{object_url}">{name}</a></li>
        {/letter_members}
    </ul>
    {/col_1}
</div>
<div class="smw-column" style="width:33%;">
    {col_2}
    <div class="smw-column-header">{letter_symbol}</div>
    <ul>
        {letter_members}
        <li><a href="{object_url}">{name}</a></li>
        {/letter_members}
    </ul>
    {/col_2}
</div>
<div class="smw-column" style="width:33%;">
    {col_3}
    <div class="smw-column-header">{letter_symbol}</div>
    <ul>
        {letter_members}
        <li><a href="{object_url}">{name}</a></li>
        {/letter_members}
    </ul>
    {/col_3}
</div>
<div class="clearfix">&nbsp;</div>