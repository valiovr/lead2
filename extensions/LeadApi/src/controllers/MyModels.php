<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 17.07.2015
 * Time: 14:17
 */
namespace LeadApi;

use LeadApi\LeadApiBase;

class MyModels extends LeadApiBase {

    public function index(){
        global $wgUser;

        $userId = $wgUser->getId();

       	$this->_data['user_id'] = $userId;
        $view = $this->ci_parser->parse('my-models',$this->_data);

        //this regular expression clear ide html formating
        return  preg_replace("/[\\t\\s]+/", " ", trim($view));
    }
}