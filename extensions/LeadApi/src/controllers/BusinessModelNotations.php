<?php
/**
 * Created by PhpStorm.
 * User: Valentin Damyanov
 * Date: 30.09.2015
 * Time: 14:00
 */

namespace LeadApi;


class BusinessModelNotations extends LeadApiBase {


    /**
     * Get inner page data
     * @param $page_name
     * @return bool|mixed|string
     */
    public function getPageContent($page_name){

        global $wgServer;
        global $wgLoadApi;
        global $wgUser;
        $userId = $wgUser->getId();

        //Api Url
        $url =  $wgLoadApi['url'].'/get_business_model_notation_inner_data/'.urlencode(str_replace('&','%26',str_replace('/','___',$page_name)));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        if(!empty($res)) {
            $this->_data['page_title'] = $res['name'];
            $this->_data['page_description'] = $res['description'];
            $this->_data['page_notation'] = $wgServer . '/images/business_model_notations/' . str_replace(' ', '%20', $res['notation']);
            $this->_data['page_layer_name'] = $res['sublayer']['layer']['name'];
            $this->_data['page_layer'] = $res['sublayer']['layer']['name'] . '_Layer';
            $this->_data['page_sublayer_name'] = $res['sublayer']['name'];
            $this->_data['page_sublayer'] = $res['sublayer']['name'] . '_Sub-layer';

            $this->_data['page_object_category'] = $res['object_category']['name'];
            $this->_data['page_object_class'] = $res['object_category']['object_class']['name'];
        }
        $view = $this->ci_parser->parse('business-model-notations-inner',$this->_data);

        return $view;
    }

    /**
     * Main Page data
     * @return mixed
     */
    public function index(){
        global $wgServer;
        global $wgLoadApi;
        global $wgTitle;
        
        $page_name = $wgTitle->mTextform;

        //Api Url
        $url =  $wgLoadApi['url'].'/get_business_model_notation_list/'.urlencode(str_replace('&',')))',str_replace('/','___',$page_name)));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);

        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        //Define Page sections Arrays

        $this->_data['revenue_model_arr'] =array();
        $this->_data['value_model_arr'] =array();
        $this->_data['service_model_arr'] =array();
        $this->_data['cost_model_arr'] =array();
        $this->_data['performance_model_arr'] =array();
        $this->_data['operating_model_arr'] =array();

        if(isset($res['success'])){
            foreach($res['success'] as $object_category){

                //Build Task Types array
                    foreach($object_category['business_model_notations'] as $business_key=>$business_notation){
                        $arr_name = str_replace(' ', '_',strtolower($object_category['name'])).'_arr';

                        if (!isset($this->_data[$arr_name][0]['category_name'])) {
                            $this->_data[$arr_name][0]['category_name'] = $object_category['name'];
                        }
                        $this->_data[$arr_name][0]['cat_pages'][$business_key]['page_name'] = $business_notation['name'];

                        $this->_data[$arr_name][0]['cat_pages'][$business_key]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $business_notation['name']);
                        $this->_data[$arr_name][0]['cat_pages'][$business_key]['page_url'] = str_replace('&','%26',$this->_data[$arr_name][0]['cat_pages'][$business_key]['page_url']);
                        $desc= '';
                        if(str_word_count($business_notation['description'])>100){
                            $desc =implode(' ', array_slice(str_word_count($business_notation['description'], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $business_notation['description'];
                        }


                        $this->_data[$arr_name][0]['cat_pages'][$business_key]['page_description'] =  $desc;
                        $this->_data[$arr_name][0]['cat_pages'][$business_key]['page_notation'] = '/images/business_model_notations/' . $business_notation['notation'];

                    }
            }
        }

        //build page view
        $view = $this->ci_parser->parse('business-model-notations', $this->_data);


        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));

    }

    public function test($page_name){
        $this->_data['page_name'] = $page_name;
        $view = $this->ci_parser->parse('object_category',$this->_data);

        return $view;
    }

}