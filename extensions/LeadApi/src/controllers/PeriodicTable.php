<?php
/**
 * Created by PhpStorm.
 * User: Valentin - valentin@orpheus.bg
 * Date: 22.7.2015 г.
 * Time: 17:15 ч.
 */

namespace LeadApi;

class PeriodicTable extends LeadApiBase
{

    public function index()
    {
        global $wgLoadApi;
        //Api Url
        $url = $wgLoadApi['url'] . '/periodic_table';

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        $firstLayer[] = $res[0];
        foreach ($firstLayer as $key => $val) {
            $val['meta_objects'] = array_reverse($val['meta_objects']);
            $this->_data['first_layer'][$val['id']]['first_layer_color'] = $val['color'];
            $this->_data['first_layer'][$val['id']]['layer_name'] = $val['name'];
            $this->_data['first_layer'][$val['id']]['first_meta_objects'] = $val['meta_objects'];

            foreach ($val['meta_objects'] as $key => $objects) {
                $namesArr = explode('(', $objects['name']);

                $objects['name'] = $namesArr[0];
                $this->_data['first_layer'][$val['id']]['first_meta_objects'][$key]['name'] = $objects['name'];
            }

        }
        $this->_data['first_layer_desktop'] = $this->_data['first_layer'];
        foreach ($res as $resKey => $table) {

            $this->_data['mobile_layers'][$table['id']]['id'] = $table['id'];
            $this->_data['mobile_layers'][$table['id']]['mobile_layer_id'] = $table['layer_id'];
            $this->_data['mobile_layers'][$table['id']]['mobile_layer_color'] = $table['color'];
            $this->_data['mobile_layers'][$table['id']]['mobile_layer_name'] = strtolower(str_replace(' ', '-', $table['name']));
            $this->_data['mobile_layers'][$table['id']]['mobile_meta_objects'] = $table['meta_objects'];


            if ($resKey == 0) continue;
            $table['meta_objects'] = array_reverse($table['meta_objects']);
            $this->_data['layers'][$table['id']]['layer_color'] = $table['color'];
            $this->_data['layers'][$table['id']]['layer_name'] = strtolower(str_replace(' ', '-', $table['name']));
            $this->_data['layers'][$table['id']]['meta_objects'] = $table['meta_objects'];


            foreach ($table['meta_objects'] as $k => $obj) {
                if ($obj['id'] == 53) {
                    unset($obj['name']);
                    $obj['name'] = 'App/System Report';
                } elseif ($obj['id'] == 54) {
                    unset($obj['name']);
                    $obj['name'] = 'App/System';
                } elseif($obj['id'] == 51) {
                    unset($obj['name']);
                    $obj['name'] = 'App/System Flow';


                }
                $namesLayersArr = explode('(', $obj['name']);

                $obj['name'] = $namesLayersArr[0];
                $this->_data['layers'][$table['id']]['meta_objects'][$k]['name'] = $obj['name'];
            }

        }
        foreach ($this->_data['mobile_layers'] as $dataKey => $mobile) {


            foreach ($mobile['mobile_meta_objects'] as $mobKey => $mob) {
                $namesMobileLayersArr = explode('(', $mob['name']);
                $mob['name'] = $namesMobileLayersArr[0];
                $this->_data['mobile_layers'][$mobile['id']]['mobile_meta_objects'][$mobKey]['mobile_name'] = $mob['name'];
            }

        }
        $this->_data['mobile_layers'][1]['mobile_layer_name'] = 'purpose-goal';
        $this->_data['layers'] = array_reverse($this->_data['layers']);
        $this->_data['width_table'] = '1200';
        $this->_data['unit'] = 'px';
        $this->_data['width_cel'] = floor($this->_data['width_table'] / 17);
        $this->_data['ceil'] = ceil(($this->_data['width_cel'] / 2.2)) . $this->_data['unit'];


        $this->_data['width_cel-x2'] = ceil($this->_data['width_cel'] * 2) . $this->_data['unit'];
        $this->_data['width_cel-x3'] = ceil($this->_data['width_cel'] * 3) . $this->_data['unit'];
        $this->_data['width_cel-x4'] = ceil($this->_data['width_cel'] * 4) . $this->_data['unit'];
        $this->_data['width_cel-x4'] = ceil($this->_data['width_cel'] * 17) . $this->_data['unit'];

        //this regular expression clear IDE html formatting
        $view = $this->ci_parser->parse('periodic-table', $this->_data);
        return preg_replace("/[\\t\\s]+/", " ", trim($view));
    }

}