<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 17.07.2015
 * Time: 14:17
 */
namespace LeadApi;

use LeadApi\LeadApiBase;

class MetaObjectDescriptions extends LeadApiBase {

    public function index(){
        global $wgLoadApi;
        global $wgUser;

        $userId = $wgUser->getId();

        //Api Url
        $url =  $wgLoadApi['url'].'/meta_objects_desc/'.$userId;

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        $this->_data['layers']=array();
        $this->_data['main_layers']=array();
        $this->_data['meta_objects_data']=array();
        if(!empty($res) && !isset($res['error'])){
            foreach($res as $layer){
                $this->_data['layers'][$layer['id']]['id'] =$layer['id'];
                $this->_data['layers'][$layer['id']]['name'] =$layer['name'];
                foreach($layer['sublayers'] as $sublayer){

                    $this->_data['main_layers'][$sublayer['layer_id']]['layer_id']=$sublayer['layer_id'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['id']=$sublayer['id'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['name']=$sublayer['name'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['color']=$sublayer['color'];

                    $this->_data['meta_objects_data'][$sublayer['id']]['sub_id'] = $sublayer['id'];
                    $this->_data['meta_objects_data'][$sublayer['id']]['sub_title'] = $sublayer['name'];
                    $this->_data['meta_objects_data'][$sublayer['id']]['sub_color'] = $sublayer['color'];
                    $this->_data['meta_objects_data'][$sublayer['id']]['main_layer_id'] = $sublayer['layer_id'];

                    $this->_data['meta_objects_data'][$sublayer['id']]['col_one']= array();

                    $this->_data['meta_objects_data'][$sublayer['id']]['col_two'] = array();
                    $this->_data['meta_objects_data'][$sublayer['id']]['col_three'] = array();
              // var_dump('<pre>');
              // print_r($sublayer['meta_objects']);
              // die();
                    foreach($sublayer['meta_objects'] as $object){

                        //Initialize and check if Meta Object is modified
                        $isModified = '';
                        $isNotModified ='';
                        if($object['user_object']!=NULL){
                            $isModified = 'metaObjectModified';
                        }
                        if($object['column_number']==1){
                            $this->_data['meta_objects_data'][$sublayer['id']]['col_one'][$object['id']]  = $object;
                            $this->_data['meta_objects_data'][$sublayer['id']]['col_one'][$object['id']]['is_modified_mo']  = $isModified;
                        }else if($object['column_number']==2){
                            $this->_data['meta_objects_data'][$sublayer['id']]['col_two'][$object['id']] = $object;
                            $this->_data['meta_objects_data'][$sublayer['id']]['col_two'][$object['id']]['is_modified_mo']  = $isModified;
                        }else{
                          $this->_data['meta_objects_data'][$sublayer['id']]['col_three'][$object['id']] = $object;
                          $this->_data['meta_objects_data'][$sublayer['id']]['col_three'][$object['id']]['is_modified_mo']  = $isModified;
                        }
                    }

                }
            }
        }

        $view = $this->ci_parser->parse('meta-object-descriptions',$this->_data);

        //this regular expression clear ide html formatting
        return  preg_replace("/[\\t\\s]+/", " ", trim($view));
    }

    public function getPageContent($page_name){
        global $wgServer;
        global $wgLoadApi;
        global $wgUser;

        $userId = $wgUser->getId();

        //Api Url
        $url =  $wgLoadApi['url'].'/get_meta_object_internal_data/'.urlencode(str_replace('/','___',$page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);


        $this->_data['semantic_relationships'] = array();
        if(!empty($res)){
            $this->_data['page_title'] =$res['name'];
            $this->_data['page_description'] =$res['description'];
            $this->_data['page_notation'] =$wgServer.'/images/meta_objects/'.str_replace(' ','%20', $res['icon']);
            $this->_data['page_layer_name'] =$res['sublayer']['layer']['name'];
            $this->_data['page_layer'] =$res['sublayer']['name'].'_layer';
            $this->_data['page_sublayer_name'] =$res['sublayer']['name'];
            $this->_data['page_sublayer'] =$res['sublayer']['name'].'_Sub-Layer';

            if(!empty($res['object_relations_inverse'])){
                foreach($res['object_relations_inverse'] as $rel_key=>$rel_data){
                    if($rel_data['relation_type']!= NULL){
                        $this->_data['semantic_relationships'][$rel_key]['rel_object'] = $rel_data['meta_object']['name'];
                        $this->_data['semantic_relationships'][$rel_key]['relation_type'] = $rel_data['relation_type']['name'];
                        $this->_data['semantic_relationships'][$rel_key]['meta_object'] = $rel_data['related_object']['name'];
                    }
                }

            }
        }

       // echo '<pre>'; var_dump(count($this->_data['semantic_relationships'])); die;
        $view = $this->ci_parser->parse('meta-object-descriptions-inner',$this->_data);

        return $view;
    }


}