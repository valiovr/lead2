<?php

/**
 * Created by PhpStorm.
 * User: Valentin Damyanov
 * Date: 31.07.2015
 * Time: 11:20
 */
namespace LeadApi;

class SemanticRelationships extends LeadApiBase
{

    public function index()
    {

        global $wgLoadApi;

        //Api Url
        $url = $wgLoadApi['url'] . '/semantic_relationships';

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

//        var_dump($res); die;

        //Close connection
        curl_close($ch);
        $this->_data['layers'] = array();
        $this->_data['main_layers'] = array();
        $this->_data['meta_objects_data'] = array();
        $this->_data['all_meta_objects'] = array();
        $this->_data['object_relations_data'] = array();
        $this->_data['relation_type'] = array();
        $this->_data['availableMetObjects'] = array();

        if (!empty($res) && !isset($res['error'])) {
            foreach ($res as $layer) {
            if($layer['name'] == 'Application'){
                    $layer['name'] = 'Information';

                }
                $this->_data['layers'][$layer['id']]['id'] = $layer['id'];
                $this->_data['layers'][$layer['id']]['name'] = $layer['name'];
                $this->_data['layers'][$layer['id']]['color'] = $layer['color'];

                foreach ($layer['sublayers'] as $sublayer) {
                    if($sublayer['name'] == 'Purpose & Goal (Value)') {
                        $sublayer['name'] = 'Purpose & Goal';
                    }elseif($sublayer['name'] == 'Information System'){
                        $sublayer['name'] = 'Application';
                    }
                    $this->_data['main_layers'][$sublayer['layer_id']]['layer_id']=$sublayer['layer_id'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['id']=$sublayer['id'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['name']=$sublayer['name'];
                    $this->_data['main_layers'][$sublayer['layer_id']]['sublayers'][$sublayer['id']]['color']=$sublayer['color'];

                    foreach ($sublayer['meta_objects'] as $meta_objects) {

//                        array_push($this->_data['availableMetObjects'], $layer['name'] .' | '. $sublayer['name'] .' | '. $meta_objects['name']);
                        $this->_data['all_meta_objects'][$meta_objects['id']]['id'] =  $meta_objects['id'];
                        $this->_data['all_meta_objects'][$meta_objects['id']]['icon'] =  $meta_objects['icon'];
                        $this->_data['all_meta_objects'][$meta_objects['id']]['name'] = $layer['name'] .' | '. $sublayer['name'] .' | '. $meta_objects['name'];

                        $this->_data['dropdown_data'][$meta_objects['sub_layer_id']]['sub_layer_id'] = $meta_objects['sub_layer_id'];
                        $this->_data['dropdown_data'][$meta_objects['sub_layer_id']]['sub_layer_name'] = $sublayer['name'];
                        $this->_data['dropdown_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['id'] = $meta_objects['id'];
                        $this->_data['dropdown_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['icon'] = $meta_objects['icon'];
                        $this->_data['dropdown_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['name'] = $meta_objects['name'];

                        $this->_data['meta_objects_data'][$meta_objects['sub_layer_id']]['sub_layer_id'] = $meta_objects['sub_layer_id'];
                        $this->_data['meta_objects_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['id'] = $meta_objects['id'];
                        $this->_data['meta_objects_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['icon'] = $meta_objects['icon'];
                        $this->_data['meta_objects_data'][$meta_objects['sub_layer_id']]['meta_objects'][$meta_objects['id']]['name'] = $meta_objects['name'];

//                        foreach ($meta_objects['object_relations'] as $object_relations) {
//                            $this->_data['object_relations_data'][$object_relations['object_id']]['id'] = $object_relations['id'];
//                            $this->_data['object_relations_data'][$object_relations['object_id']]['relation_type_id'] = $object_relations['relation_type_id'];
//                            $this->_data['object_relations_data'][$object_relations['object_id']]['related_object_id'] = $object_relations['related_object_id'];
//                            $this->_data['object_relations_data'][$object_relations['object_id']]['object_id'] = $object_relations['object_id'];
//
//
//                            if (!is_null($object_relations['relation_type'])) {
//
//                                $this->_data['relation_type'][$object_relations['id']]['id'] = $object_relations['relation_type']['id'];
//                                $this->_data['relation_type'][$object_relations['id']]['name'] = $object_relations['relation_type']['name'];
//
//                            }
//
//                        }
                    }

                }

            }
        }
        $this->_data['secondary_layers'] = $this->_data['layers'];
        $this->_data['secondary_main_layers'] = $this->_data['main_layers'];

        $view = $this->ci_parser->parse('semantic-relationships', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));
    }

}