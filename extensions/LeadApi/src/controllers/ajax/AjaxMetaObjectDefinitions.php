<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 26.08.2015
 * Time: 15:30
 */

namespace LeadApi\Ajax;

use LeadApi\LeadApiBase;


class AjaxMetaObjectDefinitions extends LeadApiBase {

    /**
     * Get Meta Object by ID
     */
    public static function getMetaObjectById() {
        global $wgLoadApi;

        //Meta Object ID
        $postData = $_POST['data'];
        
        //Api Url
        $url = $wgLoadApi['url'] . '/get_meta_object_by_id/' . $postData['id'];

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);
        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        echo $result_api;
        die;
    }

    public function createMetaObjectInstance(){
        global $wgLoadApi;
        global $wgUser;

        //New Instance Meta oObject data
        $postData = $_POST['data'];
        $postData['user_id'] = $wgUser->getId();

        //Api Url
        $url = $wgLoadApi['url'] . '/create_new_meta_object_instance';

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);
        $res = json_decode($result_api, true);

        //Close connection
        curl_close($ch);
        if(array_key_exists('errors',$res)){
            echo $result_api;
            die;
        }else{
            echo json_encode(array('success'=>'Created successfully meta object instance.'));
            die;
        }

    }


}