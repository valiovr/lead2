<?php
/**
 * Created by PhpStorm.
 * User: valio
 * Date: 4.8.2015 г.
 * Time: 14:51 ч.
 */
namespace LeadApi\Ajax;
use LeadApi\LeadApiBase;
class AjaxSemanticRelationships extends LeadApiBase {

    public static function getObjectRelations()
    {
        $postData = json_encode($_POST['data']);
        global $wgLoadApi;

        //Api Url
        $url = $wgLoadApi['url'] . '/object_relations/'.$postData;
        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);
        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);


        //Close connection
        curl_close($ch);
        echo $result_api;
        die;
    }
}