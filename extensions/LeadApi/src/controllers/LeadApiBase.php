<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 17.07.2015
 * Time: 14:57
 */
namespace LeadApi;
use LeadApi\Lib;

class LeadApiBase {
    public $_data= array();
    public $ci_parser = null;
    public function __construct(){

        //load parser
        if(!isset($this->ci_parser)) {
            $this->ci_parser = new \LeadApi\Lib\CI_Parser();
        }
    }
}