<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 17.07.2015
 * Time: 14:17
 */
namespace LeadApi;

use LeadApi\LeadApiBase;

class MyObjects extends LeadApiBase {

    public function index(){
        global $wgUser;

        $userId = $wgUser->getId();

       	$this->_data['user_id'] = $userId;

       	if(!isset($_GET["instance"]))
       	$this->_data['instance_id']=-1;
       	else
       	$this->_data['instance_id']=intval(htmlspecialchars($_GET["instance"]));

        $view = $this->ci_parser->parse('my-objects',$this->_data);

        //this regular expression clear ide html formating
        return  preg_replace("/[\\t\\s]+/", " ", trim($view));
    }
}