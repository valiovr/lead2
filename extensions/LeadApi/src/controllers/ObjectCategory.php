<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 30.09.2015
 * Time: 13:43
 */

namespace LeadApi;


class ObjectCategory extends LeadApiBase {

    public function index() {
        global $wgLoadApi;
        global $wgTitle;
        global $wgServer;

        $page_name = $wgTitle->mTextform;

        //Api Url
        $url = $wgLoadApi['url'] . '/get_object_category_members/' . urlencode(str_replace('/', '___', $page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('lead-api: ' . $wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        $this->_data['col_1'] = array();
        $this->_data['col_2'] = array();
        $this->_data['col_3'] = array();
        $lettersArr = array();
        if (!empty($res)) {

            foreach($res as $key=>$objectMember){

                $lettersArr[strtoupper(substr($objectMember['name'],0,1))]['letter_symbol'] =strtoupper(substr($objectMember['name'],0,1));
                $lettersArr[strtoupper(substr($objectMember['name'],0,1))]['letter_members'][$key]['name'] =$objectMember['name'];
                $lettersArr[strtoupper(substr($objectMember['name'],0,1))]['letter_members'][$key]['object_url'] =$wgServer . '/index.php?title=' . str_replace(' ', '_', $objectMember['name']);
            }
            $perPageNum = count($lettersArr)/3;

            if(is_float ($perPageNum)){
                $elementsPerColumn = intval($perPageNum+1);
            }else{
                $elementsPerColumn = intval($perPageNum);
            }
            
            $br = 0;
            foreach($lettersArr as $letterMember){

            if($br<$elementsPerColumn){
                $this->_data['col_1'][] = $letterMember;
            }else if($br>=$elementsPerColumn && $br<($perPageNum*2)){
                $this->_data['col_2'][] = $letterMember;
            }else if($br>=($perPageNum*2)){
                $this->_data['col_3'][] = $letterMember;
            }
                $br++;
            }
        }
//        echo '<pre>'; var_dump($this->_data);die;
        //build page view
        $view = $this->ci_parser->parse('object_category', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));
    }
}