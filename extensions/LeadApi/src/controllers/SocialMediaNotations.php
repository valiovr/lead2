<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 14.09.2015
 * Time: 10:23
 */

namespace LeadApi;


class SocialMediaNotations extends LeadApiBase {

    /**
     * @deprecated
     * Build data to be displayed on Social Media Notations
     * @return mixed
     */
    public function index_old() {

        global $wgServer;

        //Init arrays
        $this->_data['usability_arr'] = array();
        $this->_data['usability_arr'][0]['col_left'] = array();
        $this->_data['usability_arr'][0]['col_right'] = array();
        $this->_data['communication_arr'] = array();
        $this->_data['websites_arr'] = array();
        $this->_data['content_arr'] = array();
        
        //Get related pages by SMW API
        $smnPages = $this->_getSocialMediaNotationSubPages();
        $descriptionsArr = $this->_getPagesDescriptions($smnPages);

        //Check is exists related pages
        if (!empty($smnPages)) {
            //Init Counters
            $counterUsabilityLeft = 0;
            $counterUsabilityRight = 0;
            $counterCommunication = 0;
            $counterWebsites = 0;
            $counterContentServices = 0;

            //Build Page data
            foreach ($smnPages as $key => $subPage) {
                $image_name = str_replace(' ', '_', str_replace('File:', '', $subPage['printouts']['Has image'][0]['fulltext']));
                $image_hash = md5($image_name);
                $first_dir = substr($image_hash, 0, 1);
                $second_dir = substr($image_hash, 0, 2);

                if ($subPage['printouts']['la_category'][0]['fulltext'] === 'Usability Services') {
                    //Set Usability Services category name
                    if (!isset($this->_data['usability_arr'][0]['category_name'])) {
                        $this->_data['usability_arr'][0]['category_name'] = $subPage['printouts']['la_category'][0]['fulltext'];
                    }

                    if ($subPage['printouts']['la_col'][0]['fulltext'] == 1) {

                        $this->_data['usability_arr'][0]['col_left'][$counterUsabilityLeft]['page_name'] = $subPage['fulltext'];
                        $this->_data['usability_arr'][0]['col_left'][$counterUsabilityLeft]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $subPage['fulltext']);
                        $this->_data['usability_arr'][0]['col_left'][$counterUsabilityLeft]['page_description'] = isset($descriptionsArr[$subPage['fulltext']]) ? $descriptionsArr[$subPage['fulltext']] : '';
                        $this->_data['usability_arr'][0]['col_left'][$counterUsabilityLeft]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                        $counterUsabilityLeft++;
                    } else {
                        $this->_data['usability_arr'][0]['col_right'][$counterUsabilityRight]['page_name'] = $subPage['fulltext'];
                        $this->_data['usability_arr'][0]['col_right'][$counterUsabilityRight]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $subPage['fulltext']);
                        $this->_data['usability_arr'][0]['col_right'][$counterUsabilityRight]['page_description'] = isset($descriptionsArr[$subPage['fulltext']]) ? $descriptionsArr[$subPage['fulltext']] : '';
                        $this->_data['usability_arr'][0]['col_right'][$counterUsabilityRight]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                        $counterUsabilityRight++;
                    }
                } else if ($subPage['printouts']['la_category'][0]['fulltext'] === 'Communication') {
                    //Set Communication category name
                    if (!isset($this->_data['communication_arr'][0]['com_category_name'])) {
                        $this->_data['communication_arr'][0]['com_category_name'] = $subPage['printouts']['la_category'][0]['fulltext'];
                    }

                    $this->_data['communication_arr'][0]['comunication_pages'][$counterCommunication]['com_page_name'] = $subPage['fulltext'];
                    $this->_data['communication_arr'][0]['comunication_pages'][$counterCommunication]['com_page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $subPage['fulltext']);
                    $this->_data['communication_arr'][0]['comunication_pages'][$counterCommunication]['com_page_description'] = isset($descriptionsArr[$subPage['fulltext']]) ? $descriptionsArr[$subPage['fulltext']] : '';
                    $this->_data['communication_arr'][0]['comunication_pages'][$counterCommunication]['com_page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;

                    $counterCommunication++;
                } else if ($subPage['printouts']['la_category'][0]['fulltext'] === 'Social Media Websites') {
                    //Set Social Media Websites category name
                    if (!isset($this->_data['websites_arr'][0]['ws_category_name'])) {
                        $this->_data['websites_arr'][0]['ws_category_name'] = $subPage['printouts']['la_category'][0]['fulltext'];
                    }

                    $this->_data['websites_arr'][0]['websites_pages'][$counterWebsites]['ws_page_name'] = $subPage['fulltext'];
                    $this->_data['websites_arr'][0]['websites_pages'][$counterWebsites]['ws_page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $subPage['fulltext']);
                    $this->_data['websites_arr'][0]['websites_pages'][$counterWebsites]['ws_page_description'] = isset($descriptionsArr[$subPage['fulltext']]) ? $descriptionsArr[$subPage['fulltext']] : '';
                    $this->_data['websites_arr'][0]['websites_pages'][$counterWebsites]['ws_page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;

                    $counterWebsites++;
                } else {
                    //Set Social Media Websites category name
                    if (!isset($this->_data['content_arr'][0]['cs_category_name'])) {
                        $this->_data['content_arr'][0]['cs_category_name'] = $subPage['printouts']['la_category'][0]['fulltext'];
                    }

                    $this->_data['content_arr'][0]['content_services_pages'][$counterContentServices]['cs_page_name'] = $subPage['fulltext'];
                    $this->_data['content_arr'][0]['content_services_pages'][$counterContentServices]['cs_page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $subPage['fulltext']);
                    $this->_data['content_arr'][0]['content_services_pages'][$counterContentServices]['cs_page_description'] = isset($descriptionsArr[$subPage['fulltext']]) ? $descriptionsArr[$subPage['fulltext']] : '';
                    $this->_data['content_arr'][0]['content_services_pages'][$counterContentServices]['cs_page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;

                    $counterContentServices++;
                }
            }

        }

        //build page view
        $view = $this->ci_parser->parse('social-media-notations', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));

    }

    /**
     * @deprecated
     * Make Request to SMW API to get related pages to Social Media Notations
     *
     * @return mixed
     */
    private function _getSocialMediaNotationSubPages() {
        global $wgServer;

        //Api Url
        $url = $wgServer . '/api.php?action=askargs&conditions=la_pgrel::pg-smn&printouts=la_class|la_category|Has%20image|la_col|la_position&parameters=sort%3Dla_position|order%3Dasc&format=json';

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        return $res['query']['results'];
    }

    /**
     * @deprecated
     * Extract description section from pages
     *
     * @param array $smnPages
     * @return array
     */
    private function _getPagesDescriptions($smnPages = array()) {
        global $wgServer;

        $urlParams = array();
        foreach ($smnPages as $page) {
            $urlParams[] = $page['fulltext'];
        }
        //Build url
        $url = $wgServer . '/api.php/?action=query&prop=revisions&rvprop=content&rvsection=1&format=json&titles=' . str_replace(' ', '%20', implode('|', $urlParams));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        $descriptionsArr = array();
        if (!empty($res['query']['pages'])) {
            //Build Descriptions Array
            foreach ($res['query']['pages'] as $page) {
                $descriptionsArr[$page['title']] = preg_replace('/^[ \t]*[\r\n]+/m', '',
                    preg_replace('/\[\[.*?\]\]/', '', str_replace('=== Description ===', '',
                        $page['revisions'][0]['*'])));
            }
        }

        return $descriptionsArr;
    }


    /**
     * Get inner page data
     * @param $page_name
     * @return bool|mixed|string
     */
    public function getPageContent($page_name){

        global $wgServer;
        global $wgLoadApi;
        global $wgUser;

        $userId = $wgUser->getId();

        //Api Url
        $url =  $wgLoadApi['url'].'/get_social_media_notation_inner_data/'.urlencode(str_replace('/','___',$page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        if(!empty($res)) {
            $this->_data['page_title'] = $res['name'];
            $this->_data['page_description'] = $res['description'];
            $this->_data['page_notation'] = $wgServer . '/images/social_media_notations/' . str_replace(' ', '%20', $res['notation']);
            $this->_data['page_layer_name'] = $res['sublayer']['layer']['name'];
            $this->_data['page_layer'] = $res['sublayer']['layer']['name'] . '_Layer';
            $this->_data['page_sublayer_name'] = $res['sublayer']['name'];
            $this->_data['page_sublayer'] = $res['sublayer']['name'] . '_Sub-layer';

            $this->_data['page_object_category'] = $res['object_category']['name'];
            $this->_data['page_object_class'] = $res['object_category']['object_class']['name'];
        }
        $view = $this->ci_parser->parse('social-media-notations-inner',$this->_data);

        return $view;
    }

    /**
     * Main Page data
     * @return mixed
     */
    public function index(){
        global $wgServer;
        global $wgLoadApi;
        global $wgTitle;

        $page_name = $wgTitle->mTextform;

        //Api Url
        $url =  $wgLoadApi['url'].'/get_social_media_notation_list/'.urlencode(str_replace('/','___',$page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        //Define Page sections Arrays
        $this->_data['usability_services_arr'] =array();
        $this->_data['communication_arr'] =array();
        $this->_data['social_media_websites_arr'] =array();
        $this->_data['content_services_arr'] =array();


        if(isset($res['success'])){
            foreach($res['success'] as $object_category){

                //Build Task Types array
                foreach($object_category['social_media_notations'] as $social_media_key=>$social_median_notation){
                    $arr_name = str_replace(' ', '_',strtolower($object_category['name'])).'_arr';

                    if (!isset($this->_data[$arr_name][0]['category_name'])) {
                        $this->_data[$arr_name][0]['category_name'] = $object_category['name'];
                        $this->_data[$arr_name][0]['sublayer_color'] = $social_median_notation['sublayer']['color'];
                    }
                    $this->_data[$arr_name][0]['cat_pages'][$social_media_key]['page_name'] = $social_median_notation['name'];
                    $this->_data[$arr_name][0]['cat_pages'][$social_media_key]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $social_median_notation['name']);
                    $desc= '';
                    if(str_word_count($social_median_notation['description'])>100){
                        $desc =implode(' ', array_slice(str_word_count($social_median_notation['description'], 2), 0, 100)). ' ...';
                    }else{
                        $desc =  $social_median_notation['description'];
                    }

                    $this->_data[$arr_name][0]['cat_pages'][$social_media_key]['page_description'] =  $desc;
                    $this->_data[$arr_name][0]['cat_pages'][$social_media_key]['page_notation'] = '/images/social_media_notations/' . $social_median_notation['notation'];
                }
            }
        }

        //build page view
        $view = $this->ci_parser->parse('social-media-notations', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));

    }
}