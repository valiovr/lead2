<?php
/**
 * Created by PhpStorm.
 * User: Miroslav Marinov
 * Date: 17.09.2015
 * Time: 11:00
 */

namespace LeadApi;


class XbpmnNotations extends LeadApiBase {

    public function index_old(){
        global $wgServer;

        //Init page data arrays
        $this->_data['task_types_arr'] = array();
        $this->_data['gateways_arr'] = array();
        $this->_data['markers_arr'] = array();
        $this->_data['flows_arr'] = array();
        $this->_data['objects_arr'] = array();
        $this->_data['events_arr'] = array();
        $this->_data['conversations_arr'] = array();
        $this->_data['activities_arr'] = array();

        //Get Related pages
        $relatedPages = $this->_getXbpmnRelatedPages();
        //Extract related pages descriptions
        $relPagesDescriptions = $this->_getRelatedPagesDescriptions($relatedPages);

        if(!empty($relatedPages)){
            //Init array counters
            $counterTaskTypes = 0;
            $counterGateways = 0;
            $counterMarkers = 0;
            $counterFlows = 0;
            $counterObjects = 0;
            $counterEvents = 0;
            $counterConversations = 0;
            $counterActivities = 0;

            foreach($relatedPages as $relatedPage){
                //Extract image sub directories
                $image_name = str_replace(' ', '_', str_replace('File:', '', $relatedPage['printouts']['Has image'][0]['fulltext']));
                $image_hash = md5($image_name);
                $first_dir = substr($image_hash, 0, 1);
                $second_dir = substr($image_hash, 0, 2);

                if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Task Types') {
                    //Set Task Types category name
                    if (!isset($this->_data['task_types_arr'][0]['category_name'])) {
                        $this->_data['task_types_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['task_types_arr'][0]['cat_pages'][$counterTaskTypes]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['task_types_arr'][0]['cat_pages'][$counterTaskTypes]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);
                   $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                        $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['task_types_arr'][0]['cat_pages'][$counterTaskTypes]['page_description'] =  $desc;
                    $this->_data['task_types_arr'][0]['cat_pages'][$counterTaskTypes]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterTaskTypes++;

                }else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Gateways') {
                    //Set Gateways category name
                    if (!isset($this->_data['gateways_arr'][0]['category_name'])) {
                        $this->_data['gateways_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['gateways_arr'][0]['cat_pages'][$counterGateways]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['gateways_arr'][0]['cat_pages'][$counterGateways]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['gateways_arr'][0]['cat_pages'][$counterGateways]['page_description'] = $desc;
                    $this->_data['gateways_arr'][0]['cat_pages'][$counterGateways]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterGateways++;

                }else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Markers') {
                    //Set Markers category name
                    if (!isset($this->_data['markers_arr'][0]['category_name'])) {
                        $this->_data['markers_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['markers_arr'][0]['cat_pages'][$counterMarkers]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['markers_arr'][0]['cat_pages'][$counterMarkers]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['markers_arr'][0]['cat_pages'][$counterMarkers]['page_description'] = $desc;
                    $this->_data['markers_arr'][0]['cat_pages'][$counterMarkers]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterMarkers++;

                }else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Flows') {
                    //Set Flows category name
                    if (!isset($this->_data['flows_arr'][0]['category_name'])) {
                        $this->_data['flows_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['flows_arr'][0]['cat_pages'][$counterFlows]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['flows_arr'][0]['cat_pages'][$counterFlows]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['flows_arr'][0]['cat_pages'][$counterFlows]['page_description'] =$desc;
                    $this->_data['flows_arr'][0]['cat_pages'][$counterFlows]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterFlows++;

                }else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Objects') {
                    //Set Objects category name
                    if (!isset($this->_data['objects_arr'][0]['category_name'])) {
                        $this->_data['objects_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['objects_arr'][0]['cat_pages'][$counterObjects]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['objects_arr'][0]['cat_pages'][$counterObjects]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['objects_arr'][0]['cat_pages'][$counterObjects]['page_description'] = $desc;
                    $this->_data['objects_arr'][0]['cat_pages'][$counterObjects]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterObjects++;

                }else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Events') {
                    //Set Events category name
                    if (!isset($this->_data['events_arr'][0]['category_name'])) {
                        $this->_data['events_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['events_arr'][0]['cat_pages'][$counterEvents]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['events_arr'][0]['cat_pages'][$counterEvents]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['events_arr'][0]['cat_pages'][$counterEvents]['page_description'] = $desc;
                    $this->_data['events_arr'][0]['cat_pages'][$counterEvents]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterEvents++;

                }
                else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Conversations') {
                    //Set Events category name
                    if (!isset($this->_data['conversations_arr'][0]['category_name'])) {
                        $this->_data['conversations_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['conversations_arr'][0]['cat_pages'][$counterConversations]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['conversations_arr'][0]['cat_pages'][$counterConversations]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['conversations_arr'][0]['cat_pages'][$counterConversations]['page_description'] =  $desc;
                    $this->_data['conversations_arr'][0]['cat_pages'][$counterConversations]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterConversations++;

                }
                else if ($relatedPage['printouts']['la_category'][0]['fulltext'] === 'Activities') {
                    //Set Events category name
                    if (!isset($this->_data['activities_arr'][0]['category_name'])) {
                        $this->_data['activities_arr'][0]['category_name'] = $relatedPage['printouts']['la_category'][0]['fulltext'];
                    }
                    $this->_data['activities_arr'][0]['cat_pages'][$counterActivities]['page_name'] = $relatedPage['fulltext'];
                    $this->_data['activities_arr'][0]['cat_pages'][$counterActivities]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $relatedPage['fulltext']);

                    $desc= '';
                    if(isset($relPagesDescriptions[$relatedPage['fulltext']])) {
                        if(str_word_count($relPagesDescriptions[$relatedPage['fulltext']])>100){
                            $desc =implode(' ', array_slice(str_word_count($relPagesDescriptions[$relatedPage['fulltext']], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $relPagesDescriptions[$relatedPage['fulltext']];
                        }
                    }

                    $this->_data['activities_arr'][0]['cat_pages'][$counterActivities]['page_description'] =  $desc;
                    $this->_data['activities_arr'][0]['cat_pages'][$counterActivities]['page_notation'] = '/images/' . $first_dir . '/' . $second_dir . '/' . $image_name;
                    $counterActivities++;

                }
            }
        }
        

        //build page view
        $view = $this->ci_parser->parse('x-bpmn-notations', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));

    }

    /**
     * Make Request to SMW API to get related pages to Social Media Notations
     *
     * @return mixed
     */
    private function _getXbpmnRelatedPages() {
        global $wgServer;

        //Api Url
        $url = $wgServer . '/api.php?action=askargs&conditions=la_pgrel::pg-xbpmn&printouts=la_class|la_category|Has%20image|la_col|la_position&parameters=limit%3D10000|sort%3Dla_position|order%3Dasc&format=json';

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        return $res['query']['results'];
    }

    /**
     * Extract description section from pages
     *
     * @param array $relatedPages
     * @return array
     */
    private function _getRelatedPagesDescriptions($relatedPages = array()) {
        global $wgServer;

        $urlParams = array();
        foreach ($relatedPages as $page) {
            $urlParams[] = $page['fulltext'];
        }
        //Build url
        $url = $wgServer . '/api.php/?action=query&prop=revisions&rvprop=content&rvsection=1&format=json&titles=' . str_replace(' ', '%20', implode('|', $urlParams));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        $descriptionsArr = array();
        if (!empty($res['query']['pages'])) {
            //Build Descriptions Array
            foreach ($res['query']['pages'] as $page) {
                $descriptionsArr[$page['title']] = preg_replace('/^[ \t]*[\r\n]+/m', '',
                    preg_replace('/\[\[.*?\]\]/', '', str_replace('=== Description ===', '',
                        $page['revisions'][0]['*'])));
            }
        }

        return $descriptionsArr;
    }

    /**
     * Get inner page data
     * @param $page_name
     * @return bool|mixed|string
     */
    public function getPageContent($page_name){

        global $wgServer;
        global $wgLoadApi;
        global $wgUser;

        $userId = $wgUser->getId();

        //Api Url
        $url =  $wgLoadApi['url'].'/get_xbpmn_notation_inner_data/'.urlencode(str_replace('/','___',$page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);

        if(!empty($res)) {
            $this->_data['page_title'] = $res['name'];
            $this->_data['page_description'] = $res['description'];
            $this->_data['page_notation'] = $wgServer . '/images/xbpmn_notations/' . str_replace(' ', '%20', $res['notation']);
            $this->_data['page_layer_name'] = $res['sublayer']['layer']['name'];
            $this->_data['page_layer'] = $res['sublayer']['layer']['name'] . '_Layer';
            $this->_data['page_sublayer_name'] = $res['sublayer']['name'];
            $this->_data['page_sublayer'] = $res['sublayer']['name'] . '_Sub-layer';

            $this->_data['page_object_category'] = $res['object_category']['name'];
            $this->_data['page_object_class'] = $res['object_category']['object_class']['name'];
        }
        $view = $this->ci_parser->parse('x-bpmn-notations-inner',$this->_data);

        return $view;
    }

    /**
     * Main Page data
     * @return mixed
     */
    public function index(){
        global $wgServer;
        global $wgLoadApi;
        global $wgTitle;

        $page_name = $wgTitle->mTextform;

        //Api Url
        $url =  $wgLoadApi['url'].'/get_xbpmn_notation_list/'.urlencode(str_replace('/','___',$page_name));

        //Open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('lead-api: ' .$wgLoadApi['key']));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //Execute post
        $result_api = curl_exec($ch);

        $res = json_decode($result_api, true);
        $info = curl_getinfo($ch);

        //Close connection
        curl_close($ch);
        //Define Page sections Arrays
        $this->_data['task_types_arr'] =array();
        $this->_data['gateways_arr'] =array();
        $this->_data['markers_arr'] =array();
        $this->_data['flows_arr'] =array();
        $this->_data['objects_arr'] =array();
        $this->_data['events_arr'] =array();
        $this->_data['conversations_arr'] =array();
        $this->_data['activities_arr'] =array();

        if(isset($res['success'])){
            foreach($res['success'] as $object_category){

                    //Build Task Types array
                    foreach($object_category['xbpmn_notations'] as $xbpm_key=>$xbpmn_notation){
                        $arr_name = str_replace(' ', '_',strtolower($object_category['name'])).'_arr';

                        if (!isset($this->_data[$arr_name][0]['category_name'])) {
                            $this->_data[$arr_name][0]['category_name'] = $object_category['name'];
                        }
                        $this->_data[$arr_name][0]['cat_pages'][$xbpm_key]['page_name'] = $xbpmn_notation['name'];
                        $this->_data[$arr_name][0]['cat_pages'][$xbpm_key]['page_url'] = $wgServer . '/index.php?title=' . str_replace(' ', '_', $xbpmn_notation['name']);
                        $desc= '';
                        if(str_word_count($xbpmn_notation['description'])>100){
                            $desc =implode(' ', array_slice(str_word_count($xbpmn_notation['description'], 2), 0, 100)). ' ...';
                        }else{
                            $desc =  $xbpmn_notation['description'];
                        }

                        $this->_data[$arr_name][0]['cat_pages'][$xbpm_key]['page_description'] =  $desc;
                        $this->_data[$arr_name][0]['cat_pages'][$xbpm_key]['page_notation'] = '/images/xbpmn_notations/' . $xbpmn_notation['notation'];
                    }
            }
        }

        //build page view
        $view = $this->ci_parser->parse('x-bpmn-notations', $this->_data);

        //this regular expression clear ide html formating
        return preg_replace("/[\\t\\s]+/", " ", trim($view));

    }



}