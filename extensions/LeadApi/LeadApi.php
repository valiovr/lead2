<?php


// Take credit for your work.
$wgExtensionCredits['other'][] = array(

    // The full path and filename of the file. This allows MediaWiki
    // to display the Subversion revision number on Special:Version.
    'path' => __FILE__,

    // The name of the extension, which will appear on Special:Version.
    'name' => 'Lead Api',

    // A description of the extension, which will appear on Special:Version.
    'description' => 'Use API in Lead website.',

    // Alternatively, you can specify a message key for the description.
    'descriptionmsg' => 'lead-api-desc',

    // The version of the extension, which will appear on Special:Version.
    // This can be a number or a string.
    'version' => 1,

    // Your name, which will appear on Special:Version.
    'author' => 'Leading Practice',

    // The URL to a wiki page/web page with information about the extension,
    // which will appear on Special:Version.
    'url' => 'https://www.mediawiki.org/wiki/Manual:Parser_functions',

);


$dir = dirname(__FILE__);

/**** Setup ****/

// Allow translation of the parser function name
$wgMessagesDirs['LeadApi'] = __DIR__ . '/i18n';
$wgExtensionMessagesFiles['LeadApiAlias'] = __DIR__ . '/LeadApi.i18n.php';



// Load default settings
require_once __DIR__ . '/LeadApi.settings.php';

$wgAutoloadClasses['LeadApiHooks'] = $dir . '/LeadApi.hooks.php';
$wgHooks['ParserFirstCallInit'][] = 'LeadApiHooks::registerHooks';


/***** Autoload *****/
if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    include_once( __DIR__ . '/vendor/autoload.php' );
}

/***** Register Ajax Methods *****/
$wgAjaxExportList[] = 'LeadApi\Ajax\AjaxSemanticRelationships::getObjectRelations';
$wgAjaxExportList[] = 'LeadApi\Ajax\AjaxMetaObjectDefinitions::getMetaObjectById';
$wgAjaxExportList[] = 'LeadApi\Ajax\AjaxMetaObjectDefinitions::createMetaObjectInstance';
