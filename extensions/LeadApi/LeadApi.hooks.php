<?php 

class LeadApiHooks{

	public static function registerHooks(Parser &$parser) {

		// Add the following to a wiki page to see how it works:
		//  {{#lead: MetaObjectsDescription }}
		$parser->setFunctionHook( 'lead', 'LeadApiHooks::parseContent' );
		$parser->setFunctionHook( 'lead-mw-content', 'LeadApiHooks::parseMwContent' );

		return true;
	}

    /**
     * Hook Parse Content returned from given class
     * @param $parser
     * @param $name
     * @return string
     */
	public static function parseContent( $parser, $name ) {
        $parser->disableCache();
        $className = '\LeadApi\\'.$name;
        //create instance to expected class
        $instance = new $className();
		return array($instance->index(),'noparse' => true,'isHTML' => true); // array( ,'nowiki'=>false, ,  );
	}

	/**
	 * Hook Parse Mediawiki Content returned from given class
	 * @param $parser
	 * @param $name
	 * @return string
	 */
	public static function parseMwContent($parser, $class, $method,$page_name ) {
		$parser->disableCache();
		$className = '\LeadApi\\'.$class;
		//create instance to expected class
		$instance = new $className();

		return array($instance->$method($page_name),'noparse' => false,'isHTML' => false,'nowiki'=>false); // array( ,'nowiki'=>false, ,  );
	}
}

